Library ieee;
USE ieee.std_logic_1164.all;
ENTITY control IS
	PORT ( inst : in std_logic_vector(11 downto 0);
				clock : in std_logic;
				rIn, rOut : out std_logic_vector(0 to 3);
				Ain,Bin,Gin,Gout,Add,Extern,Xorin,xin,x2in,hin,hout,done:out std_logic);
END;

ARCHITECTURE Behavior OF control IS 
	Signal rx, ry: std_logic_vector(0 to 3);
	TYPE State_type IS (Initial, load, load2, move, add1, add2, add3, add4, xor1, xor2, xor3,xor4); 
	SIGNAL Y_present, Y_next : State_type;
BEGIN 
	--Y_present<=Initial;--added since lab
	PROCESS (Clock) 
	BEGIN 
		IF (Clock'EVENT AND Clock = '1') THEN 
			Y_present <= Y_next ; END IF ; 
	END PROCESS; 
		
	process (clock)
	begin
	--(7 downto 4) and (3 downto 0) represent the register op codes.
	-- Basically decoding the register op codes in the following process
	-- the decoding is basically to ensure the asserted control signal would apply to each of the registers 0 to 3 respectively
	    -- the first part shows the value stored in the first register. The final value is also stored in the first register. 
		if inst(7 downto 4)="0000" then
			rx <= "1000";
		elsif inst(7 downto 4)="0001" then
			rx <= "0100";
		elsif inst(7 downto 4)="0010" then
			rx <= "0010";
		elsif inst(7 downto 4)="0011" then
			rx <= "0001";
		end if;
		-- the second part shows the value stored in the second register. 
		if inst(3 downto 0)="0000" then
			ry <= "1000";
		elsif inst(3 downto 0)="0001" then
			ry <= "0100";
		elsif inst(3 downto 0)="0010" then
			ry <= "0010";
		elsif inst(3 downto 0)="0011" then
			ry <= "0001";
		end if;
	end process;
			
	PROCESS (Y_present) -- state table 
		BEGIN 
			CASE Y_present IS 
				WHEN Initial =>  
					Done<='0';--added since lab
					Extern<='0'; Ain<='0'; Gin<='0'; Gout<='0'; Add<='0'; Extern<='0'; Xorin<='0'; xin<='0'; hin<='0'; hout<='0';
					rIn<="0000"; rOut<="0000";
					--declare outputs-- 
					-- decoding the first 4 op codes which determines the operation needed to be carry out as in the state table.
					-- check state table will make sense!!
					IF inst(11 downto 8)="0000" THEN 
						Y_next <= load;
						rIn <=rx;
					ELSIF inst(11 downto 8)="0001" then 
						Y_next <= move;
						rIn<=rx;
						rOut<=ry;
					ELSIF inst(11 downto 8)="0010" then 
						Y_next <= add1;
						Ain<='1';
						rOut<=rx;
					ELSIF inst(11 downto 8)="0011" then 
						Y_next <= xor1;
						Xin<='1';
						rOut<=rx;
					End if;
				-- execution of the operations
				When load =>
					Extern<='1';
					Y_next<=load2;
				When add1 =>
					Ain<='0';
					Bin<='1';
					rOut <= ry;
					Y_next <=add2;
				When add2 =>
					Add <= '1';
					Gin <= '1';
					Bin<='0';
					rOut<="0000";
					Y_next <= add3;
				When add3 =>
					Add<='0'; Gin<='0'; rOut<="0000";
					Gout <= '1';
					rIn <= rx;
					Y_next <= add4;
				When xor1 =>
					Xin<='0';
					X2in<='1';
					Y_next <= xor2;
					rOut <= ry;
					
				When xor2 =>
					Hin <= '1';
					rOut<="0000";
					Y_next <= xor3;
					Xorin<='1';
				When xor3 =>
					Xorin<='0'; Hin<='0'; rOut<="0000";
					Hout <='1';
					Rin <= rx;
					Y_next<=xor4;
				-- default condition when none of the above case occurs
				When others =>
					Ain<='0'; Gin<='0'; Gout<='0'; Add<='0'; Extern<='0'; Xorin<='0'; xin<='0'; hin<='0'; hout<='0';
					rIn<="0000"; rOut<="0000";
					Y_next <= Initial;
					done<='1';
			End Case;
		End Process;
End;
		