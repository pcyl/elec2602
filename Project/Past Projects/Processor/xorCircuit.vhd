Library ieee;
USE ieee.std_logic_1164.all;
-- VHDL for an XOR function.
ENTITY xorCircuit IS
	PORT (X, Y : IN Std_logic_vector(15 downto 0);
			xorIn : IN Std_logic;
			xXORY : Out Std_logic_vector(15 Downto 0));
END xorCircuit;

Architecture behaviour of xorCircuit is
Begin
	process(xorIn)
	begin
		if(xorIn = '1') then
			xXORY<=X XOR Y;
		end if;
	end process;
end;