Library ieee;
USE ieee.std_logic_1164.all;
Use Ieee.std_logic_unsigned.all;

ENTITY adder IS
	PORT (X, Y : IN Std_logic_vector(15 downto 0);
			Add : IN Std_logic;
			xPlusY : Out Std_logic_vector(15 Downto 0));
END adder;

Architecture behaviour of adder is
Begin
	process(add)
	begin
		if(add = '1') then
			xPlusY<=X+Y;
		end if;
	end process;
end;
