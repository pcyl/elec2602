LIBRARY ieee;
USE ieee.std_logic_1164.all;
Use Ieee.std_logic_unsigned.all;
ENTITY PC IS
	Port (clock, reset, IncPC: in std_logic;
				vout: out std_logic_vector(15 downto 0));
	End PC;
Architecture behaviour of PC is
	Signal mid : STD_LOGIC_vector(15 downto 0);

Begin
	process (IncPC, Reset, clock)
			begin
				if(reset='1') then
					mid <= "0000000000000000";
				elsif (clock'event and clock = '1') then
					if(IncPC = '1') then
						mid<=mid+"0000000000000001";
					end if;
				end if;
	end process;
	vout<=mid;
End;