Library ieee;
USE ieee.std_logic_1164.all;
Use Ieee.std_logic_unsigned.all;
use ieee.numeric_std.all ;
ENTITY progMem IS   
GENERIC ( S,N : INTEGER := 16 );
	--S is the memory size, N is the address size
port(    
	clock: in std_logic;
	data: in std_logic_vector (15 downto 0);   
	write_addr: in std_logic_vector (N-1 downto 0);
    read_addr: in std_logic_vector (N-1 downto 0);
	write_enable: in std_logic; 
	q: out std_logic_vector (15 downto 0));   
END;

ARCHITECTURE behavioral OF progMem  IS
TYPE mem IS ARRAY (S-1 downto 0) OF 
			std_logic_vector(15 downto 0);  

FUNCTION initialize_ram RETURN mem IS    
VARIABLE result : mem; 
BEGIN
--enter your own program here
-------------instructions----------------
--"0000000000000000"; --LOAD R0
--"0000000000010000"; --LOAD R1
--"0000000000100000"; --LOAD R2
--"0000000000110000"; --LOAD R3
--"0000000100010000"; --Move R0 to R1
--"0000000100100000"; --Move R0 to R2
--"0000000100110000"; --Move R0 to R3
--"0000000100000001"; --Move R1 to R0
--"0000000100100001"; --Move R1 to R2
--"0000000100110001"; --Move R1 to R3
--"0000000100000010"; --Move R2 to R0
--"0000000100010010"; --Move R2 to R1
--"0000000100110010"; --Move R2 to R3
--"0000000100000011"; --Move R3 to R0
--"0000000100010011"; --Move R3 to R1
--"0000000100100011"; --Move R3 to R2
--"0000001000010000"; --Add R0 to R1
--"0000001000100000"; --Add R0 to R2
--"0000001000110000"; --Add R0 to R3
--"0000001000000001"; --Add R1 to R0
--"0000001000100001"; --Add R1 to R2
--"0000001000110001"; --Add R1 to R3
--"0000001000000010"; --Add R2 to R0
--"0000001000010010"; --Add R2 to R1
--"0000001000110010"; --Add R2 to R3
--"0000001000000011"; --Add R3 to R0
--"0000001000010011"; --Add R3 to R1
--"0000001000100011"; --Add R3 to R2
--"0000001100010000"; --XOR R0 to R1
--"0000001100100000"; --XOR R0 to R2
--"0000001100110000"; --XOR R0 to R3
--"0000001100000001"; --XOR R1 to R0
--"0000001100100001"; --XOR R1 to R2
--"0000001100110001"; --XOR R1 to R3
--"0000001100000010"; --XOR R2 to R0
--"0000001100010010"; --XOR R2 to R1
--"0000001100110010"; --XOR R2 to R3
--"0000001100000011"; --XOR R3 to R0
--"0000001100010011"; --XOR R3 to R1
--"0000001100100011"; --XOR R3 to R2
	
	result(0) := "0000000000000000"; --LOAD R0
	result(1) := "0000000000010001"; --Data = 0011 hex
	result(2) := "0000000000010000"; --LOAD R1
	result(3) := "0000000000100010"; --Data = 0022 hex
	result(4) := "0000000000100000"; --LOAD R2
	result(5) := "0000000000110011"; --Data = 0033 hex	
	result(6) := "0000000000110000"; --LOAD R3
	result(7) := "0000000001000100"; --Data = 0044 hex	
	result(8) := "0000000100000001"; --Move R1 to R0
	result(9) := "0000001000000010"; --Add R2 to R0 = 0055 hex
	result(10) := "0000001100000001"; --XOR R1 to R0 = 0011 hex
	
	return result;
END initialize_ram;

SIGNAL raMem : mem := initialize_ram;
BEGIN  
  PROCESS (clock) 
  BEGIN  
	IF (clock'event and clock = '1') THEN       
		IF (write_enable = '1') THEN         
			raMem(to_integer(unsigned(write_addr))) <= data;      
		END IF;    
	  q <= raMem(to_integer(unsigned(read_addr))); 
	END IF;  
END PROCESS;
END behavioral;
