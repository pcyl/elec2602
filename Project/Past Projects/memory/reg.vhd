LIBRARY ieee;
USE ieee.std_logic_1164.all;
ENTITY reg IS
	Port (clock, reset, Rin: in std_logic;
				vin: in std_logic_vector(15 downto 0);
				vout: out std_logic_vector(15 downto 0));
	End reg;
Architecture behaviour of reg is
	Signal mid : STD_LOGIC_vector(15 downto 0);



Begin
	process (Rin, Reset, clock)
			begin
				if(reset='1') then
					mid <= "0000000000000000";
				elsif (clock'event and clock = '1') then
					if(Rin = '1') then
						mid<=vin;
					end if;
				end if;
	end process;
	vout<=mid;
End;
		
	