LIBRARY ieee;
USE ieee.std_logic_1164.all;
entity tristate is
	port( enable : in std_logic;
		A : in std_logic_vector(15 downto 0);
		Q : out std_logic_vector(15 downto 0));
end tristate;
architecture behavioural of tristate is
	begin
		process (A, enable)
			begin
				if (enable = '1') then
					Q <= A;
				else
					Q <= "ZZZZZZZZZZZZZZZZ";
				end if;
		end process;
end behavioural;