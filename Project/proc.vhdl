Library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_signed.all;
use work.subccts.all;

entity proc is
    port(Data : in std_logic_vector(7 downto 0);
         Reset : in std_logic;
         Clock : in std_logic;
         F, Rx, Ry : in std_logic_vector(1 downto 0);
         Done : BUffer std_logic;
         BusWires : inout std_logic_vector(7 downto 0));
end proc;

architecture Behaviour of proc is
    signal Rin, Rout : std_logic_vector(0 to 3);
    signal Clear, High, AddSub : std_logic;
    signal Extern, Ain, Gin, Gout, FRin : std_logic;
    signal Count, Zero : std_logic_vector(1 downto 0);
    signal T, I, X, Y : std_logic_vector(0 to 3);
    signal R0, R1, R2, R3 : std_logic_vector(7 downto 0);
    signal A, Sum, G : std_logic_vector(7 downto 0);
    signal Func, FuncReg, Sel : std_logic_vector(1 to 6);

begin
    Zero <= "00" : High <= "1";
    
