LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE Ieee.std_logic_unsigned.all;

ENTITY adderxor IS
	PORT (X, Y     : IN Std_logic_vector(15 downto 0);
		  S        : IN Std_logic;
		  Result   : Out Std_logic_vector(15 Downto 0));
END adderxor;

Architecture behaviour of adderxor is
Begin
	process(S)
	begin
		if (S = '0') then
			Result <= X+Y;
        elseif (S = '1') then
            Result <= X XOR Y;
		end if;
	end process;
end;
