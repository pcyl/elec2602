LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY controlunit IS
	PORT (
		instructions 			: IN std_logic_vector(15 downto 0);
		Clock 					: IN std_logic;
		Rin, Rout 				: OUT std_logic_vector(3 downto 0);
		Ain, Gin, Gout 			: OUT std_logic;
		AddXor, Extern, Done 	: OUT std_logic));
END controlunit;

ARCHITECTURE Behavior OF controlunit IS
	TYPE State_type IS (AddXor, Load, Move);
	SIGNAL reg : std_logic_vector(3 downto 0);
	SIGNAL rx, ry : std_logic_vector(15 downto 0);
	SIGNAL counter : std_logic;
BEGIN
	PROCESS (Clock)
	BEGIN
		IF (Clock'event AND Clock = '1') THEN

	END PROCESS;

	PROCESS (Clock)
	BEGIN
		IF (instructions(15 downto 12) = "0000" OR counter = '1') THEN -- LOAD INSTRUCTION
			-- Store register in signal reg
			reg <= instructions(11 downto 8);
			-- GRAB next 16 bits
			counter <= '1';
			-- Store into register reg
			-- Reset reg
		END IF;

		IF (counter = '1') THEN -- Checks if Load instruction has been loaded
			Rin(reg) = instructions(15 downto 0);
		END IF;

		IF (instructions(15 downto 12) = "0001") THEN -- MOVE INSTRUCTION
			-- Store first register into signal reg(0)
			-- load reg(0) into second register
			-- Reset reg
		END IF;

		IF (instructions(15 downto 12) = "0010") THEN -- ADD INSTRUCTION
			-- load rx
			-- load ry
			-- load into adderxor function with S = 0, with output in rx
		END IF;

		IF (instructions(15 downto 12) = "0011") THEN -- XOR INSTRUCTION
			-- load rx
			-- load ry
			-- load into addercor function with S = 1, with output in rx
		END IF;

		ELSE
			-- Reset registers

