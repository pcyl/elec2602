# README #
### What is this repository for? ###

This repo is my coursework in ELEC2602: Digital Logic. Mainly contains lab work and report writing in LaTeX.

### How do I get set up? ###

Just a bunch of folders put together here. There is a section for lab work and a section for report writing.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact