LIBRARY ieee;
USE ieee.STD_LOGIC_1164.all;

ENTITY mux_3b_5to1 IS
	PORT (S : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
	U, V, W, X, Y : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
	M : OUT STD_LOGIC_VECTOR(2 DOWNTO 0));
END mux_3b_5to1;

ARCHITECTURE Behavior OF mux_3b_5to1 IS
COMPONENT mux_1b_5to1
	PORT (S : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
	U, V, W, X, Y : IN STD_LOGIC;
	M : OUT STD_LOGIC);
END COMPONENT;

BEGIN
	M0 : mux_1b_5to1
	PORT MAP (S(2 DOWNTO 0), U(2), V(2), W(2), X(2), Y(2), M(2));
	M1 : mux_1b_5to1
	PORT MAP (S(2 DOWNTO 0), U(1), V(1), W(1), X(1), Y(1), M(1));
	M2 : mux_1b_5to1
	PORT MAP (S(2 DOWNTO 0), U(0), V(0), W(0), X(0), Y(0), M(0));
END Behavior;