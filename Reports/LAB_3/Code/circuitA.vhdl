library ieee;
use ieee.std_logic_1164.all;

entity circuitA is
  port (v : in std_logic_vector(2 downto 0);
  b : out std_logic_vector(3 downto 0));
end;

architecture behaviour of circuitA is
begin
	b <= "0000" when v = "010" else
		  "0001" when v = "011" else
		  "0010" when v = "100" else
		  "0011" when v = "101" else
		  "0100" when v = "110" else
		  "0101" when v = "111" else
		  "1111";
end;
