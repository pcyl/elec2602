library ieee;
use ieee.std_logic_1164.all;

entity circuitB is
  port (x :in std_logic;
  d : out std_logic_vector(0 to 6));
end;

architecture behaviour of circuitB is
begin
	d <= "1001111" when x = '1' else
		  "0000001" when x = '0';
end;
