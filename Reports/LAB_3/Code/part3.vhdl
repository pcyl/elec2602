LIBRARY ieee;
USE ieee.std_logic_1164.all;

entity part3 is
port( SW : IN std_logic_vector(8 downto 0);
		LEDR : OUT std_logic_vector(8 downto 0);
		LEDG : OUT std_logic_vector(4 downto 0));
end;

architecture behavior of part3 is
component fa
  port (a, b, cin : in std_logic;
  c, s : out std_logic);
 end component;
 
 signal c : std_logic_vector(2 downto 0);
 begin
 LEDR <= SW;
 F1 : fa
 port map(SW(4), SW(0), SW(8), c(0), LEDG(0));
 F2 : fa
 port map(SW(5), SW(1), c(0), c(1), LEDG(1));
 F3 : fa
 port map(SW(6), SW(2), c(1), c(2), LEDG(2));
 F4 : fa
 port map(SW(7), SW(3), c(2), LEDG(4), LEDG(3));
 
end; 