LIBRARY ieee ;
USE ieee.std_logic_1164.all ;
ENTITY neglatch IS

 PORT ( Clk, D : IN STD_LOGIC ;
  Q : buffer STD_LOGIC;
 Qnot : OUT STD_LOGIC );

END neglatch ;


ARCHITECTURE Behavior OF neglatch IS
SIGNAL Qm: STD_LOGIC;

BEGIN

	PROCESS (Clk, D)
	BEGIN
		if (Clk = '1') then
		Qm <= D ;

	end if ;
	end process;
	
	PROCESS (Qm, Clk)
	BEGIN
		if (Clk = '0') then
			Q <= Qm;
		
		end if;
	end process;
	Qnot <= not Q;
	
	
	
END Behavior ; 
