LIBRARY ieee;
USE ieee.std_logic_1164.all;


ENTITY part5 IS
 PORT ( SW: IN std_logic_vector(15 downto 0);
		  KEY: IN std_logic_vector(1 downto 0);
			HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, HEX6, HEX7: OUT std_logic_vector(0 to 6));
END part5;

ARCHITECTURE Structural OF part5 IS

 COMPONENT latcher IS
 PORT (Clk, D : IN STD_LOGIC;
	Q, Qnot : OUT STD_LOGIC);
	end component;

COMPONENT hexdecoder is
  port (s : in std_logic_vector(3 downto 0);
  display : out std_logic_vector(0 to 6));
end component;

signal tmp0 : std_logic_vector(3 downto 0);
signal tmp1 : std_logic_vector(3 downto 0);
signal tmp2 : std_logic_vector(3 downto 0);
signal tmp3 : std_logic_vector(3 downto 0);

BEGIN
	latch0 : latcher port map(NOT key(0), SW(0),tmp0(0));
	latch1 : latcher port map(NOT key(0), SW(1),tmp0(1));
	latch2 : latcher port map(NOT key(0), SW(2),tmp0(2));
	latch3 : latcher port map(NOT key(0), SW(3),tmp0(3));

	latch4 : latcher port map(NOT key(0), SW(4),tmp1(0));
	latch5 : latcher port map(NOT key(0), SW(5),tmp1(1));
	latch6 : latcher port map(NOT key(0), SW(6),tmp1(2));
	latch7 : latcher port map(NOT key(0), SW(7),tmp1(3));

	latch8 : latcher port map(NOT key(0), SW(8),tmp2(0));
	latch9 : latcher port map(NOT key(0), SW(9),tmp2(1));
	latch10 : latcher port map(NOT key(0), SW(10),tmp2(2));
	latch11 : latcher port map(NOT key(0), SW(11),tmp2(3));

	latch12 : latcher port map(NOT key(0), SW(12),tmp3(0));
	latch13 : latcher port map(NOT key(0), SW(13),tmp3(1));
	latch14 : latcher port map(NOT key(0), SW(14),tmp3(2));
	latch15 : latcher port map(NOT key(0), SW(15),tmp3(3));

	decode0 : hexdecoder PORT MAP (SW(3 downto 0), HEX0);
	decode1 : hexdecoder port map(SW(7 downto 4), HEX1);
	decode2 : hexdecoder port map(SW(11 downto 8), HEX2);
	decode3 : hexdecoder port map(SW(15 downto 12), HEX3);

	decode4 : hexdecoder port map(tmp0(3 downto 0), HEX4);
	decode5 : hexdecoder port map(tmp1(3 downto 0), HEX5);
	decode6 : hexdecoder port map(tmp2(3 downto 0), HEX6);
	decode7 : hexdecoder port map(tmp3(3 downto 0), HEX7);
END Structural;
