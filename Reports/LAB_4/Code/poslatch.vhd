LIBRARY ieee ;
USE ieee.std_logic_1164.all ;
ENTITY poslatch IS

 PORT ( Clk, D : IN STD_LOGIC ;
 Q : buffer STD_LOGIC;
  Qnot : OUT STD_LOGIC) ;
END poslatch ;


ARCHITECTURE Behavior OF poslatch IS
SIGNAL Qm: STD_LOGIC;
BEGIN

	PROCESS ( D, Clk )
	BEGIN
		if Clk = '0' then
		Qm <= D ;
	
	end if ;
	end process;
	
	PROCESS (Qm, Clk)
	BEGIN
		if (Clk = '1') then
			Q <= Qm;
	
		end if;
	end process;
Qnot <= NOT Q;
	
	
	
END Behavior ; 
