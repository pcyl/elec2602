library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
 
entity part1 is
   port (SW: in std_logic_vector(1 downto 0);
	KEY: in std_logic_vector(1 downto 0);
	LEDR: out std_logic_vector(15 downto 0);
	HEX0, HEX1, HEX2, HEX3: OUT std_logic_vector(0 to 6));
end part1;
 
architecture beh of part1 is	 
	signal temp: std_logic;
	
component t_trigger is
   port (T, Reset, CLK, CLK_enable: in std_logic;
	 Q: out std_logic);
end component;

component hexdecoder is
  port (s : in std_logic_vector(3 downto 0);
  display : out std_logic_vector(0 to 6));
end component;

signal q : std_logic_vector(15 downto 0);
signal qout : std_logic_vector(15 downto 0);
begin

	q(0) <= (qout(0) and SW(1)); 
	q(1) <= (qout(1) and q(0));
   q(2) <= (qout(2) and q(1));
	q(3) <= (qout(3) and q(2));
	q(4) <= (qout(4) and q(3));
	q(5) <= (qout(5) and q(4));
	q(6) <= (qout(6) and q(5));
	q(7) <= (qout(7) and q(6));
	q(8) <= (qout(8) and q(7));
	q(9) <= (qout(9) and q(8));
	q(10) <= (qout(10) and q(9));
	q(11) <= (qout(11) and q(10));
	q(12) <= (qout(12) and q(11));
	q(13) <= (qout(13) and q(12));
	q(14) <= (qout(14) and q(13));
	q(15) <= (qout(15) and q(14));
	
  counter1 : t_trigger port map(SW(1), SW(0), KEY(0), SW(1), qout(0));
  counter2 : t_trigger port map(q(0), SW(0), KEY(0), SW(1), qout(1));
  counter3 : t_trigger port map(q(1), SW(0), KEY(0), SW(1), qout(2));
  counter4 : t_trigger port map(q(2), SW(0), KEY(0), SW(1), qout(3));
  
  counter5 : t_trigger port map(q(3), SW(0), KEY(0), SW(1), qout(4));
  counter6 : t_trigger port map(q(4), SW(0), KEY(0), SW(1), qout(5));
  counter7 : t_trigger port map(q(5), SW(0), KEY(0), SW(1), qout(6));
  counter8 : t_trigger port map(q(6), SW(0), KEY(0), SW(1), qout(7));
  
  counter9 : t_trigger port map(q(7), SW(0), KEY(0), SW(1), qout(8));
  counter10 : t_trigger port map(q(8), SW(0), KEY(0), SW(1), qout(9));
  counter11 : t_trigger port map(q(9), SW(0), KEY(0), SW(1), qout(10));
  counter12 : t_trigger port map(q(10), SW(0), KEY(0), SW(1), qout(11));
  
  counter13 : t_trigger port map(q(11), SW(0), KEY(0), SW(1), qout(12));
  counter14 : t_trigger port map(q(12), SW(0), KEY(0), SW(1), qout(13));
  counter15 : t_trigger port map(q(13), SW(0), KEY(0), SW(1), qout(14));
  counter16 : t_trigger port map(q(14), SW(0), KEY(0), SW(1), qout(15));
  
  LEDR(15 downto 0) <= qout(15 downto 0);
  
  decode0 : hexdecoder port map(qout(3 downto 0), HEX0);
  decode1 : hexdecoder port map(qout(7 downto 4), HEX1);
  decode2 : hexdecoder port map(qout(11 downto 8), HEX2);
  decode3 : hexdecoder port map(qout(15 downto 12), HEX3);
end beh;