library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_signed.all;


entity part3 is
port(CLOCK_50 : in STD_LOGIC;
	SW : in std_LOGIC_VECTOR(1 downto 0);
	Q : BUFFER STD_LOGIC_VECTOR(25 downto 0);
	HEX0, HEX1, HEX2, HEX3: OUT std_logic_vector(0 to 6);
	LEDR : OUT std_LOGIC_VECTOR(15 downto 0));
end part3;

architecture rtl of part3 is
	signal count : std_logic_vector(15 downto 0);
	signal R : std_LOGIC;
	signal enable : std_LOGIC;

component hexdecoder is
  port (s : in std_logic_vector(3 downto 0);
  display : out std_logic_vector(0 to 6));
end component;


begin

R <= SW(0);
enable <= SW(1);
PROCESS(CLOCK_50, R)
BEGIN
IF R = '1' then
	Q <= (others => '0');
ELSIF (CLOCK_50'event AND CLOCK_50='1' AND enable='1') then
	Q <= Q+1;
END IF;
END PROCESS;

PROCESS(Q)
BEGIN
IF R = '1' then
	count <= (others => '0');
ELSIF (CLOCK_50'event AND CLOCK_50='1' AND
	enable='1' AND Q = "10111110101111000010000000") then
	count <= count+1;
END IF;
END PROCESS;


LEDR(15 downto 0) <= count(15 downto 0);
decode1 :  hexdecoder port map(count(3 downto 0), HEX0);
decode2 :  hexdecoder port map(count(7 downto 4), HEX1);
decode3 :  hexdecoder port map(count(11 downto 8), HEX2);
decode4 :  hexdecoder port map(count(15 downto 12), HEX3);

end rtl;
