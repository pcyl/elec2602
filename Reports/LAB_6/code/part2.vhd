LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY part2 IS
PORT (SW: IN STD_LOGIC_VECTOR(17 downto 16); --SW(16):reset, SW(17):the K input
KEY: IN STD_LOGIC_VECTOR(0 downto 0); --clk
LEDR: OUT STD_LOGIC_VECTOR(17 downto 0); --flip flop output
LEDG: OUT STD_LOGIC_VECTOR(7 downto 0)); --G is true output
end part2;

ARCHITECTURE Behavior OF part2 IS
TYPE State_type IS (B,C,D,E,F,G,H,I,A);
SIGNAL y_p, y_n : State_type;
BEGIN

PROCESS (KEY(0), SW(16))
BEGIN
if SW(16) = '1' then y_p <= A ; -- resets to state A
elsif (KEY(0)'EVENT AND KEY(0) = '1') then y_p <= y_n ; --rising edge clock
end if;
END PROCESS ;

PROCESS (SW(17),y_p)
BEGIN
CASE y_p IS
WHEN A =>
--declare outputs--
IF SW(17) = '1' THEN
y_n <= B;
ELSE
y_n <= F;
END IF;
LEDR(8 DOWNTO 0) <= "000000000";
WHEN B =>
IF SW(17) = '1' THEN
y_n <= C;
ELSE
y_n <= G;
END IF;
LEDR(8 DOWNTO 0) <= "000000011";
WHEN C =>
IF SW(17) = '1' THEN
y_n <= D;
ELSE
y_n <= H;
END IF;
LEDR(8 DOWNTO 0) <= "000000101";
WHEN D =>
IF SW(17) = '1' THEN
y_n <= E;
ELSE
y_n <= I;
END IF;
LEDR(8 DOWNTO 0) <= "000001001";
WHEN E =>
IF SW(17) = '1' THEN
y_n <= B;
ELSE
y_n <= F;
END IF;
LEDR(8 DOWNTO 0) <= "000010001";
WHEN F =>
y_n <= G;
LEDR(8 DOWNTO 0) <= "000100001";
WHEN G =>
y_n <= H;
LEDR(8 DOWNTO 0) <= "001000001";
WHEN H =>
y_n <= I;
LEDR(8 DOWNTO 0) <= "010000001";
WHEN I =>
IF SW(17) = '1' THEN
y_n <= B;
ELSE
y_n <= F;
END IF;
LEDR(8 DOWNTO 0) <= "100000001";
END CASE;
END PROCESS;

LEDR(17 downto 10) <= "11111111"when y_p = I else "00000000";
LEDG(7 downto 0) <= "11111111" when y_p = E else "00000000";
END Behavior;
