\documentclass[a4paper, 11pt]{article}
\usepackage{fullpage}
\usepackage{microtype}
\usepackage{float}
\usepackage{graphicx}
\usepackage{minted}
\usepackage{amsmath}
%\setlength{\parskip}{0.5em}
%\setlength{\parindent}{0em}

\newcommand{\HRule}{\rule{\linewidth}{0.5mm}}

\author{Patrick Lai}
\date{}
\title{Laboratory Project: \\ 16-bit Processor}

\begin{document}
\input{./title.tex}
\definecolor{bg}{rgb}{0.95,0.95,0.95}

\section{Objectives}
The objective of this project is to create a 16-bit Processor that is capable of receiving and operating on a small RISC architecture instruction set consisting of four instructions which include load, move, add and xor. The exact operations of these instructions are explained in detail in Section 3. The processor is to contain four 16-bit registers, a common bus, an Arithmetic Logic Unit (ALU), tristate buffers and a control unit. There are also additional objectives that are optional improvements to the processor design such as additional instructions, performance optimisations, etc.

\section{Equipment}
\begin{itemize}
\item Altera Quartus II Software Package
\item Altera DE-2 FPGA Board
\item Computer
\end{itemize}

\section{Design}
\subsection{Initial Stage -- Scheduling \& Planning}
The initial stage of this project was to firstly fully understand all the specifications before even starting on coding or drawing a datapath diagram. This step involved a collaborative discussion between our team members on the specifics of the project and how we should approach the project as well as carefully planning the schedule to meet the deadline.

The discussions were made through meetings and messaging applications such as WhatsApp and Skype. In our discussions we had prominently planned a schedule which was primarily based on the recommended schedule in the project description with a slight extension added as a buffer which is shown below in Table \ref{tab:schedule}.

\begin{table}[H]
\centering
\caption{Schedule \label{tab:schedule}}
\begin{tabular}{| c | c |}
\hline
Week & Parts \\ \hline
9 & 1 -- 3\\ 
10 & 3 -- 5\\ 
11 & 5 -- 6\\ 
12 & 5 -- 6 / Optional Improvements\\
13 & Optional Improvements\\ \hline
\end{tabular}
\end{table}

We had allocated two weeks towards Part 5 and 6 as they were considered very difficult and so a buffer was made for the situation where we do not finish them in time during week 12. But in the case if we do then we could start working on optional improvements. We had budgeted our schedule completely all the way to week 13, due to the difficulty of the project.

\subsection{Synthesis of Ideas}
In this stage, we had approached to the development of the datapath diagram and its included components. This stage was about focusing on the block components of the processor rather than the inner control functions of each component. Our team discussions, sought to carefully approach the task and work together to deliver a sound view of what to develop for the processor.

Our initial work toward a datapath diagram was to understand the kind of operations the processor was required to do. This was specified in the project description, this included four instructions, four 16-bit registers and an ALU, all controlled by the control unit. The four instructions included load, mov, add and xor, all of which is described in Table \ref{tab:InstSet} below:

\begin{table}[H]
\centering
\caption{Instruction Set \label{tab:InstSet}}
\begin{tabular}{l | l}
\hline
Operation & Function performed  \\ \hline
load Rx, D & Rx $\leftarrow$ D \\
mov Rx, Ry & Rx $\leftarrow$ Ry \\
add Rx, Ry & Rx $\leftarrow$ Rx + Ry \\
xor Rx, Ry & Rx $\leftarrow$ Rx $\oplus$ Ry \\ \hline 
\end{tabular}
\end{table}

We had developed a datapath diagram that was heavily based on the design in the recommended textbook (Fundamentals of Digital Logic, Brown and Vranesic). This design was chosen due to its simplicity and ease of construction with only the control unit being the main operator of all operations required. The following Figure \ref{fig:datapath} below was exactly what we had wanted to produce.

\begin{figure}[H]
\centering
\caption{Datapath Diagram \label{fig:datapath}}
\includegraphics[scale=0.9]{Diagrams/datapath_diagram.png}
\end{figure}

From the advice of the tutors, getting the datapath diagram correct was crucial in the development of the processor, as it will help determine how to write up the control unit, determining when to release data into the bus and how to release it.
\pagebreak
\subsection{Function Analysis}
This stage was our initial launch in our process to develop the necessary functions which included a state diagram. This was initially quite challenging as we hadn't had much experience in drawing up our own state diagrams previously. We did find and adapt a state diagram given in the lecture. The diagram is presented below in Figure \ref{fig:state_diagram}.

\begin{figure}[H]
\centering
\caption{State Diagram \label{fig:state_diagram}}
\includegraphics[scale=0.3]{Diagrams/state_diagram.png}
\end{figure}

From the state diagram, we were able to implement the instructions, the four instructions are explained in detail below:

\subsubsection*{Load}
The Load instruction is executed in two clock cycles as per the state diagram. The main purpose of the load function is to store a specified 16-bit value into a specified register, preparing the data to be used for other operations such as move, add and xor. The process behind the load function is done in two cycles, whereby the first cycle, is the loading of the instruction which is done inside a state machine. The instruction is read with the first set of 16-bits, specifying the load function and the register to store the data. In the second cycle, the second set of 16-bits is then read from an input, this is the data. Specified in the state machine is the operation of enabling a target register in the first clock cycle and then enabling the data to flow from the bus into the target register.

\subsubsection*{Move}
The Move instruction is executed in one clock cycle as per the state diagram. The main purpose of the move function is to store data from one register to another register, this prepares it for other operations. The process of the move function is executed within a clock cycle where in the instruction only the function and the registers are required for operation. This is because within that clock cycle, the main operation is enabling the source tristate buffer and enabling the target register in order to successfully move the data from the source register into the bus then into the target register.

\subsubsection*{Add}
The Add instruction is executed in three clock cycles as per the state diagram. The main purpose of the Add function is to add two registers with 16-bits of data together and store it back into the the target register, specified by the first parameter in the instruction. The execution of the Add function is as follows: the first clock cycle is enabling the first parameter register's buffer and the tristate buffer connected to the ALU, so the data can flow to the buffer just before the ALU, the second clock cycle enables the second parameter register's buffer and enabling the ALU and the buffer connected to the ALU, within this cycle the add operation is executed and stored in the result register, the third cycle is the enabling of the result register and the first parameter register to store the result back into the first parameter register.

\subsubsection*{Xor}
The Xor instruction process is very similar to the Add instruction with the only difference being the operation itself. The instruction is executed in three clock cycles as per the state diagram. The main purpose of the Xor function is to xor two registers with 16-bits of data together and store it back into the the target register, specified by the first parameter in the instruction. The execution of the Xor function is as follows: the first clock cycle is enabling the first parameter register's buffer and the tristate buffer connected to the ALU, so the data can flow to the buffer just before the ALU, the second clock cycle enables the second parameter register's buffer and enabling the ALU and the buffer connected to the ALU, within this cycle the xor operation is executed and stored in the result register, the third cycle is the enabling of the result register and the first parameter register to store the result back into the first parameter register.

\subsubsection{Instruction Formatting}
The instruction format is very important in the design of the control unit. The specifications for the formatting was given in the lecture which involves the use of 16-bits but only a maximum of 12-bits are actually used. However the extra bits allows it to be scalable should we reach into the stage of optional improvements where more instructions can be added. The main sections of the instruction data is the first 12-bits, the componentisation of the code is outlined below.

\begin{equation*}
0011\  0001\  0010\  0000
\end{equation*}

\begin{figure}[H]
\centering
\begin{tabular}{| c | l |}
\hline
Section & Component \\ \hline
0011 & Instruction (Add) \\
0001 & Rx (R1)\\
0010 & Ry (R2)\\
0000 & n/a \\ \hline
\end{tabular}
\end{figure}

An exception to this general format is the Load function which would only require the first 8-bits significant bits, but then the next set of 16-bits for the data.

\subsubsection{Memory}
One of the requirements of the project is being able to run a program off memory. The process behind this requires a program memory component and a program counter to sequentially access each set of  instructions. A thorough cohesion between the control unit and the program counter is required for the memory to be read properly. The theory is by first reading an instruction from memory, then do the operation within the control unit, before incrementing the program counter which reads the next set of instructions. The actual component of the memory was already given in the laboratory project resources, so we didn't have to worry about designing it.

\section{Test Procedure}
Our testing started off with testing each component individually, before connecting them together in a circuit with final testing on the complete design. We first tested the components on the Altera ModelSim Simulator before proceeding to the board. The test procedure for each will be described in detail below.
\subsection{Simulations}
\subsubsection{Individual Components}
Each component was tested using basic data before being implemented into the final design. We used the ModelSim Simulator to test each section of the component. This was done by isolating the component into its own project and then compiling it before using the Waveform functional simulator. We could mainly test only three components, the Adder, Xor and Tristate buffer. For some reason we could properly test the control circuit and the registers. Those could only be tested on the board which is specified in Hardware Subsection 4.2.

\subsubsection*{Adder Circuit}
The adder circuit consists of four parts which are Add (enable), X (data), Y (data) and XPlusY (result). In order to test this component, we initially assigned values for X and Y then we had enable set to 1 and created a waveform to check whether the adder was properly adding the data correctly by desk-checking. We later added a test with a section of the enable set to 0 to see whether it would properly block the operation of add. The result of this testing is shown in Figure \ref{fig:addersim}.

\begin{figure}[H]
\centering
\caption{Adder Simulation \label{fig:addersim}}
\includegraphics[scale=0.55]{Diagrams/addersim.png}
\end{figure}

\subsubsection*{XOR Circuit}
The XOR circuit consists of four parts which are xorIn (enable), X (data), Y (data) and xXorY (result). In order to test this component, we also initially assigned values for X and Y then we had enable set to 1 and created a waveform to check whether the xor was properly xoring the data correctly by desk-checking. We later added a test with a section of the enable set to 0 to see whether it would properly block the operation of Xor. The result of this testing is shown in Figure \ref{fig:xorsim}.

\begin{figure}[H]
\centering
\caption{XOR Simulation \label{fig:xorsim}}
\includegraphics[scale=0.55]{Diagrams/xorsim.png}
\end{figure}

\subsubsection*{Tristate Buffer}
The Tristate buffer consists on three parts this includes the enable, A (data IN) and Q (data OUT). In order to test this component, we first set enable to 1, with A having an initial value in place. This correctly output a result, so in a later addition to the testing we tested the buffer with the enable set to 0 to check whether the buffer was still operating properly even when enable was off. This of course worked and the result of the testing is shown in Figure \ref{fig:trisim}.

\begin{figure}[H]
\centering
\caption{Tristate Buffer Simulation \label{fig:trisim}}
\includegraphics[scale=0.56]{Diagrams/trisim.png}
\end{figure}

\subsection{Hardware}
In this section we tested our design on the FPGA board. This is where we also tested the individual components together and the whole circuit connected together. The main individual components we tested were the hexdecoder, the adder, XOR circuit and the tristate buffer.

\subsubsection*{Hexdecoder}
We tested the hex decoder using the switches as values to be represented using the decoder. Even though this component was given in the laboratory project resources, we wanted to test it to make sure we understand how it works so we can implement it into our project properly. During our analysis, we worked out it was a 4-bit decoder with two parts, the input and output. As expected during our testing the component was working perfectly. 

\subsubsection*{Adder Circuit}
When testing the Adder circuit on the board we used the switches to input data that corresponded with the data seen in the simulation. We then assigned the red LEDs as the display of the output value. We had trouble testing it initially for some reason with out pin assignments were not working properly but eventually we found the fault to be a minor mistake in our assignments of switches. When we fixed that issue the adder circuit worked properly on the board.

\subsubsection*{XOR Circuit}
When testing the XOR circuit on the board we used the switches to input data that corresponded with the data seen in the simulation. We then assigned the red LEDs as the display of the output value. Learning from the mistakes of the Adder circuit testing we had made proper adjustments to the code. The component worked first time without any issues.

\subsubsection*{Tristate Buffer}
Testing the tristate buffer on the board was a very simple process it involved using the switches to input data onto the board, then using the key as an enable toggle, then outputting to the red LEDs. We did have initial issues with adjusting the code to fit the board. The issue was really finding an effective way of viewing whether the output was correct. We had successfully tested the buffer on the board after a few attempts. All test involved the data used in the simulation.

\subsection{Processor}
To test the processor we required test programs which we used from the laboratory resources page. There was Program1-3 that could be tested, we firstly tested Program1. These programs were loaded into the memory and accessed sequentially using our program counter. We would also be testing the board using manual input in case our board was failing the memory programs. So the switches on the board were fully assigned and the Key was assigned as our clock. The results of this testing will be explored in detail in the Results Section 5.
\pagebreak
\section{Results}
The following results are from our testing procedure outlined in the previous section. When testing the memory programs, our board was not displaying correct results at all, this was a strange occurrence since the individual components when isolated were working properly. At this stage we suspected  a bad board, so we changed the board and again it had produced an incorrect result, we concluded during that stage that the memory section was broken. So we proceeded to manual input which involved inputting values using the switches.

In our testing, the load function worked flawlessly, however we found issues with the move function which was occasionally working, where at times it would move the data, but at times it would reset the value in the target register completely. We proceeded to check the other functions, which were the add and xor functions. These however also did not work properly,as a result we attempted to debug the program. We immediately started debugging the control unit as that was where the core issue was suspected to be. 

During our debugging, we first desk-checked the logic of our state machine implemented into our control unit. However our check concluded the logic was fine. This unfortunately led to an having an issue which we couldn't solve. We had then changed parts of our code and isolated sections to see what could be causing the issue, but we soon concluded that we couldn't find the issue as well. At this stage we left the processor in its current state.

\section{Conclusion}
In our project of designing a 16-bit processor, we had to combine the skills from previous labs and include new methods learnt in recent lectures and our own research to implement the processor. In our final design we had major issues in our control unit that led to the majority of the functions to not work properly even when they were working fine individually. Due to time management issues we had to test the final design in Week 13, without managing to start on optional improvements. Learning from this project we could have completed with the functions all working properly if we had managed our time better as the parts required to fix the operation of the processor from our inspection looks minor but causes a huge issue with its operation. We hadn't expected the code to not work as we had intended. In conclusion our processor design was in the right direction but was severely hampered in refinement.

\end{document}