LIBRARY ieee;
USE ieee.STD_LOGIC_1164.all;

ENTITY char_7seg IS
  PORT(C : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
  Display : OUT STD_LOGIC_VECTOR(0 TO 6));
END char_7seg;

ARCHITECTURE Behavior OF char_7seg IS
BEGIN
  Display <= "1001000" when c = "000" else
             "0110000" when c = "001" else
             "1110001" when c = "010" else
             "0000001" when c = "011" else
             "1111111";
END Behavior;
