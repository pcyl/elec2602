--2 BIT TO 1 MULTIPLEXER
LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY mux2 is
-- s = selector
  PORT (s, x, y : IN STD_LOGIC;
  m : OUT STD_LOGIC);
END mux2;

ARCHITECTURE Behavior OF mux2 IS
BEGIN
  m <= (NOT (s) AND x) OR (s AND y);
END Behavior; 
