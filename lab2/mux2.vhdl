LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY mux2 IS
  PORT (x, y, s : IN STD_LOGIC;
  m : OUT STD_LOGIC);
END mux2;

ARCHITECTURE Behavior OF mux2 IS
BEGIN
  m <= (NOT (s) AND x) OR (s AND y);
END Behavior;
