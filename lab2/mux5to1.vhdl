LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY mux_3b_5to1 IS
  PORT (S : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
  U, V, W, X, Y : IN STD_LOGIC;
  M : OUT STD_LOGIC_VECTOR(2 DOWNTO 0));
END mux_3b_5to1;

SIGNAL A, C, D : STD_LOGIC;
ARCHITECTURE Behavior OF mux_3b_5to1 IS
BEGIN
  M0 : mux2 PORT MAP(S(2), A, Y, M);
  M1 : mux2 PORT MAP(S(1), C, D, A);
  M2 : mux2 PORT MAP(S(0), W, X, D);
  M3 : mux2 PORT MAP(S(0), U, V, C);
END Behavior;
