LIBRARY ieee;
USE ieee.std_logic_1164.all;

-- 8 bit Mux
ENTITY part2 IS
PORT ( SW : IN STD_LOGIC_VECTOR(17 DOWNTO 0); --SWTICHES
			LEDG : OUT STD_LOGIC_VECTOR(7 DOWNTO 0); -- green LEDs
			LEDR : OUT STD_LOGIC_VECTOR(17 DOWNTO 0)); -- red LEDs
END part2;

ARCHITECTURE Behavior OF part2 IS
COMPONENT mux2
	PORT (X, Y, S : IN STD_LOGIC;
	M : OUT STD_LOGIC);
END COMPONENT;

BEGIN
	LEDR <= SW;
	M0 : mux2
	PORT MAP(SW(7), SW(15), SW(17), LEDG(7));
	M1 : mux2
	PORT MAP(SW(6), SW(14), SW(17), LEDG(6));
	M2 : mux2
	PORT MAP(SW(5), SW(13), SW(17), LEDG(5));
	M3 : mux2
	PORT MAP(SW(4), SW(12), SW(17), LEDG(4));
	M4 : mux2
	PORT MAP(SW(3), SW(11), SW(17), LEDG(3));
	M5 : mux2
	PORT MAP(SW(2), SW(10), SW(17), LEDG(2));
	M6 : mux2
	PORT MAP(SW(1), SW(9), SW(17), LEDG(1));
	M7 : mux2
	PORT MAP(SW(0), SW(8), SW(17), LEDG(0));
END Behavior;