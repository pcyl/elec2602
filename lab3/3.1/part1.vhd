library ieee;
use ieee.std_logic_1164.all;

entity part1 is
  port (SW : in std_logic_vector(15 downto 0);
  HEX0, HEX1, HEX2, HEX3 : out std_logic_vector(0 to 6));
end;

architecture behaviour of part1 is
COMPONENT decode7 
 port (s : in std_logic_vector(3 downto 0);
  display : out std_logic_vector(0 to 6));
 end component;

begin
  D0 : decode7
  port map (SW(15 downto 12), HEX3);
  D1 : decode7 
  port map (SW(11 downto 8), HEX2);
  D2 : decode7
  port map (SW(7 downto 4), HEX1);
  D3 : decode7
  port map (SW(3 downto 0), HEX0);
  
end;