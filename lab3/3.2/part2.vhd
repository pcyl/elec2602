LIBRARY ieee;
USE ieee.std_logic_1164.all;

entity part2 is
port (SW: IN std_logic_vector(3 DOWNTO 0);
		HEX0, HEX1: OUT std_logic_vector(0 to 6));
end;

architecture structural of part2 IS
	component mux2 is
		PORT (s, x, y : in std_logic;
						m : out std_logic);					
	END component;

	component compare9 is
		port (v : in std_logic_vector(3 downto 0);
				c : out std_logic);
	end component;
	
	component circuitA is
		port (v : in std_logic_vector(2 downto 0);
				b : out std_logic_vector(3 downto 0));
	end component;
	
	component circuitB is
		port (x : in std_logic;
				d : out std_logic_vector(0 to 6));
	end component;
	
	component decode7 is
		port (s : in std_logic_vector(3 downto 0);
				display : out std_logic_vector(0 to 6));
	end component;

signal z : std_logic;
signal a, m : std_logic_vector(3 downto 0);
begin
	CIRCA : circuitA
	port map (SW(2 downto 0), a);
	COMP : compare9
	port map (SW(3 downto 0), z);
	CIRCB : circuitB
	port map (z, HEX1);
	M0 : mux2
	port map (z, SW(3), '0', m(3));
	M1 : mux2
	port map (z, SW(2), a(2), m(2));
	M2 : mux2
	port map (z, SW(1), a(1), m(1));
	M3 : mux2
	port map (z, SW(0), a(0), m(0));
	DEC : decode7
	port map (m(3 downto 0), HEX0);
end;