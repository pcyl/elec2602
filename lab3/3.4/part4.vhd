library ieee;
use ieee.std_logic_1164.all;

entity part4 is
	port( SW : IN std_logic_vector(8 downto 0);
		LEDR : OUT std_logic_vector(8 downto 0);
		LEDG : OUT std_logic_vector(4 downto 0);
		HEX0, HEX1, HEX4, HEX6 : OUT std_logic_vector(0 to 6));
end;