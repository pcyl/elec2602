library ieee;
use ieee.std_logic_1164.all;

entity comp_bit is
  port (a, b, c : in std_logic;
  cout : out std_logic);
end;

architecture behaviour of comp_bit is
begin
    cout <= (a and b and c) or
            (not a and not b and c);
end;
