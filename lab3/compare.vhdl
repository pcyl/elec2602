library ieee;
use ieee.std_logic_1164.all;

entity compare is
  port (x, y : in std_logic_vector(3 downto 0);
  cout : out std_logic);
end;

architecture behaviour of compare is
  component comp_bit is
    port (a,b,c : in std_logic;
    cout : out std_logic);
  end component;

  signal c : std_logic_vector(4 downto 0);
begin
  c(0) <= '1';
  g0 : for i in 3 down 0 generate
    u : comp_bit port map (x(i), y(i), c(i), c(i+1));
  end generate g0;
  cout <= c(4);
end;
