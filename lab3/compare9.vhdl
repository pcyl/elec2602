library ieee;
use ieee.std_logic_1164.all;

entity compare9 is
  port (v : in std_logic_vector(3 downto 0);
        c : out std_logic);
end;

architecture behaviour of compare9 is
begin
  c <= '0' when v = "0000" else
		 '0' when v = "0001" else
		 '0' when v = "0010" else
		 '0' when v = "0011" else
		 '0' when v = "0100" else
		 '0' when v = "0101" else
		 '0' when v = "0110" else
		 '0' when v = "0111" else
		 '0' when v = "1000" else
		 '0' when v = "1001" else
		 '1' when v = "1010" else
		 '1' when v = "1011" else
		 '1' when v = "1100" else
		 '1' when v = "1101" else
		 '1' when v = "1110" else
		 '1';
end;
