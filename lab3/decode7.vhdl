library ieee;
use ieee.std_logic_1164.all;

entity decode7 is
  port (s : in std_logic_vector(3 downto 0);
  display : out std_logic_vector(0 to 6));
end;

architecture behaviour of decode7 is
begin
  display <= "0000001" when s = "0000" else
             "1001111" when s = "0001" else
             "0010010" when s = "0010" else
             "0000110" when s = "0011" else
             "1001100" when s = "0100" else
             "0100100" when s = "0101" else
             "0100000" when s = "0110" else
             "0001111" when s = "0111" else
             "0000000" when s = "1000" else
             "0000100" when s = "1001" else
             "1111111";
end;
