-- Half adder
library ieee;
use ieee.std_logic.all;

entity ha is
  port (a, b : in std_logic;
  c, s : out std_logic);
end;

architecture behaviour of ha is
  begin
    s <= a xor b;
    c <= a and b;
  end;
