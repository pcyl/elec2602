-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "04/14/2015 18:32:34"

-- 
-- Device: Altera EP4CGX15BF14C6 Package FBGA169
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEIV;
LIBRARY IEEE;
USE CYCLONEIV.CYCLONEIV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	part1 IS
    PORT (
	Clk : IN std_logic;
	R : IN std_logic;
	S : IN std_logic;
	Q : BUFFER std_logic;
	Qnot : BUFFER std_logic
	);
END part1;

-- Design Ports Information
-- Q	=>  Location: PIN_D12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Qnot	=>  Location: PIN_B11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Clk	=>  Location: PIN_K8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R	=>  Location: PIN_A11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- S	=>  Location: PIN_K9,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF part1 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_Clk : std_logic;
SIGNAL ww_R : std_logic;
SIGNAL ww_S : std_logic;
SIGNAL ww_Q : std_logic;
SIGNAL ww_Qnot : std_logic;
SIGNAL \Q~output_o\ : std_logic;
SIGNAL \Qnot~output_o\ : std_logic;
SIGNAL \R~input_o\ : std_logic;
SIGNAL \Clk~input_o\ : std_logic;
SIGNAL \R_g~combout\ : std_logic;
SIGNAL \S~input_o\ : std_logic;
SIGNAL \S_g~combout\ : std_logic;
SIGNAL \Qb~combout\ : std_logic;
SIGNAL \Qa~combout\ : std_logic;

BEGIN

ww_Clk <= Clk;
ww_R <= R;
ww_S <= S;
Q <= ww_Q;
Qnot <= ww_Qnot;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

-- Location: IOOBUF_X33_Y28_N9
\Q~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Qa~combout\,
	devoe => ww_devoe,
	o => \Q~output_o\);

-- Location: IOOBUF_X24_Y31_N2
\Qnot~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Qb~combout\,
	devoe => ww_devoe,
	o => \Qnot~output_o\);

-- Location: IOIBUF_X20_Y31_N1
\R~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_R,
	o => \R~input_o\);

-- Location: IOIBUF_X22_Y0_N8
\Clk~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_Clk,
	o => \Clk~input_o\);

-- Location: LCCOMB_X26_Y15_N12
R_g : cycloneiv_lcell_comb
-- Equation(s):
-- \R_g~combout\ = LCELL((\R~input_o\ & \Clk~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \R~input_o\,
	datad => \Clk~input_o\,
	combout => \R_g~combout\);

-- Location: IOIBUF_X22_Y0_N1
\S~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_S,
	o => \S~input_o\);

-- Location: LCCOMB_X26_Y15_N6
S_g : cycloneiv_lcell_comb
-- Equation(s):
-- \S_g~combout\ = LCELL((\S~input_o\ & \Clk~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \S~input_o\,
	datad => \Clk~input_o\,
	combout => \S_g~combout\);

-- Location: LCCOMB_X26_Y15_N18
Qb : cycloneiv_lcell_comb
-- Equation(s):
-- \Qb~combout\ = LCELL((!\S_g~combout\ & !\Qa~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S_g~combout\,
	datad => \Qa~combout\,
	combout => \Qb~combout\);

-- Location: LCCOMB_X26_Y15_N24
Qa : cycloneiv_lcell_comb
-- Equation(s):
-- \Qa~combout\ = LCELL((!\R_g~combout\ & !\Qb~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000001010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R_g~combout\,
	datad => \Qb~combout\,
	combout => \Qa~combout\);

ww_Q <= \Q~output_o\;

ww_Qnot <= \Qnot~output_o\;
END structure;


