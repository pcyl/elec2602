// Copyright (C) 1991-2013 Altera Corporation
// Your use of Altera Corporation's design tools, logic functions 
// and other software and tools, and its AMPP partner logic 
// functions, and any output files from any of the foregoing 
// (including device programming or simulation files), and any 
// associated documentation or information are expressly subject 
// to the terms and conditions of the Altera Program License 
// Subscription Agreement, Altera MegaCore Function License 
// Agreement, or other applicable license agreement, including, 
// without limitation, that your use is for the sole purpose of 
// programming logic devices manufactured by Altera and sold by 
// Altera or its authorized distributors.  Please refer to the 
// applicable agreement for further details.

// *****************************************************************************
// This file contains a Verilog test bench with test vectors .The test vectors  
// are exported from a vector file in the Quartus Waveform Editor and apply to  
// the top level entity of the current Quartus project .The user can use this   
// testbench to simulate his design using a third-party simulation tool .       
// *****************************************************************************
// Generated on "04/16/2015 12:03:32"
                                                                        
// Verilog Self-Checking Test Bench (with test vectors) for design :    part1
// 
// Simulation tool : 3rd Party
// 

`timescale 1 ps/ 1 ps
module part1_vlg_sample_tst(
	Clk,
	R,
	S,
	sampler_tx
);
input  Clk;
input  R;
input  S;
output sampler_tx;

reg sample;
time current_time;
always @(Clk or R or S)
                                                                                
begin                                                 
 if ($realtime > 0)                                   
 begin                                                
	if ($realtime == 0 || $realtime != current_time)  
	begin									          
		if (sample === 1'bx)                          
			sample = 0;                               
		else                                          
			sample = ~sample;                         
	end										          
	current_time = $realtime;					      
 end                                                  
end                                                   

assign sampler_tx = sample;
endmodule

module part1_vlg_check_tst (
	Q,
	Qnot,
	sampler_rx
);
input  Q;
input  Qnot;
input sampler_rx;

reg  Q_expected;
reg  Qnot_expected;

reg  Q_prev;
reg  Qnot_prev;

reg  Q_expected_prev;
reg  Qnot_expected_prev;

reg  last_Q_exp;
reg  last_Qnot_exp;

reg trigger;

integer i;
integer nummismatches;

reg [1:2] on_first_change ;


initial
begin
trigger = 0;
i = 0;
nummismatches = 0;
on_first_change = 2'b1;
end

// update real /o prevs

always @(trigger)
begin
	Q_prev = Q;
	Qnot_prev = Qnot;
end

// update expected /o prevs

always @(trigger)
begin
	Q_expected_prev = Q_expected;
	Qnot_expected_prev = Qnot_expected;
end



// expected Q
initial
begin
	Q_expected = 1'bX;
end 

// expected Qnot
initial
begin
	Qnot_expected = 1'bX;
end 
// generate trigger
always @(Q_expected or Q or Qnot_expected or Qnot)
begin
	trigger <= ~trigger;
end

always @(posedge sampler_rx or negedge sampler_rx)
begin
`ifdef debug_tbench
	$display("Scanning pattern %d @time = %t",i,$realtime );
	i = i + 1;
	$display("| expected Q = %b | expected Qnot = %b | ",Q_expected_prev,Qnot_expected_prev);
	$display("| real Q = %b | real Qnot = %b | ",Q_prev,Qnot_prev);
`endif
	if (
		( Q_expected_prev !== 1'bx ) && ( Q_prev !== Q_expected_prev )
		&& ((Q_expected_prev !== last_Q_exp) ||
			on_first_change[1])
	)
	begin
		$display ("ERROR! Vector Mismatch for output port Q :: @time = %t",  $realtime);
		$display ("     Expected value = %b", Q_expected_prev);
		$display ("     Real value = %b", Q_prev);
		nummismatches = nummismatches + 1;
		on_first_change[1] = 1'b0;
		last_Q_exp = Q_expected_prev;
	end
	if (
		( Qnot_expected_prev !== 1'bx ) && ( Qnot_prev !== Qnot_expected_prev )
		&& ((Qnot_expected_prev !== last_Qnot_exp) ||
			on_first_change[2])
	)
	begin
		$display ("ERROR! Vector Mismatch for output port Qnot :: @time = %t",  $realtime);
		$display ("     Expected value = %b", Qnot_expected_prev);
		$display ("     Real value = %b", Qnot_prev);
		nummismatches = nummismatches + 1;
		on_first_change[2] = 1'b0;
		last_Qnot_exp = Qnot_expected_prev;
	end

	trigger <= ~trigger;
end
initial 

begin 
$timeformat(-12,3," ps",6);
#1000000;
if (nummismatches > 0)
	$display ("%d mismatched vectors : Simulation failed !",nummismatches);
else
	$display ("Simulation passed !");
$finish;
end 
endmodule

module part1_vlg_vec_tst();
// constants                                           
// general purpose registers
reg Clk;
reg R;
reg S;
// wires                                               
wire Q;
wire Qnot;

wire sampler;                             

// assign statements (if any)                          
part1 i1 (
// port map - connection between master ports and signals/registers   
	.Clk(Clk),
	.Q(Q),
	.Qnot(Qnot),
	.R(R),
	.S(S)
);

// Clk
initial
begin
	Clk = 1'b0;
	Clk = #30000 1'b1;
	Clk = #10000 1'b0;
	Clk = #30000 1'b1;
	Clk = #10000 1'b0;
	Clk = #30000 1'b1;
	Clk = #10000 1'b0;
	Clk = #30000 1'b1;
	Clk = #10000 1'b0;
end 

// R
initial
begin
	R = 1'b0;
	R = #60000 1'b1;
	R = #30000 1'b0;
	R = #40000 1'b1;
	R = #10000 1'b0;
	R = #50000 1'b1;
	R = #10000 1'b0;
end 

// S
initial
begin
	S = 1'b1;
	S = #50000 1'b0;
	S = #80000 1'b1;
	S = #40000 1'b0;
	S = #20000 1'b1;
	S = #10000 1'b0;
end 

part1_vlg_sample_tst tb_sample (
	.Clk(Clk),
	.R(R),
	.S(S),
	.sampler_tx(sampler)
);

part1_vlg_check_tst tb_out(
	.Q(Q),
	.Qnot(Qnot),
	.sampler_rx(sampler)
);
endmodule

