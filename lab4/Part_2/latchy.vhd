LIBRARY ieee;
USE ieee.std_logic_1164.all;


ENTITY latchy IS
 PORT ( Clk, D : IN STD_LOGIC; 
	Q, Qnot : OUT STD_LOGIC);
END latchy;



ARCHITECTURE Structural OF latchy IS
 SIGNAL S, R, S_g, R_g, Qa, Qb : STD_LOGIC ;
 ATTRIBUTE keep : boolean;
 ATTRIBUTE keep of S, R, R_g, S_g, Qa, Qb : SIGNAL IS true;
BEGIN
 S <= D;
 R <= NOT D;
 S_g <= NOT (S AND Clk);
 R_g <= NOT (R AND Clk);
 Qa <= NOT (S_g AND Qb);
 Qb <= NOT (R_g AND Qa);
 Q <= Qa;
 Qnot <= Qb;
END Structural;