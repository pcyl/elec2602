LIBRARY ieee;
USE ieee.std_logic_1164.all;


ENTITY part2 IS
 PORT ( SW : IN STD_LOGIC_VECTOR(1 DOWNTO 0); 
	LEDR : OUT STD_LOGIC_VECTOR(1 DOWNTO 0));
END part2;



ARCHITECTURE Structural OF part2 IS
 
 COMPONENT latchy IS
 PORT (Clk, D : IN STD_LOGIC; 
	Q, Qnot : OUT STD_LOGIC);
	end component;
	
BEGIN
	atch : latchy port map(SW(1), SW(0), LEDR(0));
END Structural;