-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "04/14/2015 19:40:40"

-- 
-- Device: Altera EP2AGX45CU17I3 Package UFBGA358
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ARRIAII;
LIBRARY IEEE;
USE ARRIAII.ARRIAII_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	part2 IS
    PORT (
	SW : IN std_logic_vector(1 DOWNTO 0);
	LEDR : OUT std_logic_vector(1 DOWNTO 0)
	);
END part2;

-- Design Ports Information
-- LEDR[0]	=>  Location: PIN_C14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[1]	=>  Location: PIN_F2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[1]	=>  Location: PIN_D12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[0]	=>  Location: PIN_C13,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF part2 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_SW : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_LEDR : std_logic_vector(1 DOWNTO 0);
SIGNAL \LEDR[0]~output_o\ : std_logic;
SIGNAL \LEDR[1]~output_o\ : std_logic;
SIGNAL \SW[1]~input_o\ : std_logic;
SIGNAL \SW[0]~input_o\ : std_logic;
SIGNAL \atch|S~combout\ : std_logic;
SIGNAL \atch|S_g~combout\ : std_logic;
SIGNAL \atch|R~combout\ : std_logic;
SIGNAL \atch|R_g~combout\ : std_logic;
SIGNAL \atch|Qb~combout\ : std_logic;
SIGNAL \atch|Qa~combout\ : std_logic;
SIGNAL \ALT_INV_SW[0]~input_o\ : std_logic;
SIGNAL \ALT_INV_SW[1]~input_o\ : std_logic;
SIGNAL \atch|ALT_INV_R~combout\ : std_logic;
SIGNAL \atch|ALT_INV_S~combout\ : std_logic;
SIGNAL \atch|ALT_INV_R_g~combout\ : std_logic;
SIGNAL \atch|ALT_INV_Qb~combout\ : std_logic;
SIGNAL \atch|ALT_INV_S_g~combout\ : std_logic;
SIGNAL \atch|ALT_INV_Qa~combout\ : std_logic;

BEGIN

ww_SW <= SW;
LEDR <= ww_LEDR;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_SW[0]~input_o\ <= NOT \SW[0]~input_o\;
\ALT_INV_SW[1]~input_o\ <= NOT \SW[1]~input_o\;
\atch|ALT_INV_R~combout\ <= NOT \atch|R~combout\;
\atch|ALT_INV_S~combout\ <= NOT \atch|S~combout\;
\atch|ALT_INV_R_g~combout\ <= NOT \atch|R_g~combout\;
\atch|ALT_INV_Qb~combout\ <= NOT \atch|Qb~combout\;
\atch|ALT_INV_S_g~combout\ <= NOT \atch|S_g~combout\;
\atch|ALT_INV_Qa~combout\ <= NOT \atch|Qa~combout\;

-- Location: IOOBUF_X7_Y56_N2
\LEDR[0]~output\ : arriaii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \atch|Qa~combout\,
	devoe => ww_devoe,
	o => \LEDR[0]~output_o\);

-- Location: IOOBUF_X56_Y56_N95
\LEDR[1]~output\ : arriaii_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDR[1]~output_o\);

-- Location: IOIBUF_X7_Y56_N94
\SW[1]~input\ : arriaii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(1),
	o => \SW[1]~input_o\);

-- Location: IOIBUF_X7_Y56_N63
\SW[0]~input\ : arriaii_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(0),
	o => \SW[0]~input_o\);

-- Location: LABCELL_X7_Y55_N10
\atch|S\ : arriaii_lcell_comb
-- Equation(s):
-- \atch|S~combout\ = LCELL(\SW[0]~input_o\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "0101010101010101010101010101010101010101010101010101010101010101",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_SW[0]~input_o\,
	combout => \atch|S~combout\);

-- Location: LABCELL_X7_Y55_N4
\atch|S_g\ : arriaii_lcell_comb
-- Equation(s):
-- \atch|S_g~combout\ = LCELL(( \atch|S~combout\ & ( !\SW[1]~input_o\ ) ) # ( !\atch|S~combout\ ))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111111111111111111111111111001100110011001100110011001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_SW[1]~input_o\,
	dataf => \atch|ALT_INV_S~combout\,
	combout => \atch|S_g~combout\);

-- Location: LABCELL_X7_Y55_N8
\atch|R\ : arriaii_lcell_comb
-- Equation(s):
-- \atch|R~combout\ = LCELL(!\SW[0]~input_o\)

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1010101010101010101010101010101010101010101010101010101010101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \ALT_INV_SW[0]~input_o\,
	combout => \atch|R~combout\);

-- Location: LABCELL_X7_Y55_N6
\atch|R_g\ : arriaii_lcell_comb
-- Equation(s):
-- \atch|R_g~combout\ = LCELL((!\SW[1]~input_o\) # (!\atch|R~combout\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111001100111111111100110011111111110011001111111111001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \ALT_INV_SW[1]~input_o\,
	datad => \atch|ALT_INV_R~combout\,
	combout => \atch|R_g~combout\);

-- Location: LABCELL_X7_Y55_N20
\atch|Qb\ : arriaii_lcell_comb
-- Equation(s):
-- \atch|Qb~combout\ = LCELL((!\atch|R_g~combout\) # (!\atch|Qa~combout\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111111001100111111111100110011111111110011001111111111001100",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	datab => \atch|ALT_INV_R_g~combout\,
	datad => \atch|ALT_INV_Qa~combout\,
	combout => \atch|Qb~combout\);

-- Location: LABCELL_X7_Y55_N22
\atch|Qa\ : arriaii_lcell_comb
-- Equation(s):
-- \atch|Qa~combout\ = LCELL((!\atch|S_g~combout\) # (!\atch|Qb~combout\))

-- pragma translate_off
GENERIC MAP (
	extended_lut => "off",
	lut_mask => "1111111110101010111111111010101011111111101010101111111110101010",
	shared_arith => "off")
-- pragma translate_on
PORT MAP (
	dataa => \atch|ALT_INV_S_g~combout\,
	datad => \atch|ALT_INV_Qb~combout\,
	combout => \atch|Qa~combout\);

ww_LEDR(0) <= \LEDR[0]~output_o\;

ww_LEDR(1) <= \LEDR[1]~output_o\;
END structure;


