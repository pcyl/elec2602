-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "04/14/2015 18:43:35"

-- 
-- Device: Altera EP4CGX15BF14C6 Package FBGA169
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEIV;
LIBRARY IEEE;
USE CYCLONEIV.CYCLONEIV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	part2 IS
    PORT (
	Clk : IN std_logic;
	D : IN std_logic;
	SW0 : IN std_logic;
	SW1 : IN std_logic;
	Q : BUFFER std_logic;
	Qnot : BUFFER std_logic;
	LEDR0 : BUFFER std_logic
	);
END part2;

-- Design Ports Information
-- SW0	=>  Location: PIN_H10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW1	=>  Location: PIN_N11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q	=>  Location: PIN_B6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Qnot	=>  Location: PIN_A7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR0	=>  Location: PIN_M11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Clk	=>  Location: PIN_M6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D	=>  Location: PIN_C8,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF part2 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_Clk : std_logic;
SIGNAL ww_D : std_logic;
SIGNAL ww_SW0 : std_logic;
SIGNAL ww_SW1 : std_logic;
SIGNAL ww_Q : std_logic;
SIGNAL ww_Qnot : std_logic;
SIGNAL ww_LEDR0 : std_logic;
SIGNAL \SW0~input_o\ : std_logic;
SIGNAL \SW1~input_o\ : std_logic;
SIGNAL \Q~output_o\ : std_logic;
SIGNAL \Qnot~output_o\ : std_logic;
SIGNAL \LEDR0~output_o\ : std_logic;
SIGNAL \Clk~input_o\ : std_logic;
SIGNAL \D~input_o\ : std_logic;
SIGNAL \S~combout\ : std_logic;
SIGNAL \S_g~combout\ : std_logic;
SIGNAL \R~combout\ : std_logic;
SIGNAL \R_g~combout\ : std_logic;
SIGNAL \Qb~combout\ : std_logic;
SIGNAL \Qa~combout\ : std_logic;

BEGIN

ww_Clk <= Clk;
ww_D <= D;
ww_SW0 <= SW0;
ww_SW1 <= SW1;
Q <= ww_Q;
Qnot <= ww_Qnot;
LEDR0 <= ww_LEDR0;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

-- Location: IOOBUF_X14_Y31_N9
\Q~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Qa~combout\,
	devoe => ww_devoe,
	o => \Q~output_o\);

-- Location: IOOBUF_X12_Y31_N2
\Qnot~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Qb~combout\,
	devoe => ww_devoe,
	o => \Qnot~output_o\);

-- Location: IOOBUF_X29_Y0_N9
\LEDR0~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDR0~output_o\);

-- Location: IOIBUF_X12_Y0_N8
\Clk~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_Clk,
	o => \Clk~input_o\);

-- Location: IOIBUF_X22_Y31_N8
\D~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D,
	o => \D~input_o\);

-- Location: LCCOMB_X14_Y30_N24
S : cycloneiv_lcell_comb
-- Equation(s):
-- \S~combout\ = LCELL(\D~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \D~input_o\,
	combout => \S~combout\);

-- Location: LCCOMB_X14_Y30_N12
S_g : cycloneiv_lcell_comb
-- Equation(s):
-- \S_g~combout\ = LCELL((!\S~combout\) # (!\Clk~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Clk~input_o\,
	datad => \S~combout\,
	combout => \S_g~combout\);

-- Location: LCCOMB_X14_Y30_N10
R : cycloneiv_lcell_comb
-- Equation(s):
-- \R~combout\ = LCELL(!\D~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \D~input_o\,
	combout => \R~combout\);

-- Location: LCCOMB_X14_Y30_N6
R_g : cycloneiv_lcell_comb
-- Equation(s):
-- \R_g~combout\ = LCELL((!\R~combout\) # (!\Clk~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Clk~input_o\,
	datad => \R~combout\,
	combout => \R_g~combout\);

-- Location: LCCOMB_X14_Y30_N2
Qb : cycloneiv_lcell_comb
-- Equation(s):
-- \Qb~combout\ = LCELL((!\Qa~combout\) # (!\R_g~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \R_g~combout\,
	datad => \Qa~combout\,
	combout => \Qb~combout\);

-- Location: LCCOMB_X14_Y30_N0
Qa : cycloneiv_lcell_comb
-- Equation(s):
-- \Qa~combout\ = LCELL((!\Qb~combout\) # (!\S_g~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \S_g~combout\,
	datad => \Qb~combout\,
	combout => \Qa~combout\);

-- Location: IOIBUF_X33_Y14_N1
\SW0~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW0,
	o => \SW0~input_o\);

-- Location: IOIBUF_X26_Y0_N1
\SW1~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW1,
	o => \SW1~input_o\);

ww_Q <= \Q~output_o\;

ww_Qnot <= \Qnot~output_o\;

ww_LEDR0 <= \LEDR0~output_o\;
END structure;


