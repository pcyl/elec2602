LIBRARY ieee;
USE ieee.std_logic_1164.all;


ENTITY part3 IS
 PORT ( SW : IN STD_LOGIC_VECTOR(1 DOWNTO 0); 
	LEDR : OUT STD_LOGIC_VECTOR(1 DOWNTO 0));
END part3;



ARCHITECTURE Structural OF part3 IS
 
 COMPONENT latchy IS
 PORT (Clk, D : IN STD_LOGIC; 
	Q, Qnot : OUT STD_LOGIC);
	end component;
	
	SIGNAL Qm : STD_LOGIC ;
	ATTRIBUTE keep : boolean;
	ATTRIBUTE keep of Qm : SIGNAL IS true;
	
BEGIN
	Master : latchy port map( NOT (SW(1)), SW(0), Qm);
	Slave : latchy port map(SW(1), Qm , LEDR(0));
END Structural;