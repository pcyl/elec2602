-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "04/14/2015 19:46:56"

-- 
-- Device: Altera EP4CGX15BF14C6 Package FBGA169
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEIV;
LIBRARY IEEE;
USE CYCLONEIV.CYCLONEIV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	part3 IS
    PORT (
	SW : IN std_logic_vector(1 DOWNTO 0);
	LEDR : OUT std_logic_vector(1 DOWNTO 0)
	);
END part3;

-- Design Ports Information
-- LEDR[0]	=>  Location: PIN_M6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[1]	=>  Location: PIN_M13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[1]	=>  Location: PIN_L4,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[0]	=>  Location: PIN_N6,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF part3 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_SW : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_LEDR : std_logic_vector(1 DOWNTO 0);
SIGNAL \LEDR[0]~output_o\ : std_logic;
SIGNAL \LEDR[1]~output_o\ : std_logic;
SIGNAL \SW[1]~input_o\ : std_logic;
SIGNAL \SW[0]~input_o\ : std_logic;
SIGNAL \Master|R~combout\ : std_logic;
SIGNAL \Master|R_g~combout\ : std_logic;
SIGNAL \Master|Qb~combout\ : std_logic;
SIGNAL \Master|S~combout\ : std_logic;
SIGNAL \Master|S_g~combout\ : std_logic;
SIGNAL \Master|Qa~combout\ : std_logic;
SIGNAL \Qm~combout\ : std_logic;
SIGNAL \Slave|S~combout\ : std_logic;
SIGNAL \Slave|S_g~combout\ : std_logic;
SIGNAL \Slave|R~combout\ : std_logic;
SIGNAL \Slave|R_g~combout\ : std_logic;
SIGNAL \Slave|Qb~combout\ : std_logic;
SIGNAL \Slave|Qa~combout\ : std_logic;

BEGIN

ww_SW <= SW;
LEDR <= ww_LEDR;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

-- Location: IOOBUF_X12_Y0_N9
\LEDR[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Slave|Qa~combout\,
	devoe => ww_devoe,
	o => \LEDR[0]~output_o\);

-- Location: IOOBUF_X33_Y10_N2
\LEDR[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => GND,
	devoe => ww_devoe,
	o => \LEDR[1]~output_o\);

-- Location: IOIBUF_X8_Y0_N8
\SW[1]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(1),
	o => \SW[1]~input_o\);

-- Location: IOIBUF_X12_Y0_N1
\SW[0]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(0),
	o => \SW[0]~input_o\);

-- Location: LCCOMB_X12_Y1_N8
\Master|R\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Master|R~combout\ = LCELL(!\SW[0]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \SW[0]~input_o\,
	combout => \Master|R~combout\);

-- Location: LCCOMB_X12_Y1_N6
\Master|R_g\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Master|R_g~combout\ = LCELL((\SW[1]~input_o\) # (!\Master|R~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SW[1]~input_o\,
	datac => \Master|R~combout\,
	combout => \Master|R_g~combout\);

-- Location: LCCOMB_X12_Y1_N12
\Master|Qb\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Master|Qb~combout\ = LCELL((!\Master|R_g~combout\) # (!\Master|Qa~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Master|Qa~combout\,
	datad => \Master|R_g~combout\,
	combout => \Master|Qb~combout\);

-- Location: LCCOMB_X12_Y1_N30
\Master|S\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Master|S~combout\ = LCELL(\SW[0]~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \SW[0]~input_o\,
	combout => \Master|S~combout\);

-- Location: LCCOMB_X12_Y1_N10
\Master|S_g\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Master|S_g~combout\ = LCELL((\SW[1]~input_o\) # (!\Master|S~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SW[1]~input_o\,
	datac => \Master|S~combout\,
	combout => \Master|S_g~combout\);

-- Location: LCCOMB_X12_Y1_N16
\Master|Qa\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Master|Qa~combout\ = LCELL((!\Master|S_g~combout\) # (!\Master|Qb~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Master|Qb~combout\,
	datad => \Master|S_g~combout\,
	combout => \Master|Qa~combout\);

-- Location: LCCOMB_X12_Y1_N26
Qm : cycloneiv_lcell_comb
-- Equation(s):
-- \Qm~combout\ = LCELL(\Master|Qa~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \Master|Qa~combout\,
	combout => \Qm~combout\);

-- Location: LCCOMB_X12_Y1_N24
\Slave|S\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Slave|S~combout\ = LCELL(\Qm~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Qm~combout\,
	combout => \Slave|S~combout\);

-- Location: LCCOMB_X12_Y1_N18
\Slave|S_g\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Slave|S_g~combout\ = LCELL((!\Slave|S~combout\) # (!\SW[1]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SW[1]~input_o\,
	datad => \Slave|S~combout\,
	combout => \Slave|S_g~combout\);

-- Location: LCCOMB_X12_Y1_N20
\Slave|R\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Slave|R~combout\ = LCELL(!\Qm~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Qm~combout\,
	combout => \Slave|R~combout\);

-- Location: LCCOMB_X12_Y1_N22
\Slave|R_g\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Slave|R_g~combout\ = LCELL((!\Slave|R~combout\) # (!\SW[1]~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SW[1]~input_o\,
	datad => \Slave|R~combout\,
	combout => \Slave|R_g~combout\);

-- Location: LCCOMB_X12_Y1_N28
\Slave|Qb\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Slave|Qb~combout\ = LCELL((!\Slave|Qa~combout\) # (!\Slave|R_g~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Slave|R_g~combout\,
	datad => \Slave|Qa~combout\,
	combout => \Slave|Qb~combout\);

-- Location: LCCOMB_X12_Y1_N0
\Slave|Qa\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Slave|Qa~combout\ = LCELL((!\Slave|Qb~combout\) # (!\Slave|S_g~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Slave|S_g~combout\,
	datad => \Slave|Qb~combout\,
	combout => \Slave|Qa~combout\);

ww_LEDR(0) <= \LEDR[0]~output_o\;

ww_LEDR(1) <= \LEDR[1]~output_o\;
END structure;


