-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Web Edition"

-- DATE "04/16/2015 12:50:56"

-- 
-- Device: Altera EP2C35F672C6 Package FBGA672
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEII;
LIBRARY IEEE;
USE CYCLONEII.CYCLONEII_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	part3 IS
    PORT (
	SW : IN std_logic_vector(1 DOWNTO 0);
	LEDR : OUT std_logic_vector(1 DOWNTO 0)
	);
END part3;

-- Design Ports Information
-- LEDR[0]	=>  Location: PIN_AE23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[1]	=>  Location: PIN_AF23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- SW[1]	=>  Location: PIN_N26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[0]	=>  Location: PIN_N25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default


ARCHITECTURE structure OF part3 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_SW : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_LEDR : std_logic_vector(1 DOWNTO 0);
SIGNAL \Master|S~combout\ : std_logic;
SIGNAL \Master|S_g~combout\ : std_logic;
SIGNAL \Master|R~combout\ : std_logic;
SIGNAL \Master|R_g~combout\ : std_logic;
SIGNAL \Master|Qb~combout\ : std_logic;
SIGNAL \Master|Qa~combout\ : std_logic;
SIGNAL \Qm~combout\ : std_logic;
SIGNAL \Slave|R~combout\ : std_logic;
SIGNAL \Slave|R_g~combout\ : std_logic;
SIGNAL \Slave|Qb~combout\ : std_logic;
SIGNAL \Slave|S~combout\ : std_logic;
SIGNAL \Slave|S_g~combout\ : std_logic;
SIGNAL \Slave|Qa~combout\ : std_logic;
SIGNAL \SW~combout\ : std_logic_vector(1 DOWNTO 0);

BEGIN

ww_SW <= SW;
LEDR <= ww_LEDR;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

-- Location: LCCOMB_X64_Y19_N6
\Master|S\ : cycloneii_lcell_comb
-- Equation(s):
-- \Master|S~combout\ = LCELL(\SW~combout\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \SW~combout\(0),
	combout => \Master|S~combout\);

-- Location: PIN_N26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(1),
	combout => \SW~combout\(1));

-- Location: LCCOMB_X64_Y19_N2
\Master|S_g\ : cycloneii_lcell_comb
-- Equation(s):
-- \Master|S_g~combout\ = LCELL((\SW~combout\(1)) # (!\Master|S~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111101010101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Master|S~combout\,
	datad => \SW~combout\(1),
	combout => \Master|S_g~combout\);

-- Location: PIN_N25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(0),
	combout => \SW~combout\(0));

-- Location: LCCOMB_X64_Y19_N8
\Master|R\ : cycloneii_lcell_comb
-- Equation(s):
-- \Master|R~combout\ = LCELL(!\SW~combout\(0))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \SW~combout\(0),
	combout => \Master|R~combout\);

-- Location: LCCOMB_X64_Y19_N22
\Master|R_g\ : cycloneii_lcell_comb
-- Equation(s):
-- \Master|R_g~combout\ = LCELL((\SW~combout\(1)) # (!\Master|R~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100001111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Master|R~combout\,
	datad => \SW~combout\(1),
	combout => \Master|R_g~combout\);

-- Location: LCCOMB_X64_Y19_N28
\Master|Qb\ : cycloneii_lcell_comb
-- Equation(s):
-- \Master|Qb~combout\ = LCELL((!\Master|R_g~combout\) # (!\Master|Qa~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Master|Qa~combout\,
	datad => \Master|R_g~combout\,
	combout => \Master|Qb~combout\);

-- Location: LCCOMB_X64_Y19_N24
\Master|Qa\ : cycloneii_lcell_comb
-- Equation(s):
-- \Master|Qa~combout\ = LCELL((!\Master|Qb~combout\) # (!\Master|S_g~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Master|S_g~combout\,
	datad => \Master|Qb~combout\,
	combout => \Master|Qa~combout\);

-- Location: LCCOMB_X64_Y19_N18
Qm : cycloneii_lcell_comb
-- Equation(s):
-- \Qm~combout\ = LCELL(\Master|Qa~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Master|Qa~combout\,
	combout => \Qm~combout\);

-- Location: LCCOMB_X64_Y19_N20
\Slave|R\ : cycloneii_lcell_comb
-- Equation(s):
-- \Slave|R~combout\ = LCELL(!\Qm~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000000011111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \Qm~combout\,
	combout => \Slave|R~combout\);

-- Location: LCCOMB_X64_Y19_N30
\Slave|R_g\ : cycloneii_lcell_comb
-- Equation(s):
-- \Slave|R_g~combout\ = LCELL((!\SW~combout\(1)) # (!\Slave|R~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Slave|R~combout\,
	datad => \SW~combout\(1),
	combout => \Slave|R_g~combout\);

-- Location: LCCOMB_X64_Y19_N12
\Slave|Qb\ : cycloneii_lcell_comb
-- Equation(s):
-- \Slave|Qb~combout\ = LCELL((!\Slave|Qa~combout\) # (!\Slave|R_g~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011001111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \Slave|R_g~combout\,
	datad => \Slave|Qa~combout\,
	combout => \Slave|Qb~combout\);

-- Location: LCCOMB_X64_Y19_N16
\Slave|S\ : cycloneii_lcell_comb
-- Equation(s):
-- \Slave|S~combout\ = LCELL(\Qm~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datad => \Qm~combout\,
	combout => \Slave|S~combout\);

-- Location: LCCOMB_X64_Y19_N10
\Slave|S_g\ : cycloneii_lcell_comb
-- Equation(s):
-- \Slave|S_g~combout\ = LCELL((!\SW~combout\(1)) # (!\Slave|S~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Slave|S~combout\,
	datad => \SW~combout\(1),
	combout => \Slave|S_g~combout\);

-- Location: LCCOMB_X64_Y19_N0
\Slave|Qa\ : cycloneii_lcell_comb
-- Equation(s):
-- \Slave|Qa~combout\ = LCELL((!\Slave|S_g~combout\) # (!\Slave|Qb~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101010111111111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Slave|Qb~combout\,
	datad => \Slave|S_g~combout\,
	combout => \Slave|Qa~combout\);

-- Location: PIN_AE23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \Slave|Qa~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(0));

-- Location: PIN_AF23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(1));
END structure;


