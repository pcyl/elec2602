LIBRARY ieee;
USE ieee.std_logic_1164.all;
ENTITY latcher IS

 PORT ( Clk, D : IN STD_LOGIC;
 Q, Qnot : OUT STD_LOGIC);
END latcher;


ARCHITECTURE Behavior OF latcher IS
BEGIN

	PROCESS (D, Clk)
	BEGIN
		if (Clk = '1') then
		Q   <= D ;
		Qnot <= NOT D;
	end if;
	end process;
	
END Behavior; 
