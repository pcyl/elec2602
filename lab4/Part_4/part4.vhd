LIBRARY ieee;
USE ieee.std_logic_1164.all;


ENTITY part4 IS
 PORT ( D, Clk : in std_logic;
	Qa, Qb, Qc : out std_logic);
END part4;



ARCHITECTURE Structural OF part4 IS
 
 COMPONENT latcher IS
 PORT (Clk, D : IN STD_LOGIC; 
	Q, Qnot : OUT STD_LOGIC);
	end component;
 
 COMPONENT neglatch IS
 PORT (Clk, D : IN STD_LOGIC; 
	Q, Qnot : OUT STD_LOGIC);
	end component;
 
 COMPONENT poslatch IS
 PORT (Clk, D : IN STD_LOGIC; 
	Q, Qnot : OUT STD_LOGIC);
	end component;
	
BEGIN
	latch1 : latcher port map(Clk, D, Qa);
	latch2 : poslatch port map(Clk, D, Qb);
	latch3 : neglatch port map(Clk, D, Qc);
END Structural;