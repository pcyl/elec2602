-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "04/14/2015 21:18:20"

-- 
-- Device: Altera EP4CGX15BF14C6 Package FBGA169
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEIV;
LIBRARY IEEE;
USE CYCLONEIV.CYCLONEIV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	part4 IS
    PORT (
	D : IN std_logic;
	Clk : IN std_logic;
	Qa : BUFFER std_logic;
	Qb : BUFFER std_logic;
	Qc : BUFFER std_logic
	);
END part4;

-- Design Ports Information
-- Qa	=>  Location: PIN_A8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Qb	=>  Location: PIN_A6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Qc	=>  Location: PIN_B6,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- D	=>  Location: PIN_A7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Clk	=>  Location: PIN_J7,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF part4 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_D : std_logic;
SIGNAL ww_Clk : std_logic;
SIGNAL ww_Qa : std_logic;
SIGNAL ww_Qb : std_logic;
SIGNAL ww_Qc : std_logic;
SIGNAL \Clk~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \Qa~output_o\ : std_logic;
SIGNAL \Qb~output_o\ : std_logic;
SIGNAL \Qc~output_o\ : std_logic;
SIGNAL \D~input_o\ : std_logic;
SIGNAL \Clk~input_o\ : std_logic;
SIGNAL \Clk~inputclkctrl_outclk\ : std_logic;
SIGNAL \latch1|Q~combout\ : std_logic;
SIGNAL \latch2|Qm~combout\ : std_logic;
SIGNAL \latch2|Q~combout\ : std_logic;
SIGNAL \latch3|Q~combout\ : std_logic;

BEGIN

ww_D <= D;
ww_Clk <= Clk;
Qa <= ww_Qa;
Qb <= ww_Qb;
Qc <= ww_Qc;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\Clk~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \Clk~input_o\);

-- Location: IOOBUF_X12_Y31_N9
\Qa~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \latch1|Q~combout\,
	devoe => ww_devoe,
	o => \Qa~output_o\);

-- Location: IOOBUF_X10_Y31_N2
\Qb~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \latch2|Q~combout\,
	devoe => ww_devoe,
	o => \Qb~output_o\);

-- Location: IOOBUF_X14_Y31_N9
\Qc~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \latch3|Q~combout\,
	devoe => ww_devoe,
	o => \Qc~output_o\);

-- Location: IOIBUF_X12_Y31_N1
\D~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_D,
	o => \D~input_o\);

-- Location: IOIBUF_X16_Y0_N15
\Clk~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_Clk,
	o => \Clk~input_o\);

-- Location: CLKCTRL_G17
\Clk~inputclkctrl\ : cycloneiv_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \Clk~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \Clk~inputclkctrl_outclk\);

-- Location: LCCOMB_X12_Y30_N20
\latch1|Q\ : cycloneiv_lcell_comb
-- Equation(s):
-- \latch1|Q~combout\ = (GLOBAL(\Clk~inputclkctrl_outclk\) & (\D~input_o\)) # (!GLOBAL(\Clk~inputclkctrl_outclk\) & ((\latch1|Q~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \D~input_o\,
	datac => \latch1|Q~combout\,
	datad => \Clk~inputclkctrl_outclk\,
	combout => \latch1|Q~combout\);

-- Location: LCCOMB_X12_Y30_N18
\latch2|Qm\ : cycloneiv_lcell_comb
-- Equation(s):
-- \latch2|Qm~combout\ = (GLOBAL(\Clk~inputclkctrl_outclk\) & ((\latch2|Qm~combout\))) # (!GLOBAL(\Clk~inputclkctrl_outclk\) & (\D~input_o\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \D~input_o\,
	datac => \latch2|Qm~combout\,
	datad => \Clk~inputclkctrl_outclk\,
	combout => \latch2|Qm~combout\);

-- Location: LCCOMB_X12_Y30_N30
\latch2|Q\ : cycloneiv_lcell_comb
-- Equation(s):
-- \latch2|Q~combout\ = (GLOBAL(\Clk~inputclkctrl_outclk\) & ((\latch2|Qm~combout\))) # (!GLOBAL(\Clk~inputclkctrl_outclk\) & (\latch2|Q~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111101000001010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch2|Q~combout\,
	datac => \Clk~inputclkctrl_outclk\,
	datad => \latch2|Qm~combout\,
	combout => \latch2|Q~combout\);

-- Location: LCCOMB_X12_Y30_N28
\latch3|Q\ : cycloneiv_lcell_comb
-- Equation(s):
-- \latch3|Q~combout\ = (GLOBAL(\Clk~inputclkctrl_outclk\) & (\latch3|Q~combout\)) # (!GLOBAL(\Clk~inputclkctrl_outclk\) & ((\latch1|Q~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \latch3|Q~combout\,
	datac => \Clk~inputclkctrl_outclk\,
	datad => \latch1|Q~combout\,
	combout => \latch3|Q~combout\);

ww_Qa <= \Qa~output_o\;

ww_Qb <= \Qb~output_o\;

ww_Qc <= \Qc~output_o\;
END structure;


