-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Web Edition"

-- DATE "04/17/2015 18:21:47"

-- 
-- Device: Altera EP2C35F672C6 Package FBGA672
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEII;
LIBRARY IEEE;
USE CYCLONEII.CYCLONEII_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	part5 IS
    PORT (
	SW : IN std_logic_vector(15 DOWNTO 0);
	KEY : IN std_logic_vector(1 DOWNTO 0);
	HEX0 : OUT std_logic_vector(0 TO 6);
	HEX1 : OUT std_logic_vector(0 TO 6);
	HEX2 : OUT std_logic_vector(0 TO 6);
	HEX3 : OUT std_logic_vector(0 TO 6);
	HEX4 : OUT std_logic_vector(0 TO 6);
	HEX5 : OUT std_logic_vector(0 TO 6);
	HEX6 : OUT std_logic_vector(0 TO 6);
	HEX7 : OUT std_logic_vector(0 TO 6)
	);
END part5;

-- Design Ports Information
-- KEY[1]	=>  Location: PIN_N23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- HEX0[6]	=>  Location: PIN_V13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[5]	=>  Location: PIN_V14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[4]	=>  Location: PIN_AE11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[3]	=>  Location: PIN_AD11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[2]	=>  Location: PIN_AC12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[1]	=>  Location: PIN_AB12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[0]	=>  Location: PIN_AF10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[6]	=>  Location: PIN_AB24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[5]	=>  Location: PIN_AA23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[4]	=>  Location: PIN_AA24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[3]	=>  Location: PIN_Y22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[2]	=>  Location: PIN_W21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[1]	=>  Location: PIN_V21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[0]	=>  Location: PIN_V20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[6]	=>  Location: PIN_Y24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[5]	=>  Location: PIN_AB25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[4]	=>  Location: PIN_AB26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[3]	=>  Location: PIN_AC26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[2]	=>  Location: PIN_AC25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[1]	=>  Location: PIN_V22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[0]	=>  Location: PIN_AB23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[6]	=>  Location: PIN_W24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[5]	=>  Location: PIN_U22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[4]	=>  Location: PIN_Y25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[3]	=>  Location: PIN_Y26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[2]	=>  Location: PIN_AA26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[1]	=>  Location: PIN_AA25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[0]	=>  Location: PIN_Y23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX4[6]	=>  Location: PIN_T3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX4[5]	=>  Location: PIN_R6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX4[4]	=>  Location: PIN_R7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX4[3]	=>  Location: PIN_T4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX4[2]	=>  Location: PIN_U2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX4[1]	=>  Location: PIN_U1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX4[0]	=>  Location: PIN_U9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX5[6]	=>  Location: PIN_R3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX5[5]	=>  Location: PIN_R4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX5[4]	=>  Location: PIN_R5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX5[3]	=>  Location: PIN_T9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX5[2]	=>  Location: PIN_P7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX5[1]	=>  Location: PIN_P6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX5[0]	=>  Location: PIN_T2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX6[6]	=>  Location: PIN_M4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX6[5]	=>  Location: PIN_M5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX6[4]	=>  Location: PIN_M3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX6[3]	=>  Location: PIN_M2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX6[2]	=>  Location: PIN_P3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX6[1]	=>  Location: PIN_P4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX6[0]	=>  Location: PIN_R2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX7[6]	=>  Location: PIN_N9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX7[5]	=>  Location: PIN_P9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX7[4]	=>  Location: PIN_L7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX7[3]	=>  Location: PIN_L6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX7[2]	=>  Location: PIN_L9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX7[1]	=>  Location: PIN_L2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX7[0]	=>  Location: PIN_L3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- SW[2]	=>  Location: PIN_P25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[0]	=>  Location: PIN_N25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[1]	=>  Location: PIN_N26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[3]	=>  Location: PIN_AE14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[6]	=>  Location: PIN_AC13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[4]	=>  Location: PIN_AF14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[5]	=>  Location: PIN_AD13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[7]	=>  Location: PIN_C13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[10]	=>  Location: PIN_N1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[8]	=>  Location: PIN_B13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[9]	=>  Location: PIN_A13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[11]	=>  Location: PIN_P1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[14]	=>  Location: PIN_U3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[12]	=>  Location: PIN_P2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[13]	=>  Location: PIN_T7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[15]	=>  Location: PIN_U4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- KEY[0]	=>  Location: PIN_G26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default


ARCHITECTURE structure OF part5 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_SW : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_KEY : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_HEX0 : std_logic_vector(0 TO 6);
SIGNAL ww_HEX1 : std_logic_vector(0 TO 6);
SIGNAL ww_HEX2 : std_logic_vector(0 TO 6);
SIGNAL ww_HEX3 : std_logic_vector(0 TO 6);
SIGNAL ww_HEX4 : std_logic_vector(0 TO 6);
SIGNAL ww_HEX5 : std_logic_vector(0 TO 6);
SIGNAL ww_HEX6 : std_logic_vector(0 TO 6);
SIGNAL ww_HEX7 : std_logic_vector(0 TO 6);
SIGNAL \decode0|display[6]~0_combout\ : std_logic;
SIGNAL \decode0|display[5]~1_combout\ : std_logic;
SIGNAL \decode0|display[4]~2_combout\ : std_logic;
SIGNAL \decode0|display[3]~3_combout\ : std_logic;
SIGNAL \decode0|display[2]~4_combout\ : std_logic;
SIGNAL \decode0|display[1]~5_combout\ : std_logic;
SIGNAL \decode0|display[0]~6_combout\ : std_logic;
SIGNAL \decode1|display[6]~0_combout\ : std_logic;
SIGNAL \decode1|display[5]~1_combout\ : std_logic;
SIGNAL \decode1|display[4]~2_combout\ : std_logic;
SIGNAL \decode1|display[3]~3_combout\ : std_logic;
SIGNAL \decode1|display[2]~4_combout\ : std_logic;
SIGNAL \decode1|display[1]~5_combout\ : std_logic;
SIGNAL \decode1|display[0]~6_combout\ : std_logic;
SIGNAL \decode2|display[6]~0_combout\ : std_logic;
SIGNAL \decode2|display[5]~1_combout\ : std_logic;
SIGNAL \decode2|display[4]~2_combout\ : std_logic;
SIGNAL \decode2|display[3]~3_combout\ : std_logic;
SIGNAL \decode2|display[2]~4_combout\ : std_logic;
SIGNAL \decode2|display[1]~5_combout\ : std_logic;
SIGNAL \decode2|display[0]~6_combout\ : std_logic;
SIGNAL \decode3|display[6]~0_combout\ : std_logic;
SIGNAL \decode3|display[5]~1_combout\ : std_logic;
SIGNAL \decode3|display[4]~2_combout\ : std_logic;
SIGNAL \decode3|display[3]~3_combout\ : std_logic;
SIGNAL \decode3|display[2]~4_combout\ : std_logic;
SIGNAL \decode3|display[1]~5_combout\ : std_logic;
SIGNAL \decode3|display[0]~6_combout\ : std_logic;
SIGNAL \latch2|Q~combout\ : std_logic;
SIGNAL \latch0|Q~combout\ : std_logic;
SIGNAL \latch3|Q~combout\ : std_logic;
SIGNAL \latch1|Q~combout\ : std_logic;
SIGNAL \decode4|display[6]~0_combout\ : std_logic;
SIGNAL \decode4|display[5]~1_combout\ : std_logic;
SIGNAL \decode4|display[4]~2_combout\ : std_logic;
SIGNAL \decode4|display[3]~3_combout\ : std_logic;
SIGNAL \decode4|display[2]~4_combout\ : std_logic;
SIGNAL \decode4|display[1]~5_combout\ : std_logic;
SIGNAL \decode4|display[0]~6_combout\ : std_logic;
SIGNAL \latch7|Q~combout\ : std_logic;
SIGNAL \latch6|Q~combout\ : std_logic;
SIGNAL \latch4|Q~combout\ : std_logic;
SIGNAL \latch5|Q~combout\ : std_logic;
SIGNAL \decode5|display[6]~0_combout\ : std_logic;
SIGNAL \decode5|display[5]~1_combout\ : std_logic;
SIGNAL \decode5|display[4]~2_combout\ : std_logic;
SIGNAL \decode5|display[3]~3_combout\ : std_logic;
SIGNAL \decode5|display[2]~4_combout\ : std_logic;
SIGNAL \decode5|display[1]~5_combout\ : std_logic;
SIGNAL \decode5|display[0]~6_combout\ : std_logic;
SIGNAL \latch10|Q~combout\ : std_logic;
SIGNAL \latch11|Q~combout\ : std_logic;
SIGNAL \latch8|Q~combout\ : std_logic;
SIGNAL \latch9|Q~combout\ : std_logic;
SIGNAL \decode6|display[6]~0_combout\ : std_logic;
SIGNAL \decode6|display[5]~1_combout\ : std_logic;
SIGNAL \decode6|display[4]~2_combout\ : std_logic;
SIGNAL \decode6|display[3]~3_combout\ : std_logic;
SIGNAL \decode6|display[2]~4_combout\ : std_logic;
SIGNAL \decode6|display[1]~5_combout\ : std_logic;
SIGNAL \decode6|display[0]~6_combout\ : std_logic;
SIGNAL \latch14|Q~combout\ : std_logic;
SIGNAL \latch12|Q~combout\ : std_logic;
SIGNAL \latch13|Q~combout\ : std_logic;
SIGNAL \latch15|Q~combout\ : std_logic;
SIGNAL \decode7|display[6]~0_combout\ : std_logic;
SIGNAL \decode7|display[5]~1_combout\ : std_logic;
SIGNAL \decode7|display[4]~2_combout\ : std_logic;
SIGNAL \decode7|display[3]~3_combout\ : std_logic;
SIGNAL \decode7|display[2]~4_combout\ : std_logic;
SIGNAL \decode7|display[1]~5_combout\ : std_logic;
SIGNAL \decode7|display[0]~6_combout\ : std_logic;
SIGNAL \SW~combout\ : std_logic_vector(15 DOWNTO 0);
SIGNAL \KEY~combout\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \decode7|ALT_INV_display[2]~4_combout\ : std_logic;
SIGNAL \decode6|ALT_INV_display[2]~4_combout\ : std_logic;
SIGNAL \decode5|ALT_INV_display[2]~4_combout\ : std_logic;
SIGNAL \decode4|ALT_INV_display[2]~4_combout\ : std_logic;
SIGNAL \decode3|ALT_INV_display[2]~4_combout\ : std_logic;
SIGNAL \decode2|ALT_INV_display[2]~4_combout\ : std_logic;
SIGNAL \decode1|ALT_INV_display[2]~4_combout\ : std_logic;
SIGNAL \decode0|ALT_INV_display[2]~4_combout\ : std_logic;

BEGIN

ww_SW <= SW;
ww_KEY <= KEY;
HEX0 <= ww_HEX0;
HEX1 <= ww_HEX1;
HEX2 <= ww_HEX2;
HEX3 <= ww_HEX3;
HEX4 <= ww_HEX4;
HEX5 <= ww_HEX5;
HEX6 <= ww_HEX6;
HEX7 <= ww_HEX7;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\decode7|ALT_INV_display[2]~4_combout\ <= NOT \decode7|display[2]~4_combout\;
\decode6|ALT_INV_display[2]~4_combout\ <= NOT \decode6|display[2]~4_combout\;
\decode5|ALT_INV_display[2]~4_combout\ <= NOT \decode5|display[2]~4_combout\;
\decode4|ALT_INV_display[2]~4_combout\ <= NOT \decode4|display[2]~4_combout\;
\decode3|ALT_INV_display[2]~4_combout\ <= NOT \decode3|display[2]~4_combout\;
\decode2|ALT_INV_display[2]~4_combout\ <= NOT \decode2|display[2]~4_combout\;
\decode1|ALT_INV_display[2]~4_combout\ <= NOT \decode1|display[2]~4_combout\;
\decode0|ALT_INV_display[2]~4_combout\ <= NOT \decode0|display[2]~4_combout\;

-- Location: PIN_N25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(0),
	combout => \SW~combout\(0));

-- Location: PIN_AE14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(3),
	combout => \SW~combout\(3));

-- Location: PIN_N26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(1),
	combout => \SW~combout\(1));

-- Location: PIN_P25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(2),
	combout => \SW~combout\(2));

-- Location: LCCOMB_X27_Y3_N0
\decode0|display[6]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode0|display[6]~0_combout\ = (\SW~combout\(0) & (!\SW~combout\(3) & (\SW~combout\(1) $ (!\SW~combout\(2))))) # (!\SW~combout\(0) & (!\SW~combout\(1) & (\SW~combout\(3) $ (!\SW~combout\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010010000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(0),
	datab => \SW~combout\(3),
	datac => \SW~combout\(1),
	datad => \SW~combout\(2),
	combout => \decode0|display[6]~0_combout\);

-- Location: LCCOMB_X27_Y3_N2
\decode0|display[5]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode0|display[5]~1_combout\ = (\SW~combout\(0) & (\SW~combout\(3) $ (((\SW~combout\(1)) # (!\SW~combout\(2)))))) # (!\SW~combout\(0) & (!\SW~combout\(3) & (\SW~combout\(1) & !\SW~combout\(2))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100000110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(0),
	datab => \SW~combout\(3),
	datac => \SW~combout\(1),
	datad => \SW~combout\(2),
	combout => \decode0|display[5]~1_combout\);

-- Location: LCCOMB_X27_Y3_N20
\decode0|display[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode0|display[4]~2_combout\ = (\SW~combout\(1) & (\SW~combout\(0) & (!\SW~combout\(3)))) # (!\SW~combout\(1) & ((\SW~combout\(2) & ((!\SW~combout\(3)))) # (!\SW~combout\(2) & (\SW~combout\(0)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010001100101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(0),
	datab => \SW~combout\(3),
	datac => \SW~combout\(1),
	datad => \SW~combout\(2),
	combout => \decode0|display[4]~2_combout\);

-- Location: LCCOMB_X27_Y3_N22
\decode0|display[3]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode0|display[3]~3_combout\ = (\SW~combout\(1) & ((\SW~combout\(0) & ((\SW~combout\(2)))) # (!\SW~combout\(0) & (\SW~combout\(3) & !\SW~combout\(2))))) # (!\SW~combout\(1) & (!\SW~combout\(3) & (\SW~combout\(0) $ (\SW~combout\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000101000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(0),
	datab => \SW~combout\(3),
	datac => \SW~combout\(1),
	datad => \SW~combout\(2),
	combout => \decode0|display[3]~3_combout\);

-- Location: LCCOMB_X27_Y3_N24
\decode0|display[2]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode0|display[2]~4_combout\ = (\SW~combout\(3) & (((\SW~combout\(0) & !\SW~combout\(1))) # (!\SW~combout\(2)))) # (!\SW~combout\(3) & ((\SW~combout\(0)) # ((\SW~combout\(2)) # (!\SW~combout\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011101111101111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(0),
	datab => \SW~combout\(3),
	datac => \SW~combout\(1),
	datad => \SW~combout\(2),
	combout => \decode0|display[2]~4_combout\);

-- Location: LCCOMB_X27_Y3_N18
\decode0|display[1]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode0|display[1]~5_combout\ = (\SW~combout\(3) & ((\SW~combout\(0) & (\SW~combout\(1))) # (!\SW~combout\(0) & ((\SW~combout\(2)))))) # (!\SW~combout\(3) & (\SW~combout\(2) & (\SW~combout\(0) $ (\SW~combout\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101011010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(0),
	datab => \SW~combout\(3),
	datac => \SW~combout\(1),
	datad => \SW~combout\(2),
	combout => \decode0|display[1]~5_combout\);

-- Location: LCCOMB_X27_Y3_N12
\decode0|display[0]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode0|display[0]~6_combout\ = (\SW~combout\(3) & (\SW~combout\(0) & (\SW~combout\(1) $ (\SW~combout\(2))))) # (!\SW~combout\(3) & (!\SW~combout\(1) & (\SW~combout\(0) $ (\SW~combout\(2)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100110000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(0),
	datab => \SW~combout\(3),
	datac => \SW~combout\(1),
	datad => \SW~combout\(2),
	combout => \decode0|display[0]~6_combout\);

-- Location: PIN_C13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[7]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(7),
	combout => \SW~combout\(7));

-- Location: PIN_AF14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(4),
	combout => \SW~combout\(4));

-- Location: PIN_AC13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(6),
	combout => \SW~combout\(6));

-- Location: PIN_AD13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(5),
	combout => \SW~combout\(5));

-- Location: LCCOMB_X64_Y4_N0
\decode1|display[6]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode1|display[6]~0_combout\ = (\SW~combout\(4) & (!\SW~combout\(7) & (\SW~combout\(6) $ (!\SW~combout\(5))))) # (!\SW~combout\(4) & (!\SW~combout\(5) & (\SW~combout\(7) $ (!\SW~combout\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000100101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(7),
	datab => \SW~combout\(4),
	datac => \SW~combout\(6),
	datad => \SW~combout\(5),
	combout => \decode1|display[6]~0_combout\);

-- Location: LCCOMB_X64_Y4_N2
\decode1|display[5]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode1|display[5]~1_combout\ = (\SW~combout\(4) & (\SW~combout\(7) $ (((\SW~combout\(5)) # (!\SW~combout\(6)))))) # (!\SW~combout\(4) & (!\SW~combout\(7) & (!\SW~combout\(6) & \SW~combout\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010110000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(7),
	datab => \SW~combout\(4),
	datac => \SW~combout\(6),
	datad => \SW~combout\(5),
	combout => \decode1|display[5]~1_combout\);

-- Location: LCCOMB_X64_Y4_N20
\decode1|display[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode1|display[4]~2_combout\ = (\SW~combout\(5) & (!\SW~combout\(7) & (\SW~combout\(4)))) # (!\SW~combout\(5) & ((\SW~combout\(6) & (!\SW~combout\(7))) # (!\SW~combout\(6) & ((\SW~combout\(4))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010001011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(7),
	datab => \SW~combout\(4),
	datac => \SW~combout\(6),
	datad => \SW~combout\(5),
	combout => \decode1|display[4]~2_combout\);

-- Location: LCCOMB_X64_Y4_N30
\decode1|display[3]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode1|display[3]~3_combout\ = (\SW~combout\(5) & ((\SW~combout\(4) & ((\SW~combout\(6)))) # (!\SW~combout\(4) & (\SW~combout\(7) & !\SW~combout\(6))))) # (!\SW~combout\(5) & (!\SW~combout\(7) & (\SW~combout\(4) $ (\SW~combout\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001000010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(7),
	datab => \SW~combout\(4),
	datac => \SW~combout\(6),
	datad => \SW~combout\(5),
	combout => \decode1|display[3]~3_combout\);

-- Location: LCCOMB_X64_Y4_N16
\decode1|display[2]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode1|display[2]~4_combout\ = (\SW~combout\(7) & (((\SW~combout\(4) & !\SW~combout\(5))) # (!\SW~combout\(6)))) # (!\SW~combout\(7) & ((\SW~combout\(4)) # ((\SW~combout\(6)) # (!\SW~combout\(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111011011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(7),
	datab => \SW~combout\(4),
	datac => \SW~combout\(6),
	datad => \SW~combout\(5),
	combout => \decode1|display[2]~4_combout\);

-- Location: LCCOMB_X64_Y4_N26
\decode1|display[1]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode1|display[1]~5_combout\ = (\SW~combout\(7) & ((\SW~combout\(4) & ((\SW~combout\(5)))) # (!\SW~combout\(4) & (\SW~combout\(6))))) # (!\SW~combout\(7) & (\SW~combout\(6) & (\SW~combout\(4) $ (\SW~combout\(5)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100001100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(7),
	datab => \SW~combout\(4),
	datac => \SW~combout\(6),
	datad => \SW~combout\(5),
	combout => \decode1|display[1]~5_combout\);

-- Location: LCCOMB_X64_Y4_N12
\decode1|display[0]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode1|display[0]~6_combout\ = (\SW~combout\(7) & (\SW~combout\(4) & (\SW~combout\(6) $ (\SW~combout\(5))))) # (!\SW~combout\(7) & (!\SW~combout\(5) & (\SW~combout\(4) $ (\SW~combout\(6)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100010010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(7),
	datab => \SW~combout\(4),
	datac => \SW~combout\(6),
	datad => \SW~combout\(5),
	combout => \decode1|display[0]~6_combout\);

-- Location: PIN_P1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[11]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(11),
	combout => \SW~combout\(11));

-- Location: PIN_B13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[8]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(8),
	combout => \SW~combout\(8));

-- Location: PIN_N1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[10]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(10),
	combout => \SW~combout\(10));

-- Location: PIN_A13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[9]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(9),
	combout => \SW~combout\(9));

-- Location: LCCOMB_X64_Y7_N0
\decode2|display[6]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode2|display[6]~0_combout\ = (\SW~combout\(8) & (!\SW~combout\(11) & (\SW~combout\(10) $ (!\SW~combout\(9))))) # (!\SW~combout\(8) & (!\SW~combout\(9) & (\SW~combout\(11) $ (!\SW~combout\(10)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000100101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(11),
	datab => \SW~combout\(8),
	datac => \SW~combout\(10),
	datad => \SW~combout\(9),
	combout => \decode2|display[6]~0_combout\);

-- Location: LCCOMB_X64_Y7_N2
\decode2|display[5]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode2|display[5]~1_combout\ = (\SW~combout\(8) & (\SW~combout\(11) $ (((\SW~combout\(9)) # (!\SW~combout\(10)))))) # (!\SW~combout\(8) & (!\SW~combout\(11) & (!\SW~combout\(10) & \SW~combout\(9))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010110000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(11),
	datab => \SW~combout\(8),
	datac => \SW~combout\(10),
	datad => \SW~combout\(9),
	combout => \decode2|display[5]~1_combout\);

-- Location: LCCOMB_X64_Y7_N20
\decode2|display[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode2|display[4]~2_combout\ = (\SW~combout\(9) & (!\SW~combout\(11) & (\SW~combout\(8)))) # (!\SW~combout\(9) & ((\SW~combout\(10) & (!\SW~combout\(11))) # (!\SW~combout\(10) & ((\SW~combout\(8))))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010001011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(11),
	datab => \SW~combout\(8),
	datac => \SW~combout\(10),
	datad => \SW~combout\(9),
	combout => \decode2|display[4]~2_combout\);

-- Location: LCCOMB_X64_Y7_N6
\decode2|display[3]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode2|display[3]~3_combout\ = (\SW~combout\(9) & ((\SW~combout\(8) & ((\SW~combout\(10)))) # (!\SW~combout\(8) & (\SW~combout\(11) & !\SW~combout\(10))))) # (!\SW~combout\(9) & (!\SW~combout\(11) & (\SW~combout\(8) $ (\SW~combout\(10)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001000010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(11),
	datab => \SW~combout\(8),
	datac => \SW~combout\(10),
	datad => \SW~combout\(9),
	combout => \decode2|display[3]~3_combout\);

-- Location: LCCOMB_X64_Y7_N16
\decode2|display[2]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode2|display[2]~4_combout\ = (\SW~combout\(11) & (((\SW~combout\(8) & !\SW~combout\(9))) # (!\SW~combout\(10)))) # (!\SW~combout\(11) & ((\SW~combout\(8)) # ((\SW~combout\(10)) # (!\SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111011011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(11),
	datab => \SW~combout\(8),
	datac => \SW~combout\(10),
	datad => \SW~combout\(9),
	combout => \decode2|display[2]~4_combout\);

-- Location: LCCOMB_X64_Y7_N18
\decode2|display[1]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode2|display[1]~5_combout\ = (\SW~combout\(11) & ((\SW~combout\(8) & ((\SW~combout\(9)))) # (!\SW~combout\(8) & (\SW~combout\(10))))) # (!\SW~combout\(11) & (\SW~combout\(10) & (\SW~combout\(8) $ (\SW~combout\(9)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100001100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(11),
	datab => \SW~combout\(8),
	datac => \SW~combout\(10),
	datad => \SW~combout\(9),
	combout => \decode2|display[1]~5_combout\);

-- Location: LCCOMB_X64_Y7_N12
\decode2|display[0]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode2|display[0]~6_combout\ = (\SW~combout\(11) & (\SW~combout\(8) & (\SW~combout\(10) $ (\SW~combout\(9))))) # (!\SW~combout\(11) & (!\SW~combout\(9) & (\SW~combout\(8) $ (\SW~combout\(10)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100010010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(11),
	datab => \SW~combout\(8),
	datac => \SW~combout\(10),
	datad => \SW~combout\(9),
	combout => \decode2|display[0]~6_combout\);

-- Location: PIN_U3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[14]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(14),
	combout => \SW~combout\(14));

-- Location: PIN_T7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[13]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(13),
	combout => \SW~combout\(13));

-- Location: PIN_P2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[12]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(12),
	combout => \SW~combout\(12));

-- Location: PIN_U4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[15]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(15),
	combout => \SW~combout\(15));

-- Location: LCCOMB_X64_Y8_N0
\decode3|display[6]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode3|display[6]~0_combout\ = (\SW~combout\(12) & (!\SW~combout\(15) & (\SW~combout\(14) $ (!\SW~combout\(13))))) # (!\SW~combout\(12) & (!\SW~combout\(13) & (\SW~combout\(14) $ (!\SW~combout\(15)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001010010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(14),
	datab => \SW~combout\(13),
	datac => \SW~combout\(12),
	datad => \SW~combout\(15),
	combout => \decode3|display[6]~0_combout\);

-- Location: LCCOMB_X64_Y8_N2
\decode3|display[5]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode3|display[5]~1_combout\ = (\SW~combout\(14) & (\SW~combout\(12) & (\SW~combout\(13) $ (\SW~combout\(15))))) # (!\SW~combout\(14) & (!\SW~combout\(15) & ((\SW~combout\(13)) # (\SW~combout\(12)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000011010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(14),
	datab => \SW~combout\(13),
	datac => \SW~combout\(12),
	datad => \SW~combout\(15),
	combout => \decode3|display[5]~1_combout\);

-- Location: LCCOMB_X64_Y8_N20
\decode3|display[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode3|display[4]~2_combout\ = (\SW~combout\(13) & (((\SW~combout\(12) & !\SW~combout\(15))))) # (!\SW~combout\(13) & ((\SW~combout\(14) & ((!\SW~combout\(15)))) # (!\SW~combout\(14) & (\SW~combout\(12)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000011110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(14),
	datab => \SW~combout\(13),
	datac => \SW~combout\(12),
	datad => \SW~combout\(15),
	combout => \decode3|display[4]~2_combout\);

-- Location: LCCOMB_X64_Y8_N30
\decode3|display[3]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode3|display[3]~3_combout\ = (\SW~combout\(13) & ((\SW~combout\(14) & (\SW~combout\(12))) # (!\SW~combout\(14) & (!\SW~combout\(12) & \SW~combout\(15))))) # (!\SW~combout\(13) & (!\SW~combout\(15) & (\SW~combout\(14) $ (\SW~combout\(12)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010010010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(14),
	datab => \SW~combout\(13),
	datac => \SW~combout\(12),
	datad => \SW~combout\(15),
	combout => \decode3|display[3]~3_combout\);

-- Location: LCCOMB_X64_Y8_N16
\decode3|display[2]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode3|display[2]~4_combout\ = (\SW~combout\(14) & (((!\SW~combout\(13) & \SW~combout\(12))) # (!\SW~combout\(15)))) # (!\SW~combout\(14) & (((\SW~combout\(12)) # (\SW~combout\(15))) # (!\SW~combout\(13))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111010111111011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(14),
	datab => \SW~combout\(13),
	datac => \SW~combout\(12),
	datad => \SW~combout\(15),
	combout => \decode3|display[2]~4_combout\);

-- Location: LCCOMB_X64_Y8_N18
\decode3|display[1]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode3|display[1]~5_combout\ = (\SW~combout\(13) & ((\SW~combout\(12) & ((\SW~combout\(15)))) # (!\SW~combout\(12) & (\SW~combout\(14))))) # (!\SW~combout\(13) & (\SW~combout\(14) & (\SW~combout\(12) $ (\SW~combout\(15)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101000101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(14),
	datab => \SW~combout\(13),
	datac => \SW~combout\(12),
	datad => \SW~combout\(15),
	combout => \decode3|display[1]~5_combout\);

-- Location: LCCOMB_X64_Y8_N12
\decode3|display[0]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode3|display[0]~6_combout\ = (\SW~combout\(14) & (!\SW~combout\(13) & (\SW~combout\(12) $ (!\SW~combout\(15))))) # (!\SW~combout\(14) & (\SW~combout\(12) & (\SW~combout\(13) $ (!\SW~combout\(15)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000000010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(14),
	datab => \SW~combout\(13),
	datac => \SW~combout\(12),
	datad => \SW~combout\(15),
	combout => \decode3|display[0]~6_combout\);

-- Location: PIN_G26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\KEY[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_KEY(0),
	combout => \KEY~combout\(0));

-- Location: LCCOMB_X1_Y14_N6
\latch2|Q\ : cycloneii_lcell_comb
-- Equation(s):
-- \latch2|Q~combout\ = (\KEY~combout\(0) & ((\latch2|Q~combout\))) # (!\KEY~combout\(0) & (\SW~combout\(2)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(2),
	datab => \KEY~combout\(0),
	datad => \latch2|Q~combout\,
	combout => \latch2|Q~combout\);

-- Location: LCCOMB_X1_Y14_N0
\latch0|Q\ : cycloneii_lcell_comb
-- Equation(s):
-- \latch0|Q~combout\ = (\KEY~combout\(0) & ((\latch0|Q~combout\))) # (!\KEY~combout\(0) & (\SW~combout\(0)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \KEY~combout\(0),
	datac => \SW~combout\(0),
	datad => \latch0|Q~combout\,
	combout => \latch0|Q~combout\);

-- Location: LCCOMB_X1_Y14_N12
\latch3|Q\ : cycloneii_lcell_comb
-- Equation(s):
-- \latch3|Q~combout\ = (\KEY~combout\(0) & ((\latch3|Q~combout\))) # (!\KEY~combout\(0) & (\SW~combout\(3)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111110000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \KEY~combout\(0),
	datac => \SW~combout\(3),
	datad => \latch3|Q~combout\,
	combout => \latch3|Q~combout\);

-- Location: LCCOMB_X1_Y14_N2
\latch1|Q\ : cycloneii_lcell_comb
-- Equation(s):
-- \latch1|Q~combout\ = (\KEY~combout\(0) & ((\latch1|Q~combout\))) # (!\KEY~combout\(0) & (\SW~combout\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110111000100010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(1),
	datab => \KEY~combout\(0),
	datad => \latch1|Q~combout\,
	combout => \latch1|Q~combout\);

-- Location: LCCOMB_X1_Y14_N24
\decode4|display[6]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode4|display[6]~0_combout\ = (\latch0|Q~combout\ & (!\latch3|Q~combout\ & (\latch2|Q~combout\ $ (!\latch1|Q~combout\)))) # (!\latch0|Q~combout\ & (!\latch1|Q~combout\ & (\latch2|Q~combout\ $ (!\latch3|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100000100101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch2|Q~combout\,
	datab => \latch0|Q~combout\,
	datac => \latch3|Q~combout\,
	datad => \latch1|Q~combout\,
	combout => \decode4|display[6]~0_combout\);

-- Location: LCCOMB_X1_Y14_N26
\decode4|display[5]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode4|display[5]~1_combout\ = (\latch2|Q~combout\ & (\latch0|Q~combout\ & (\latch3|Q~combout\ $ (\latch1|Q~combout\)))) # (!\latch2|Q~combout\ & (!\latch3|Q~combout\ & ((\latch0|Q~combout\) # (\latch1|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110110000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch2|Q~combout\,
	datab => \latch0|Q~combout\,
	datac => \latch3|Q~combout\,
	datad => \latch1|Q~combout\,
	combout => \decode4|display[5]~1_combout\);

-- Location: LCCOMB_X1_Y14_N28
\decode4|display[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode4|display[4]~2_combout\ = (\latch1|Q~combout\ & (((\latch0|Q~combout\ & !\latch3|Q~combout\)))) # (!\latch1|Q~combout\ & ((\latch2|Q~combout\ & ((!\latch3|Q~combout\))) # (!\latch2|Q~combout\ & (\latch0|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110001001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch2|Q~combout\,
	datab => \latch0|Q~combout\,
	datac => \latch3|Q~combout\,
	datad => \latch1|Q~combout\,
	combout => \decode4|display[4]~2_combout\);

-- Location: LCCOMB_X1_Y14_N30
\decode4|display[3]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode4|display[3]~3_combout\ = (\latch1|Q~combout\ & ((\latch2|Q~combout\ & (\latch0|Q~combout\)) # (!\latch2|Q~combout\ & (!\latch0|Q~combout\ & \latch3|Q~combout\)))) # (!\latch1|Q~combout\ & (!\latch3|Q~combout\ & (\latch2|Q~combout\ $ 
-- (\latch0|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100000000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch2|Q~combout\,
	datab => \latch0|Q~combout\,
	datac => \latch3|Q~combout\,
	datad => \latch1|Q~combout\,
	combout => \decode4|display[3]~3_combout\);

-- Location: LCCOMB_X1_Y14_N16
\decode4|display[2]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode4|display[2]~4_combout\ = (\latch2|Q~combout\ & (((\latch0|Q~combout\ & !\latch1|Q~combout\)) # (!\latch3|Q~combout\))) # (!\latch2|Q~combout\ & ((\latch0|Q~combout\) # ((\latch3|Q~combout\) # (!\latch1|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111011011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch2|Q~combout\,
	datab => \latch0|Q~combout\,
	datac => \latch3|Q~combout\,
	datad => \latch1|Q~combout\,
	combout => \decode4|display[2]~4_combout\);

-- Location: LCCOMB_X1_Y14_N18
\decode4|display[1]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode4|display[1]~5_combout\ = (\latch3|Q~combout\ & ((\latch0|Q~combout\ & ((\latch1|Q~combout\))) # (!\latch0|Q~combout\ & (\latch2|Q~combout\)))) # (!\latch3|Q~combout\ & (\latch2|Q~combout\ & (\latch0|Q~combout\ $ (\latch1|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001000101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch2|Q~combout\,
	datab => \latch0|Q~combout\,
	datac => \latch3|Q~combout\,
	datad => \latch1|Q~combout\,
	combout => \decode4|display[1]~5_combout\);

-- Location: LCCOMB_X1_Y14_N20
\decode4|display[0]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode4|display[0]~6_combout\ = (\latch2|Q~combout\ & (!\latch1|Q~combout\ & (\latch0|Q~combout\ $ (!\latch3|Q~combout\)))) # (!\latch2|Q~combout\ & (\latch0|Q~combout\ & (\latch3|Q~combout\ $ (!\latch1|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000010000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch2|Q~combout\,
	datab => \latch0|Q~combout\,
	datac => \latch3|Q~combout\,
	datad => \latch1|Q~combout\,
	combout => \decode4|display[0]~6_combout\);

-- Location: LCCOMB_X14_Y16_N12
\latch7|Q\ : cycloneii_lcell_comb
-- Equation(s):
-- \latch7|Q~combout\ = (\KEY~combout\(0) & (\latch7|Q~combout\)) # (!\KEY~combout\(0) & ((\SW~combout\(7))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch7|Q~combout\,
	datac => \SW~combout\(7),
	datad => \KEY~combout\(0),
	combout => \latch7|Q~combout\);

-- Location: LCCOMB_X14_Y16_N22
\latch6|Q\ : cycloneii_lcell_comb
-- Equation(s):
-- \latch6|Q~combout\ = (\KEY~combout\(0) & ((\latch6|Q~combout\))) # (!\KEY~combout\(0) & (\SW~combout\(6)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(6),
	datab => \latch6|Q~combout\,
	datad => \KEY~combout\(0),
	combout => \latch6|Q~combout\);

-- Location: LCCOMB_X14_Y16_N24
\latch4|Q\ : cycloneii_lcell_comb
-- Equation(s):
-- \latch4|Q~combout\ = (\KEY~combout\(0) & ((\latch4|Q~combout\))) # (!\KEY~combout\(0) & (\SW~combout\(4)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(4),
	datac => \latch4|Q~combout\,
	datad => \KEY~combout\(0),
	combout => \latch4|Q~combout\);

-- Location: LCCOMB_X14_Y16_N10
\latch5|Q\ : cycloneii_lcell_comb
-- Equation(s):
-- \latch5|Q~combout\ = (\KEY~combout\(0) & (\latch5|Q~combout\)) # (!\KEY~combout\(0) & ((\SW~combout\(5))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch5|Q~combout\,
	datab => \SW~combout\(5),
	datad => \KEY~combout\(0),
	combout => \latch5|Q~combout\);

-- Location: LCCOMB_X14_Y16_N0
\decode5|display[6]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode5|display[6]~0_combout\ = (\latch4|Q~combout\ & (!\latch7|Q~combout\ & (\latch6|Q~combout\ $ (!\latch5|Q~combout\)))) # (!\latch4|Q~combout\ & (!\latch5|Q~combout\ & (\latch7|Q~combout\ $ (!\latch6|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000011001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch7|Q~combout\,
	datab => \latch6|Q~combout\,
	datac => \latch4|Q~combout\,
	datad => \latch5|Q~combout\,
	combout => \decode5|display[6]~0_combout\);

-- Location: LCCOMB_X14_Y16_N26
\decode5|display[5]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode5|display[5]~1_combout\ = (\latch6|Q~combout\ & (\latch4|Q~combout\ & (\latch7|Q~combout\ $ (\latch5|Q~combout\)))) # (!\latch6|Q~combout\ & (!\latch7|Q~combout\ & ((\latch4|Q~combout\) # (\latch5|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000110010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch7|Q~combout\,
	datab => \latch6|Q~combout\,
	datac => \latch4|Q~combout\,
	datad => \latch5|Q~combout\,
	combout => \decode5|display[5]~1_combout\);

-- Location: LCCOMB_X14_Y16_N28
\decode5|display[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode5|display[4]~2_combout\ = (\latch5|Q~combout\ & (!\latch7|Q~combout\ & ((\latch4|Q~combout\)))) # (!\latch5|Q~combout\ & ((\latch6|Q~combout\ & (!\latch7|Q~combout\)) # (!\latch6|Q~combout\ & ((\latch4|Q~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch7|Q~combout\,
	datab => \latch6|Q~combout\,
	datac => \latch4|Q~combout\,
	datad => \latch5|Q~combout\,
	combout => \decode5|display[4]~2_combout\);

-- Location: LCCOMB_X14_Y16_N6
\decode5|display[3]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode5|display[3]~3_combout\ = (\latch5|Q~combout\ & ((\latch6|Q~combout\ & ((\latch4|Q~combout\))) # (!\latch6|Q~combout\ & (\latch7|Q~combout\ & !\latch4|Q~combout\)))) # (!\latch5|Q~combout\ & (!\latch7|Q~combout\ & (\latch6|Q~combout\ $ 
-- (\latch4|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001000010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch7|Q~combout\,
	datab => \latch6|Q~combout\,
	datac => \latch4|Q~combout\,
	datad => \latch5|Q~combout\,
	combout => \decode5|display[3]~3_combout\);

-- Location: LCCOMB_X14_Y16_N16
\decode5|display[2]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode5|display[2]~4_combout\ = (\latch7|Q~combout\ & (((\latch4|Q~combout\ & !\latch5|Q~combout\)) # (!\latch6|Q~combout\))) # (!\latch7|Q~combout\ & ((\latch6|Q~combout\) # ((\latch4|Q~combout\) # (!\latch5|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011011110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch7|Q~combout\,
	datab => \latch6|Q~combout\,
	datac => \latch4|Q~combout\,
	datad => \latch5|Q~combout\,
	combout => \decode5|display[2]~4_combout\);

-- Location: LCCOMB_X14_Y16_N2
\decode5|display[1]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode5|display[1]~5_combout\ = (\latch7|Q~combout\ & ((\latch4|Q~combout\ & ((\latch5|Q~combout\))) # (!\latch4|Q~combout\ & (\latch6|Q~combout\)))) # (!\latch7|Q~combout\ & (\latch6|Q~combout\ & (\latch4|Q~combout\ $ (\latch5|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110001001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch7|Q~combout\,
	datab => \latch6|Q~combout\,
	datac => \latch4|Q~combout\,
	datad => \latch5|Q~combout\,
	combout => \decode5|display[1]~5_combout\);

-- Location: LCCOMB_X14_Y16_N4
\decode5|display[0]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode5|display[0]~6_combout\ = (\latch7|Q~combout\ & (\latch4|Q~combout\ & (\latch6|Q~combout\ $ (\latch5|Q~combout\)))) # (!\latch7|Q~combout\ & (!\latch5|Q~combout\ & (\latch6|Q~combout\ $ (\latch4|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000010010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch7|Q~combout\,
	datab => \latch6|Q~combout\,
	datac => \latch4|Q~combout\,
	datad => \latch5|Q~combout\,
	combout => \decode5|display[0]~6_combout\);

-- Location: LCCOMB_X1_Y23_N14
\latch10|Q\ : cycloneii_lcell_comb
-- Equation(s):
-- \latch10|Q~combout\ = (\KEY~combout\(0) & ((\latch10|Q~combout\))) # (!\KEY~combout\(0) & (\SW~combout\(10)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(10),
	datac => \latch10|Q~combout\,
	datad => \KEY~combout\(0),
	combout => \latch10|Q~combout\);

-- Location: LCCOMB_X1_Y23_N4
\latch11|Q\ : cycloneii_lcell_comb
-- Equation(s):
-- \latch11|Q~combout\ = (\KEY~combout\(0) & ((\latch11|Q~combout\))) # (!\KEY~combout\(0) & (\SW~combout\(11)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SW~combout\(11),
	datac => \latch11|Q~combout\,
	datad => \KEY~combout\(0),
	combout => \latch11|Q~combout\);

-- Location: LCCOMB_X1_Y23_N8
\latch8|Q\ : cycloneii_lcell_comb
-- Equation(s):
-- \latch8|Q~combout\ = (\KEY~combout\(0) & ((\latch8|Q~combout\))) # (!\KEY~combout\(0) & (\SW~combout\(8)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(8),
	datac => \latch8|Q~combout\,
	datad => \KEY~combout\(0),
	combout => \latch8|Q~combout\);

-- Location: LCCOMB_X1_Y23_N26
\latch9|Q\ : cycloneii_lcell_comb
-- Equation(s):
-- \latch9|Q~combout\ = (\KEY~combout\(0) & ((\latch9|Q~combout\))) # (!\KEY~combout\(0) & (\SW~combout\(9)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(9),
	datab => \latch9|Q~combout\,
	datad => \KEY~combout\(0),
	combout => \latch9|Q~combout\);

-- Location: LCCOMB_X1_Y23_N16
\decode6|display[6]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode6|display[6]~0_combout\ = (\latch8|Q~combout\ & (!\latch11|Q~combout\ & (\latch10|Q~combout\ $ (!\latch9|Q~combout\)))) # (!\latch8|Q~combout\ & (!\latch9|Q~combout\ & (\latch10|Q~combout\ $ (!\latch11|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000011001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch10|Q~combout\,
	datab => \latch11|Q~combout\,
	datac => \latch8|Q~combout\,
	datad => \latch9|Q~combout\,
	combout => \decode6|display[6]~0_combout\);

-- Location: LCCOMB_X1_Y23_N18
\decode6|display[5]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode6|display[5]~1_combout\ = (\latch10|Q~combout\ & (\latch8|Q~combout\ & (\latch11|Q~combout\ $ (\latch9|Q~combout\)))) # (!\latch10|Q~combout\ & (!\latch11|Q~combout\ & ((\latch8|Q~combout\) # (\latch9|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000110010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch10|Q~combout\,
	datab => \latch11|Q~combout\,
	datac => \latch8|Q~combout\,
	datad => \latch9|Q~combout\,
	combout => \decode6|display[5]~1_combout\);

-- Location: LCCOMB_X1_Y23_N20
\decode6|display[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode6|display[4]~2_combout\ = (\latch9|Q~combout\ & (((!\latch11|Q~combout\ & \latch8|Q~combout\)))) # (!\latch9|Q~combout\ & ((\latch10|Q~combout\ & (!\latch11|Q~combout\)) # (!\latch10|Q~combout\ & ((\latch8|Q~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000001110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch10|Q~combout\,
	datab => \latch11|Q~combout\,
	datac => \latch8|Q~combout\,
	datad => \latch9|Q~combout\,
	combout => \decode6|display[4]~2_combout\);

-- Location: LCCOMB_X1_Y23_N30
\decode6|display[3]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode6|display[3]~3_combout\ = (\latch9|Q~combout\ & ((\latch10|Q~combout\ & ((\latch8|Q~combout\))) # (!\latch10|Q~combout\ & (\latch11|Q~combout\ & !\latch8|Q~combout\)))) # (!\latch9|Q~combout\ & (!\latch11|Q~combout\ & (\latch10|Q~combout\ $ 
-- (\latch8|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010000010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch10|Q~combout\,
	datab => \latch11|Q~combout\,
	datac => \latch8|Q~combout\,
	datad => \latch9|Q~combout\,
	combout => \decode6|display[3]~3_combout\);

-- Location: LCCOMB_X1_Y23_N24
\decode6|display[2]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode6|display[2]~4_combout\ = (\latch10|Q~combout\ & (((\latch8|Q~combout\ & !\latch9|Q~combout\)) # (!\latch11|Q~combout\))) # (!\latch10|Q~combout\ & ((\latch11|Q~combout\) # ((\latch8|Q~combout\) # (!\latch9|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011011110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch10|Q~combout\,
	datab => \latch11|Q~combout\,
	datac => \latch8|Q~combout\,
	datad => \latch9|Q~combout\,
	combout => \decode6|display[2]~4_combout\);

-- Location: LCCOMB_X1_Y23_N10
\decode6|display[1]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode6|display[1]~5_combout\ = (\latch11|Q~combout\ & ((\latch8|Q~combout\ & ((\latch9|Q~combout\))) # (!\latch8|Q~combout\ & (\latch10|Q~combout\)))) # (!\latch11|Q~combout\ & (\latch10|Q~combout\ & (\latch8|Q~combout\ $ (\latch9|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101000101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch10|Q~combout\,
	datab => \latch11|Q~combout\,
	datac => \latch8|Q~combout\,
	datad => \latch9|Q~combout\,
	combout => \decode6|display[1]~5_combout\);

-- Location: LCCOMB_X1_Y23_N28
\decode6|display[0]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode6|display[0]~6_combout\ = (\latch10|Q~combout\ & (!\latch9|Q~combout\ & (\latch11|Q~combout\ $ (!\latch8|Q~combout\)))) # (!\latch10|Q~combout\ & (\latch8|Q~combout\ & (\latch11|Q~combout\ $ (!\latch9|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000010010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch10|Q~combout\,
	datab => \latch11|Q~combout\,
	datac => \latch8|Q~combout\,
	datad => \latch9|Q~combout\,
	combout => \decode6|display[0]~6_combout\);

-- Location: LCCOMB_X1_Y24_N14
\latch14|Q\ : cycloneii_lcell_comb
-- Equation(s):
-- \latch14|Q~combout\ = (\KEY~combout\(0) & ((\latch14|Q~combout\))) # (!\KEY~combout\(0) & (\SW~combout\(14)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(14),
	datac => \latch14|Q~combout\,
	datad => \KEY~combout\(0),
	combout => \latch14|Q~combout\);

-- Location: LCCOMB_X1_Y24_N0
\latch12|Q\ : cycloneii_lcell_comb
-- Equation(s):
-- \latch12|Q~combout\ = (\KEY~combout\(0) & (\latch12|Q~combout\)) # (!\KEY~combout\(0) & ((\SW~combout\(12))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \latch12|Q~combout\,
	datac => \SW~combout\(12),
	datad => \KEY~combout\(0),
	combout => \latch12|Q~combout\);

-- Location: LCCOMB_X1_Y24_N26
\latch13|Q\ : cycloneii_lcell_comb
-- Equation(s):
-- \latch13|Q~combout\ = (\KEY~combout\(0) & (\latch13|Q~combout\)) # (!\KEY~combout\(0) & ((\SW~combout\(13))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \latch13|Q~combout\,
	datac => \SW~combout\(13),
	datad => \KEY~combout\(0),
	combout => \latch13|Q~combout\);

-- Location: LCCOMB_X1_Y24_N12
\latch15|Q\ : cycloneii_lcell_comb
-- Equation(s):
-- \latch15|Q~combout\ = (\KEY~combout\(0) & ((\latch15|Q~combout\))) # (!\KEY~combout\(0) & (\SW~combout\(15)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(15),
	datac => \latch15|Q~combout\,
	datad => \KEY~combout\(0),
	combout => \latch15|Q~combout\);

-- Location: LCCOMB_X1_Y24_N8
\decode7|display[6]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode7|display[6]~0_combout\ = (\latch12|Q~combout\ & (!\latch15|Q~combout\ & (\latch14|Q~combout\ $ (!\latch13|Q~combout\)))) # (!\latch12|Q~combout\ & (!\latch13|Q~combout\ & (\latch14|Q~combout\ $ (!\latch15|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001010000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch14|Q~combout\,
	datab => \latch12|Q~combout\,
	datac => \latch13|Q~combout\,
	datad => \latch15|Q~combout\,
	combout => \decode7|display[6]~0_combout\);

-- Location: LCCOMB_X1_Y24_N10
\decode7|display[5]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode7|display[5]~1_combout\ = (\latch14|Q~combout\ & (\latch12|Q~combout\ & (\latch13|Q~combout\ $ (\latch15|Q~combout\)))) # (!\latch14|Q~combout\ & (!\latch15|Q~combout\ & ((\latch12|Q~combout\) # (\latch13|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100011010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch14|Q~combout\,
	datab => \latch12|Q~combout\,
	datac => \latch13|Q~combout\,
	datad => \latch15|Q~combout\,
	combout => \decode7|display[5]~1_combout\);

-- Location: LCCOMB_X1_Y24_N28
\decode7|display[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode7|display[4]~2_combout\ = (\latch13|Q~combout\ & (((\latch12|Q~combout\ & !\latch15|Q~combout\)))) # (!\latch13|Q~combout\ & ((\latch14|Q~combout\ & ((!\latch15|Q~combout\))) # (!\latch14|Q~combout\ & (\latch12|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010011001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch14|Q~combout\,
	datab => \latch12|Q~combout\,
	datac => \latch13|Q~combout\,
	datad => \latch15|Q~combout\,
	combout => \decode7|display[4]~2_combout\);

-- Location: LCCOMB_X1_Y24_N30
\decode7|display[3]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode7|display[3]~3_combout\ = (\latch13|Q~combout\ & ((\latch14|Q~combout\ & (\latch12|Q~combout\)) # (!\latch14|Q~combout\ & (!\latch12|Q~combout\ & \latch15|Q~combout\)))) # (!\latch13|Q~combout\ & (!\latch15|Q~combout\ & (\latch14|Q~combout\ $ 
-- (\latch12|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000010000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch14|Q~combout\,
	datab => \latch12|Q~combout\,
	datac => \latch13|Q~combout\,
	datad => \latch15|Q~combout\,
	combout => \decode7|display[3]~3_combout\);

-- Location: LCCOMB_X1_Y24_N16
\decode7|display[2]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode7|display[2]~4_combout\ = (\latch14|Q~combout\ & (((\latch12|Q~combout\ & !\latch13|Q~combout\)) # (!\latch15|Q~combout\))) # (!\latch14|Q~combout\ & ((\latch12|Q~combout\) # ((\latch15|Q~combout\) # (!\latch13|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101110111101111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch14|Q~combout\,
	datab => \latch12|Q~combout\,
	datac => \latch13|Q~combout\,
	datad => \latch15|Q~combout\,
	combout => \decode7|display[2]~4_combout\);

-- Location: LCCOMB_X1_Y24_N18
\decode7|display[1]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode7|display[1]~5_combout\ = (\latch13|Q~combout\ & ((\latch12|Q~combout\ & ((\latch15|Q~combout\))) # (!\latch12|Q~combout\ & (\latch14|Q~combout\)))) # (!\latch13|Q~combout\ & (\latch14|Q~combout\ & (\latch12|Q~combout\ $ (\latch15|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001000101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch14|Q~combout\,
	datab => \latch12|Q~combout\,
	datac => \latch13|Q~combout\,
	datad => \latch15|Q~combout\,
	combout => \decode7|display[1]~5_combout\);

-- Location: LCCOMB_X1_Y24_N20
\decode7|display[0]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode7|display[0]~6_combout\ = (\latch14|Q~combout\ & (!\latch13|Q~combout\ & (\latch12|Q~combout\ $ (!\latch15|Q~combout\)))) # (!\latch14|Q~combout\ & (\latch12|Q~combout\ & (\latch13|Q~combout\ $ (!\latch15|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100000000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch14|Q~combout\,
	datab => \latch12|Q~combout\,
	datac => \latch13|Q~combout\,
	datad => \latch15|Q~combout\,
	combout => \decode7|display[0]~6_combout\);

-- Location: PIN_N23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\KEY[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_KEY(1));

-- Location: PIN_V13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode0|display[6]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(6));

-- Location: PIN_V14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode0|display[5]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(5));

-- Location: PIN_AE11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode0|display[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(4));

-- Location: PIN_AD11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode0|display[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(3));

-- Location: PIN_AC12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode0|ALT_INV_display[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(2));

-- Location: PIN_AB12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode0|display[1]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(1));

-- Location: PIN_AF10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode0|display[0]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(0));

-- Location: PIN_AB24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode1|display[6]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(6));

-- Location: PIN_AA23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode1|display[5]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(5));

-- Location: PIN_AA24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode1|display[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(4));

-- Location: PIN_Y22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode1|display[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(3));

-- Location: PIN_W21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode1|ALT_INV_display[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(2));

-- Location: PIN_V21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode1|display[1]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(1));

-- Location: PIN_V20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode1|display[0]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(0));

-- Location: PIN_Y24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode2|display[6]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(6));

-- Location: PIN_AB25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode2|display[5]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(5));

-- Location: PIN_AB26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode2|display[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(4));

-- Location: PIN_AC26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode2|display[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(3));

-- Location: PIN_AC25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode2|ALT_INV_display[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(2));

-- Location: PIN_V22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode2|display[1]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(1));

-- Location: PIN_AB23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode2|display[0]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(0));

-- Location: PIN_W24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode3|display[6]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(6));

-- Location: PIN_U22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode3|display[5]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(5));

-- Location: PIN_Y25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode3|display[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(4));

-- Location: PIN_Y26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode3|display[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(3));

-- Location: PIN_AA26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode3|ALT_INV_display[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(2));

-- Location: PIN_AA25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode3|display[1]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(1));

-- Location: PIN_Y23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode3|display[0]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(0));

-- Location: PIN_T3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX4[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode4|display[6]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX4(6));

-- Location: PIN_R6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX4[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode4|display[5]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX4(5));

-- Location: PIN_R7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX4[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode4|display[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX4(4));

-- Location: PIN_T4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX4[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode4|display[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX4(3));

-- Location: PIN_U2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX4[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode4|ALT_INV_display[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX4(2));

-- Location: PIN_U1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX4[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode4|display[1]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX4(1));

-- Location: PIN_U9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX4[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode4|display[0]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX4(0));

-- Location: PIN_R3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX5[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode5|display[6]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX5(6));

-- Location: PIN_R4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX5[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode5|display[5]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX5(5));

-- Location: PIN_R5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX5[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode5|display[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX5(4));

-- Location: PIN_T9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX5[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode5|display[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX5(3));

-- Location: PIN_P7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX5[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode5|ALT_INV_display[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX5(2));

-- Location: PIN_P6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX5[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode5|display[1]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX5(1));

-- Location: PIN_T2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX5[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode5|display[0]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX5(0));

-- Location: PIN_M4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX6[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode6|display[6]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX6(6));

-- Location: PIN_M5,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX6[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode6|display[5]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX6(5));

-- Location: PIN_M3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX6[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode6|display[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX6(4));

-- Location: PIN_M2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX6[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode6|display[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX6(3));

-- Location: PIN_P3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX6[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode6|ALT_INV_display[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX6(2));

-- Location: PIN_P4,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX6[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode6|display[1]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX6(1));

-- Location: PIN_R2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX6[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode6|display[0]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX6(0));

-- Location: PIN_N9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX7[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode7|display[6]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX7(6));

-- Location: PIN_P9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX7[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode7|display[5]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX7(5));

-- Location: PIN_L7,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX7[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode7|display[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX7(4));

-- Location: PIN_L6,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX7[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode7|display[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX7(3));

-- Location: PIN_L9,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX7[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode7|ALT_INV_display[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX7(2));

-- Location: PIN_L2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX7[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode7|display[1]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX7(1));

-- Location: PIN_L3,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX7[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode7|display[0]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX7(0));
END structure;


