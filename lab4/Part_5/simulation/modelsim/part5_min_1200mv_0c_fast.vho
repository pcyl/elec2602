-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "04/15/2015 23:12:36"

-- 
-- Device: Altera EP4CGX22CF19C6 Package FBGA324
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEIV;
LIBRARY IEEE;
USE CYCLONEIV.CYCLONEIV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	part5 IS
    PORT (
	SW : IN std_logic_vector(15 DOWNTO 0);
	KEY : IN std_logic_vector(1 DOWNTO 0);
	HEX0 : OUT std_logic_vector(0 TO 6);
	HEX1 : OUT std_logic_vector(0 TO 6);
	HEX2 : OUT std_logic_vector(0 TO 6);
	HEX3 : OUT std_logic_vector(0 TO 6);
	HEX4 : OUT std_logic_vector(0 TO 6);
	HEX5 : OUT std_logic_vector(0 TO 6);
	HEX6 : OUT std_logic_vector(0 TO 6);
	HEX7 : OUT std_logic_vector(0 TO 6)
	);
END part5;

-- Design Ports Information
-- KEY[1]	=>  Location: PIN_M7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[6]	=>  Location: PIN_F15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[5]	=>  Location: PIN_C16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[4]	=>  Location: PIN_E16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[3]	=>  Location: PIN_C18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[2]	=>  Location: PIN_E15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[1]	=>  Location: PIN_F16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[0]	=>  Location: PIN_B18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[6]	=>  Location: PIN_K15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[5]	=>  Location: PIN_M17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[4]	=>  Location: PIN_M16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[3]	=>  Location: PIN_N18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[2]	=>  Location: PIN_L18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[1]	=>  Location: PIN_M18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[0]	=>  Location: PIN_K16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[6]	=>  Location: PIN_P12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[5]	=>  Location: PIN_R12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[4]	=>  Location: PIN_V15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[3]	=>  Location: PIN_R13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[2]	=>  Location: PIN_T13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[1]	=>  Location: PIN_U15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[0]	=>  Location: PIN_P13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[6]	=>  Location: PIN_A11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[5]	=>  Location: PIN_B9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[4]	=>  Location: PIN_A10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[3]	=>  Location: PIN_B10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[2]	=>  Location: PIN_D9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[1]	=>  Location: PIN_A8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[0]	=>  Location: PIN_C9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[6]	=>  Location: PIN_J16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[5]	=>  Location: PIN_H16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[4]	=>  Location: PIN_G17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[3]	=>  Location: PIN_G18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[2]	=>  Location: PIN_G15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[1]	=>  Location: PIN_G16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX4[0]	=>  Location: PIN_F17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[6]	=>  Location: PIN_R17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[5]	=>  Location: PIN_P16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[4]	=>  Location: PIN_N15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[3]	=>  Location: PIN_R18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[2]	=>  Location: PIN_T18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[1]	=>  Location: PIN_R16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX5[0]	=>  Location: PIN_N16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX6[6]	=>  Location: PIN_T16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX6[5]	=>  Location: PIN_U18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX6[4]	=>  Location: PIN_R14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX6[3]	=>  Location: PIN_T15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX6[2]	=>  Location: PIN_V17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX6[1]	=>  Location: PIN_R15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX6[0]	=>  Location: PIN_V18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX7[6]	=>  Location: PIN_D11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX7[5]	=>  Location: PIN_A14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX7[4]	=>  Location: PIN_C12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX7[3]	=>  Location: PIN_B13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX7[2]	=>  Location: PIN_A15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX7[1]	=>  Location: PIN_A13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX7[0]	=>  Location: PIN_E10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[2]	=>  Location: PIN_D17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[0]	=>  Location: PIN_D18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[1]	=>  Location: PIN_E18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[3]	=>  Location: PIN_F18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[6]	=>  Location: PIN_L15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[4]	=>  Location: PIN_N17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[5]	=>  Location: PIN_P18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[7]	=>  Location: PIN_L16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[10]	=>  Location: PIN_T14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[8]	=>  Location: PIN_U16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[9]	=>  Location: PIN_T17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[11]	=>  Location: PIN_V16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[14]	=>  Location: PIN_C10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[12]	=>  Location: PIN_D12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[13]	=>  Location: PIN_C11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[15]	=>  Location: PIN_D10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[0]	=>  Location: PIN_M10,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF part5 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_SW : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_KEY : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_HEX0 : std_logic_vector(0 TO 6);
SIGNAL ww_HEX1 : std_logic_vector(0 TO 6);
SIGNAL ww_HEX2 : std_logic_vector(0 TO 6);
SIGNAL ww_HEX3 : std_logic_vector(0 TO 6);
SIGNAL ww_HEX4 : std_logic_vector(0 TO 6);
SIGNAL ww_HEX5 : std_logic_vector(0 TO 6);
SIGNAL ww_HEX6 : std_logic_vector(0 TO 6);
SIGNAL ww_HEX7 : std_logic_vector(0 TO 6);
SIGNAL \KEY[0]~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \KEY[1]~input_o\ : std_logic;
SIGNAL \HEX0[6]~output_o\ : std_logic;
SIGNAL \HEX0[5]~output_o\ : std_logic;
SIGNAL \HEX0[4]~output_o\ : std_logic;
SIGNAL \HEX0[3]~output_o\ : std_logic;
SIGNAL \HEX0[2]~output_o\ : std_logic;
SIGNAL \HEX0[1]~output_o\ : std_logic;
SIGNAL \HEX0[0]~output_o\ : std_logic;
SIGNAL \HEX1[6]~output_o\ : std_logic;
SIGNAL \HEX1[5]~output_o\ : std_logic;
SIGNAL \HEX1[4]~output_o\ : std_logic;
SIGNAL \HEX1[3]~output_o\ : std_logic;
SIGNAL \HEX1[2]~output_o\ : std_logic;
SIGNAL \HEX1[1]~output_o\ : std_logic;
SIGNAL \HEX1[0]~output_o\ : std_logic;
SIGNAL \HEX2[6]~output_o\ : std_logic;
SIGNAL \HEX2[5]~output_o\ : std_logic;
SIGNAL \HEX2[4]~output_o\ : std_logic;
SIGNAL \HEX2[3]~output_o\ : std_logic;
SIGNAL \HEX2[2]~output_o\ : std_logic;
SIGNAL \HEX2[1]~output_o\ : std_logic;
SIGNAL \HEX2[0]~output_o\ : std_logic;
SIGNAL \HEX3[6]~output_o\ : std_logic;
SIGNAL \HEX3[5]~output_o\ : std_logic;
SIGNAL \HEX3[4]~output_o\ : std_logic;
SIGNAL \HEX3[3]~output_o\ : std_logic;
SIGNAL \HEX3[2]~output_o\ : std_logic;
SIGNAL \HEX3[1]~output_o\ : std_logic;
SIGNAL \HEX3[0]~output_o\ : std_logic;
SIGNAL \HEX4[6]~output_o\ : std_logic;
SIGNAL \HEX4[5]~output_o\ : std_logic;
SIGNAL \HEX4[4]~output_o\ : std_logic;
SIGNAL \HEX4[3]~output_o\ : std_logic;
SIGNAL \HEX4[2]~output_o\ : std_logic;
SIGNAL \HEX4[1]~output_o\ : std_logic;
SIGNAL \HEX4[0]~output_o\ : std_logic;
SIGNAL \HEX5[6]~output_o\ : std_logic;
SIGNAL \HEX5[5]~output_o\ : std_logic;
SIGNAL \HEX5[4]~output_o\ : std_logic;
SIGNAL \HEX5[3]~output_o\ : std_logic;
SIGNAL \HEX5[2]~output_o\ : std_logic;
SIGNAL \HEX5[1]~output_o\ : std_logic;
SIGNAL \HEX5[0]~output_o\ : std_logic;
SIGNAL \HEX6[6]~output_o\ : std_logic;
SIGNAL \HEX6[5]~output_o\ : std_logic;
SIGNAL \HEX6[4]~output_o\ : std_logic;
SIGNAL \HEX6[3]~output_o\ : std_logic;
SIGNAL \HEX6[2]~output_o\ : std_logic;
SIGNAL \HEX6[1]~output_o\ : std_logic;
SIGNAL \HEX6[0]~output_o\ : std_logic;
SIGNAL \HEX7[6]~output_o\ : std_logic;
SIGNAL \HEX7[5]~output_o\ : std_logic;
SIGNAL \HEX7[4]~output_o\ : std_logic;
SIGNAL \HEX7[3]~output_o\ : std_logic;
SIGNAL \HEX7[2]~output_o\ : std_logic;
SIGNAL \HEX7[1]~output_o\ : std_logic;
SIGNAL \HEX7[0]~output_o\ : std_logic;
SIGNAL \SW[1]~input_o\ : std_logic;
SIGNAL \SW[2]~input_o\ : std_logic;
SIGNAL \SW[3]~input_o\ : std_logic;
SIGNAL \SW[0]~input_o\ : std_logic;
SIGNAL \decode0|display[6]~0_combout\ : std_logic;
SIGNAL \decode0|display[5]~1_combout\ : std_logic;
SIGNAL \decode0|display[4]~2_combout\ : std_logic;
SIGNAL \decode0|display[3]~3_combout\ : std_logic;
SIGNAL \decode0|display[2]~4_combout\ : std_logic;
SIGNAL \decode0|display[1]~5_combout\ : std_logic;
SIGNAL \decode0|display[0]~6_combout\ : std_logic;
SIGNAL \SW[6]~input_o\ : std_logic;
SIGNAL \SW[7]~input_o\ : std_logic;
SIGNAL \SW[4]~input_o\ : std_logic;
SIGNAL \SW[5]~input_o\ : std_logic;
SIGNAL \decode1|display[6]~0_combout\ : std_logic;
SIGNAL \decode1|display[5]~1_combout\ : std_logic;
SIGNAL \decode1|display[4]~2_combout\ : std_logic;
SIGNAL \decode1|display[3]~3_combout\ : std_logic;
SIGNAL \decode1|display[2]~4_combout\ : std_logic;
SIGNAL \decode1|display[1]~5_combout\ : std_logic;
SIGNAL \decode1|display[0]~6_combout\ : std_logic;
SIGNAL \SW[11]~input_o\ : std_logic;
SIGNAL \SW[8]~input_o\ : std_logic;
SIGNAL \SW[10]~input_o\ : std_logic;
SIGNAL \SW[9]~input_o\ : std_logic;
SIGNAL \decode2|display[6]~0_combout\ : std_logic;
SIGNAL \decode2|display[5]~1_combout\ : std_logic;
SIGNAL \decode2|display[4]~2_combout\ : std_logic;
SIGNAL \decode2|display[3]~3_combout\ : std_logic;
SIGNAL \decode2|display[2]~4_combout\ : std_logic;
SIGNAL \decode2|display[1]~5_combout\ : std_logic;
SIGNAL \decode2|display[0]~6_combout\ : std_logic;
SIGNAL \SW[14]~input_o\ : std_logic;
SIGNAL \SW[15]~input_o\ : std_logic;
SIGNAL \SW[12]~input_o\ : std_logic;
SIGNAL \SW[13]~input_o\ : std_logic;
SIGNAL \decode3|display[6]~0_combout\ : std_logic;
SIGNAL \decode3|display[5]~1_combout\ : std_logic;
SIGNAL \decode3|display[4]~2_combout\ : std_logic;
SIGNAL \decode3|display[3]~3_combout\ : std_logic;
SIGNAL \decode3|display[2]~4_combout\ : std_logic;
SIGNAL \decode3|display[1]~5_combout\ : std_logic;
SIGNAL \decode3|display[0]~6_combout\ : std_logic;
SIGNAL \KEY[0]~input_o\ : std_logic;
SIGNAL \KEY[0]~inputclkctrl_outclk\ : std_logic;
SIGNAL \latch3|Q~combout\ : std_logic;
SIGNAL \latch0|Q~combout\ : std_logic;
SIGNAL \latch2|Q~combout\ : std_logic;
SIGNAL \latch1|Q~combout\ : std_logic;
SIGNAL \decode4|display[6]~0_combout\ : std_logic;
SIGNAL \decode4|display[5]~1_combout\ : std_logic;
SIGNAL \decode4|display[4]~2_combout\ : std_logic;
SIGNAL \decode4|display[3]~3_combout\ : std_logic;
SIGNAL \decode4|display[2]~4_combout\ : std_logic;
SIGNAL \decode4|display[1]~5_combout\ : std_logic;
SIGNAL \decode4|display[0]~6_combout\ : std_logic;
SIGNAL \latch5|Q~combout\ : std_logic;
SIGNAL \latch4|Q~combout\ : std_logic;
SIGNAL \latch6|Q~combout\ : std_logic;
SIGNAL \latch7|Q~combout\ : std_logic;
SIGNAL \decode5|display[6]~0_combout\ : std_logic;
SIGNAL \decode5|display[5]~1_combout\ : std_logic;
SIGNAL \decode5|display[4]~2_combout\ : std_logic;
SIGNAL \decode5|display[3]~3_combout\ : std_logic;
SIGNAL \decode5|display[2]~4_combout\ : std_logic;
SIGNAL \decode5|display[1]~5_combout\ : std_logic;
SIGNAL \decode5|display[0]~6_combout\ : std_logic;
SIGNAL \latch9|Q~combout\ : std_logic;
SIGNAL \latch8|Q~combout\ : std_logic;
SIGNAL \latch10|Q~combout\ : std_logic;
SIGNAL \latch11|Q~combout\ : std_logic;
SIGNAL \decode6|display[6]~0_combout\ : std_logic;
SIGNAL \decode6|display[5]~1_combout\ : std_logic;
SIGNAL \decode6|display[4]~2_combout\ : std_logic;
SIGNAL \decode6|display[3]~3_combout\ : std_logic;
SIGNAL \decode6|display[2]~4_combout\ : std_logic;
SIGNAL \decode6|display[1]~5_combout\ : std_logic;
SIGNAL \decode6|display[0]~6_combout\ : std_logic;
SIGNAL \latch13|Q~combout\ : std_logic;
SIGNAL \latch12|Q~combout\ : std_logic;
SIGNAL \latch14|Q~combout\ : std_logic;
SIGNAL \latch15|Q~combout\ : std_logic;
SIGNAL \decode7|display[6]~0_combout\ : std_logic;
SIGNAL \decode7|display[5]~1_combout\ : std_logic;
SIGNAL \decode7|display[4]~2_combout\ : std_logic;
SIGNAL \decode7|display[3]~3_combout\ : std_logic;
SIGNAL \decode7|display[2]~4_combout\ : std_logic;
SIGNAL \decode7|display[1]~5_combout\ : std_logic;
SIGNAL \decode7|display[0]~6_combout\ : std_logic;
SIGNAL \decode7|ALT_INV_display[2]~4_combout\ : std_logic;
SIGNAL \decode6|ALT_INV_display[2]~4_combout\ : std_logic;
SIGNAL \decode5|ALT_INV_display[2]~4_combout\ : std_logic;
SIGNAL \decode4|ALT_INV_display[2]~4_combout\ : std_logic;
SIGNAL \decode3|ALT_INV_display[2]~4_combout\ : std_logic;
SIGNAL \decode2|ALT_INV_display[2]~4_combout\ : std_logic;
SIGNAL \decode1|ALT_INV_display[2]~4_combout\ : std_logic;
SIGNAL \decode0|ALT_INV_display[2]~4_combout\ : std_logic;

BEGIN

ww_SW <= SW;
ww_KEY <= KEY;
HEX0 <= ww_HEX0;
HEX1 <= ww_HEX1;
HEX2 <= ww_HEX2;
HEX3 <= ww_HEX3;
HEX4 <= ww_HEX4;
HEX5 <= ww_HEX5;
HEX6 <= ww_HEX6;
HEX7 <= ww_HEX7;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\KEY[0]~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \KEY[0]~input_o\);
\decode7|ALT_INV_display[2]~4_combout\ <= NOT \decode7|display[2]~4_combout\;
\decode6|ALT_INV_display[2]~4_combout\ <= NOT \decode6|display[2]~4_combout\;
\decode5|ALT_INV_display[2]~4_combout\ <= NOT \decode5|display[2]~4_combout\;
\decode4|ALT_INV_display[2]~4_combout\ <= NOT \decode4|display[2]~4_combout\;
\decode3|ALT_INV_display[2]~4_combout\ <= NOT \decode3|display[2]~4_combout\;
\decode2|ALT_INV_display[2]~4_combout\ <= NOT \decode2|display[2]~4_combout\;
\decode1|ALT_INV_display[2]~4_combout\ <= NOT \decode1|display[2]~4_combout\;
\decode0|ALT_INV_display[2]~4_combout\ <= NOT \decode0|display[2]~4_combout\;

-- Location: IOOBUF_X52_Y32_N2
\HEX0[6]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode0|display[6]~0_combout\,
	devoe => ww_devoe,
	o => \HEX0[6]~output_o\);

-- Location: IOOBUF_X48_Y41_N2
\HEX0[5]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode0|display[5]~1_combout\,
	devoe => ww_devoe,
	o => \HEX0[5]~output_o\);

-- Location: IOOBUF_X52_Y32_N23
\HEX0[4]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode0|display[4]~2_combout\,
	devoe => ww_devoe,
	o => \HEX0[4]~output_o\);

-- Location: IOOBUF_X50_Y41_N2
\HEX0[3]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode0|display[3]~3_combout\,
	devoe => ww_devoe,
	o => \HEX0[3]~output_o\);

-- Location: IOOBUF_X52_Y32_N9
\HEX0[2]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode0|ALT_INV_display[2]~4_combout\,
	devoe => ww_devoe,
	o => \HEX0[2]~output_o\);

-- Location: IOOBUF_X52_Y32_N16
\HEX0[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode0|display[1]~5_combout\,
	devoe => ww_devoe,
	o => \HEX0[1]~output_o\);

-- Location: IOOBUF_X50_Y41_N9
\HEX0[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode0|display[0]~6_combout\,
	devoe => ww_devoe,
	o => \HEX0[0]~output_o\);

-- Location: IOOBUF_X52_Y18_N2
\HEX1[6]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode1|display[6]~0_combout\,
	devoe => ww_devoe,
	o => \HEX1[6]~output_o\);

-- Location: IOOBUF_X52_Y15_N9
\HEX1[5]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode1|display[5]~1_combout\,
	devoe => ww_devoe,
	o => \HEX1[5]~output_o\);

-- Location: IOOBUF_X52_Y15_N2
\HEX1[4]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode1|display[4]~2_combout\,
	devoe => ww_devoe,
	o => \HEX1[4]~output_o\);

-- Location: IOOBUF_X52_Y16_N9
\HEX1[3]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode1|display[3]~3_combout\,
	devoe => ww_devoe,
	o => \HEX1[3]~output_o\);

-- Location: IOOBUF_X52_Y19_N9
\HEX1[2]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode1|ALT_INV_display[2]~4_combout\,
	devoe => ww_devoe,
	o => \HEX1[2]~output_o\);

-- Location: IOOBUF_X52_Y19_N2
\HEX1[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode1|display[1]~5_combout\,
	devoe => ww_devoe,
	o => \HEX1[1]~output_o\);

-- Location: IOOBUF_X52_Y18_N9
\HEX1[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode1|display[0]~6_combout\,
	devoe => ww_devoe,
	o => \HEX1[0]~output_o\);

-- Location: IOOBUF_X38_Y0_N9
\HEX2[6]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode2|display[6]~0_combout\,
	devoe => ww_devoe,
	o => \HEX2[6]~output_o\);

-- Location: IOOBUF_X36_Y0_N9
\HEX2[5]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode2|display[5]~1_combout\,
	devoe => ww_devoe,
	o => \HEX2[5]~output_o\);

-- Location: IOOBUF_X34_Y0_N2
\HEX2[4]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode2|display[4]~2_combout\,
	devoe => ww_devoe,
	o => \HEX2[4]~output_o\);

-- Location: IOOBUF_X36_Y0_N2
\HEX2[3]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode2|display[3]~3_combout\,
	devoe => ww_devoe,
	o => \HEX2[3]~output_o\);

-- Location: IOOBUF_X41_Y0_N23
\HEX2[2]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode2|ALT_INV_display[2]~4_combout\,
	devoe => ww_devoe,
	o => \HEX2[2]~output_o\);

-- Location: IOOBUF_X41_Y0_N9
\HEX2[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode2|display[1]~5_combout\,
	devoe => ww_devoe,
	o => \HEX2[1]~output_o\);

-- Location: IOOBUF_X38_Y0_N2
\HEX2[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode2|display[0]~6_combout\,
	devoe => ww_devoe,
	o => \HEX2[0]~output_o\);

-- Location: IOOBUF_X23_Y41_N9
\HEX3[6]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode3|display[6]~0_combout\,
	devoe => ww_devoe,
	o => \HEX3[6]~output_o\);

-- Location: IOOBUF_X21_Y41_N2
\HEX3[5]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode3|display[5]~1_combout\,
	devoe => ww_devoe,
	o => \HEX3[5]~output_o\);

-- Location: IOOBUF_X23_Y41_N2
\HEX3[4]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode3|display[4]~2_combout\,
	devoe => ww_devoe,
	o => \HEX3[4]~output_o\);

-- Location: IOOBUF_X21_Y41_N9
\HEX3[3]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode3|display[3]~3_combout\,
	devoe => ww_devoe,
	o => \HEX3[3]~output_o\);

-- Location: IOOBUF_X18_Y41_N9
\HEX3[2]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode3|ALT_INV_display[2]~4_combout\,
	devoe => ww_devoe,
	o => \HEX3[2]~output_o\);

-- Location: IOOBUF_X16_Y41_N2
\HEX3[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode3|display[1]~5_combout\,
	devoe => ww_devoe,
	o => \HEX3[1]~output_o\);

-- Location: IOOBUF_X18_Y41_N2
\HEX3[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode3|display[0]~6_combout\,
	devoe => ww_devoe,
	o => \HEX3[0]~output_o\);

-- Location: IOOBUF_X52_Y23_N2
\HEX4[6]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode4|display[6]~0_combout\,
	devoe => ww_devoe,
	o => \HEX4[6]~output_o\);

-- Location: IOOBUF_X52_Y28_N2
\HEX4[5]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode4|display[5]~1_combout\,
	devoe => ww_devoe,
	o => \HEX4[5]~output_o\);

-- Location: IOOBUF_X52_Y27_N9
\HEX4[4]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode4|display[4]~2_combout\,
	devoe => ww_devoe,
	o => \HEX4[4]~output_o\);

-- Location: IOOBUF_X52_Y25_N9
\HEX4[3]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode4|display[3]~3_combout\,
	devoe => ww_devoe,
	o => \HEX4[3]~output_o\);

-- Location: IOOBUF_X52_Y28_N9
\HEX4[2]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode4|ALT_INV_display[2]~4_combout\,
	devoe => ww_devoe,
	o => \HEX4[2]~output_o\);

-- Location: IOOBUF_X52_Y27_N2
\HEX4[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode4|display[1]~5_combout\,
	devoe => ww_devoe,
	o => \HEX4[1]~output_o\);

-- Location: IOOBUF_X52_Y25_N2
\HEX4[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode4|display[0]~6_combout\,
	devoe => ww_devoe,
	o => \HEX4[0]~output_o\);

-- Location: IOOBUF_X52_Y11_N2
\HEX5[6]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode5|display[6]~0_combout\,
	devoe => ww_devoe,
	o => \HEX5[6]~output_o\);

-- Location: IOOBUF_X52_Y10_N9
\HEX5[5]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode5|display[5]~1_combout\,
	devoe => ww_devoe,
	o => \HEX5[5]~output_o\);

-- Location: IOOBUF_X52_Y9_N9
\HEX5[4]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode5|display[4]~2_combout\,
	devoe => ww_devoe,
	o => \HEX5[4]~output_o\);

-- Location: IOOBUF_X52_Y12_N2
\HEX5[3]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode5|display[3]~3_combout\,
	devoe => ww_devoe,
	o => \HEX5[3]~output_o\);

-- Location: IOOBUF_X52_Y11_N9
\HEX5[2]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode5|ALT_INV_display[2]~4_combout\,
	devoe => ww_devoe,
	o => \HEX5[2]~output_o\);

-- Location: IOOBUF_X52_Y10_N2
\HEX5[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode5|display[1]~5_combout\,
	devoe => ww_devoe,
	o => \HEX5[1]~output_o\);

-- Location: IOOBUF_X52_Y9_N2
\HEX5[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode5|display[0]~6_combout\,
	devoe => ww_devoe,
	o => \HEX5[0]~output_o\);

-- Location: IOOBUF_X46_Y0_N9
\HEX6[6]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode6|display[6]~0_combout\,
	devoe => ww_devoe,
	o => \HEX6[6]~output_o\);

-- Location: IOOBUF_X46_Y0_N23
\HEX6[5]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode6|display[5]~1_combout\,
	devoe => ww_devoe,
	o => \HEX6[5]~output_o\);

-- Location: IOOBUF_X48_Y0_N9
\HEX6[4]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode6|display[4]~2_combout\,
	devoe => ww_devoe,
	o => \HEX6[4]~output_o\);

-- Location: IOOBUF_X48_Y0_N2
\HEX6[3]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode6|display[3]~3_combout\,
	devoe => ww_devoe,
	o => \HEX6[3]~output_o\);

-- Location: IOOBUF_X43_Y0_N2
\HEX6[2]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode6|ALT_INV_display[2]~4_combout\,
	devoe => ww_devoe,
	o => \HEX6[2]~output_o\);

-- Location: IOOBUF_X50_Y0_N2
\HEX6[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode6|display[1]~5_combout\,
	devoe => ww_devoe,
	o => \HEX6[1]~output_o\);

-- Location: IOOBUF_X46_Y0_N16
\HEX6[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode6|display[0]~6_combout\,
	devoe => ww_devoe,
	o => \HEX6[0]~output_o\);

-- Location: IOOBUF_X31_Y41_N2
\HEX7[6]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode7|display[6]~0_combout\,
	devoe => ww_devoe,
	o => \HEX7[6]~output_o\);

-- Location: IOOBUF_X34_Y41_N2
\HEX7[5]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode7|display[5]~1_combout\,
	devoe => ww_devoe,
	o => \HEX7[5]~output_o\);

-- Location: IOOBUF_X36_Y41_N2
\HEX7[4]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode7|display[4]~2_combout\,
	devoe => ww_devoe,
	o => \HEX7[4]~output_o\);

-- Location: IOOBUF_X31_Y41_N23
\HEX7[3]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode7|display[3]~3_combout\,
	devoe => ww_devoe,
	o => \HEX7[3]~output_o\);

-- Location: IOOBUF_X34_Y41_N9
\HEX7[2]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode7|ALT_INV_display[2]~4_combout\,
	devoe => ww_devoe,
	o => \HEX7[2]~output_o\);

-- Location: IOOBUF_X31_Y41_N16
\HEX7[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode7|display[1]~5_combout\,
	devoe => ww_devoe,
	o => \HEX7[1]~output_o\);

-- Location: IOOBUF_X29_Y41_N9
\HEX7[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode7|display[0]~6_combout\,
	devoe => ww_devoe,
	o => \HEX7[0]~output_o\);

-- Location: IOIBUF_X52_Y30_N8
\SW[1]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(1),
	o => \SW[1]~input_o\);

-- Location: IOIBUF_X52_Y31_N1
\SW[2]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(2),
	o => \SW[2]~input_o\);

-- Location: IOIBUF_X52_Y30_N1
\SW[3]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(3),
	o => \SW[3]~input_o\);

-- Location: IOIBUF_X52_Y31_N8
\SW[0]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(0),
	o => \SW[0]~input_o\);

-- Location: LCCOMB_X51_Y32_N8
\decode0|display[6]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode0|display[6]~0_combout\ = (\SW[0]~input_o\ & (!\SW[3]~input_o\ & (\SW[1]~input_o\ $ (!\SW[2]~input_o\)))) # (!\SW[0]~input_o\ & (!\SW[1]~input_o\ & (\SW[2]~input_o\ $ (!\SW[3]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100101000001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[1]~input_o\,
	datab => \SW[2]~input_o\,
	datac => \SW[3]~input_o\,
	datad => \SW[0]~input_o\,
	combout => \decode0|display[6]~0_combout\);

-- Location: LCCOMB_X51_Y32_N10
\decode0|display[5]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode0|display[5]~1_combout\ = (\SW[1]~input_o\ & (!\SW[3]~input_o\ & ((\SW[0]~input_o\) # (!\SW[2]~input_o\)))) # (!\SW[1]~input_o\ & (\SW[0]~input_o\ & (\SW[2]~input_o\ $ (!\SW[3]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100101100000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[1]~input_o\,
	datab => \SW[2]~input_o\,
	datac => \SW[3]~input_o\,
	datad => \SW[0]~input_o\,
	combout => \decode0|display[5]~1_combout\);

-- Location: LCCOMB_X51_Y32_N28
\decode0|display[4]~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode0|display[4]~2_combout\ = (\SW[1]~input_o\ & (((!\SW[3]~input_o\ & \SW[0]~input_o\)))) # (!\SW[1]~input_o\ & ((\SW[2]~input_o\ & (!\SW[3]~input_o\)) # (!\SW[2]~input_o\ & ((\SW[0]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001111100000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[1]~input_o\,
	datab => \SW[2]~input_o\,
	datac => \SW[3]~input_o\,
	datad => \SW[0]~input_o\,
	combout => \decode0|display[4]~2_combout\);

-- Location: LCCOMB_X51_Y32_N22
\decode0|display[3]~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode0|display[3]~3_combout\ = (\SW[1]~input_o\ & ((\SW[2]~input_o\ & ((\SW[0]~input_o\))) # (!\SW[2]~input_o\ & (\SW[3]~input_o\ & !\SW[0]~input_o\)))) # (!\SW[1]~input_o\ & (!\SW[3]~input_o\ & (\SW[2]~input_o\ $ (\SW[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100100100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[1]~input_o\,
	datab => \SW[2]~input_o\,
	datac => \SW[3]~input_o\,
	datad => \SW[0]~input_o\,
	combout => \decode0|display[3]~3_combout\);

-- Location: LCCOMB_X51_Y32_N24
\decode0|display[2]~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode0|display[2]~4_combout\ = (\SW[2]~input_o\ & (((!\SW[1]~input_o\ & \SW[0]~input_o\)) # (!\SW[3]~input_o\))) # (!\SW[2]~input_o\ & (((\SW[3]~input_o\) # (\SW[0]~input_o\)) # (!\SW[1]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111111100111101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[1]~input_o\,
	datab => \SW[2]~input_o\,
	datac => \SW[3]~input_o\,
	datad => \SW[0]~input_o\,
	combout => \decode0|display[2]~4_combout\);

-- Location: LCCOMB_X51_Y32_N26
\decode0|display[1]~5\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode0|display[1]~5_combout\ = (\SW[1]~input_o\ & ((\SW[0]~input_o\ & ((\SW[3]~input_o\))) # (!\SW[0]~input_o\ & (\SW[2]~input_o\)))) # (!\SW[1]~input_o\ & (\SW[2]~input_o\ & (\SW[3]~input_o\ $ (\SW[0]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010011001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[1]~input_o\,
	datab => \SW[2]~input_o\,
	datac => \SW[3]~input_o\,
	datad => \SW[0]~input_o\,
	combout => \decode0|display[1]~5_combout\);

-- Location: LCCOMB_X51_Y32_N4
\decode0|display[0]~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode0|display[0]~6_combout\ = (\SW[2]~input_o\ & (!\SW[1]~input_o\ & (\SW[3]~input_o\ $ (!\SW[0]~input_o\)))) # (!\SW[2]~input_o\ & (\SW[0]~input_o\ & (\SW[1]~input_o\ $ (!\SW[3]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000100000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[1]~input_o\,
	datab => \SW[2]~input_o\,
	datac => \SW[3]~input_o\,
	datad => \SW[0]~input_o\,
	combout => \decode0|display[0]~6_combout\);

-- Location: IOIBUF_X52_Y13_N1
\SW[6]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(6),
	o => \SW[6]~input_o\);

-- Location: IOIBUF_X52_Y13_N8
\SW[7]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(7),
	o => \SW[7]~input_o\);

-- Location: IOIBUF_X52_Y16_N1
\SW[4]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(4),
	o => \SW[4]~input_o\);

-- Location: IOIBUF_X52_Y12_N8
\SW[5]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(5),
	o => \SW[5]~input_o\);

-- Location: LCCOMB_X51_Y15_N16
\decode1|display[6]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode1|display[6]~0_combout\ = (\SW[4]~input_o\ & (!\SW[7]~input_o\ & (\SW[6]~input_o\ $ (!\SW[5]~input_o\)))) # (!\SW[4]~input_o\ & (!\SW[5]~input_o\ & (\SW[6]~input_o\ $ (!\SW[7]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000011001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[6]~input_o\,
	datab => \SW[7]~input_o\,
	datac => \SW[4]~input_o\,
	datad => \SW[5]~input_o\,
	combout => \decode1|display[6]~0_combout\);

-- Location: LCCOMB_X51_Y15_N2
\decode1|display[5]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode1|display[5]~1_combout\ = (\SW[6]~input_o\ & (\SW[4]~input_o\ & (\SW[7]~input_o\ $ (\SW[5]~input_o\)))) # (!\SW[6]~input_o\ & (!\SW[7]~input_o\ & ((\SW[4]~input_o\) # (\SW[5]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000110010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[6]~input_o\,
	datab => \SW[7]~input_o\,
	datac => \SW[4]~input_o\,
	datad => \SW[5]~input_o\,
	combout => \decode1|display[5]~1_combout\);

-- Location: LCCOMB_X51_Y15_N4
\decode1|display[4]~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode1|display[4]~2_combout\ = (\SW[5]~input_o\ & (((!\SW[7]~input_o\ & \SW[4]~input_o\)))) # (!\SW[5]~input_o\ & ((\SW[6]~input_o\ & (!\SW[7]~input_o\)) # (!\SW[6]~input_o\ & ((\SW[4]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000001110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[6]~input_o\,
	datab => \SW[7]~input_o\,
	datac => \SW[4]~input_o\,
	datad => \SW[5]~input_o\,
	combout => \decode1|display[4]~2_combout\);

-- Location: LCCOMB_X51_Y15_N30
\decode1|display[3]~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode1|display[3]~3_combout\ = (\SW[5]~input_o\ & ((\SW[6]~input_o\ & ((\SW[4]~input_o\))) # (!\SW[6]~input_o\ & (\SW[7]~input_o\ & !\SW[4]~input_o\)))) # (!\SW[5]~input_o\ & (!\SW[7]~input_o\ & (\SW[6]~input_o\ $ (\SW[4]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010000010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[6]~input_o\,
	datab => \SW[7]~input_o\,
	datac => \SW[4]~input_o\,
	datad => \SW[5]~input_o\,
	combout => \decode1|display[3]~3_combout\);

-- Location: LCCOMB_X51_Y15_N24
\decode1|display[2]~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode1|display[2]~4_combout\ = (\SW[6]~input_o\ & (((\SW[4]~input_o\ & !\SW[5]~input_o\)) # (!\SW[7]~input_o\))) # (!\SW[6]~input_o\ & ((\SW[7]~input_o\) # ((\SW[4]~input_o\) # (!\SW[5]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011011110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[6]~input_o\,
	datab => \SW[7]~input_o\,
	datac => \SW[4]~input_o\,
	datad => \SW[5]~input_o\,
	combout => \decode1|display[2]~4_combout\);

-- Location: LCCOMB_X51_Y15_N18
\decode1|display[1]~5\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode1|display[1]~5_combout\ = (\SW[7]~input_o\ & ((\SW[4]~input_o\ & ((\SW[5]~input_o\))) # (!\SW[4]~input_o\ & (\SW[6]~input_o\)))) # (!\SW[7]~input_o\ & (\SW[6]~input_o\ & (\SW[4]~input_o\ $ (\SW[5]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101000101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[6]~input_o\,
	datab => \SW[7]~input_o\,
	datac => \SW[4]~input_o\,
	datad => \SW[5]~input_o\,
	combout => \decode1|display[1]~5_combout\);

-- Location: LCCOMB_X51_Y15_N12
\decode1|display[0]~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode1|display[0]~6_combout\ = (\SW[6]~input_o\ & (!\SW[5]~input_o\ & (\SW[7]~input_o\ $ (!\SW[4]~input_o\)))) # (!\SW[6]~input_o\ & (\SW[4]~input_o\ & (\SW[7]~input_o\ $ (!\SW[5]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000010010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[6]~input_o\,
	datab => \SW[7]~input_o\,
	datac => \SW[4]~input_o\,
	datad => \SW[5]~input_o\,
	combout => \decode1|display[0]~6_combout\);

-- Location: IOIBUF_X43_Y0_N8
\SW[11]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(11),
	o => \SW[11]~input_o\);

-- Location: IOIBUF_X41_Y0_N1
\SW[8]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(8),
	o => \SW[8]~input_o\);

-- Location: IOIBUF_X41_Y0_N15
\SW[10]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(10),
	o => \SW[10]~input_o\);

-- Location: IOIBUF_X46_Y0_N1
\SW[9]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(9),
	o => \SW[9]~input_o\);

-- Location: LCCOMB_X41_Y1_N8
\decode2|display[6]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode2|display[6]~0_combout\ = (\SW[8]~input_o\ & (!\SW[11]~input_o\ & (\SW[10]~input_o\ $ (!\SW[9]~input_o\)))) # (!\SW[8]~input_o\ & (!\SW[9]~input_o\ & (\SW[11]~input_o\ $ (!\SW[10]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000100101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[11]~input_o\,
	datab => \SW[8]~input_o\,
	datac => \SW[10]~input_o\,
	datad => \SW[9]~input_o\,
	combout => \decode2|display[6]~0_combout\);

-- Location: LCCOMB_X41_Y1_N2
\decode2|display[5]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode2|display[5]~1_combout\ = (\SW[8]~input_o\ & (\SW[11]~input_o\ $ (((\SW[9]~input_o\) # (!\SW[10]~input_o\))))) # (!\SW[8]~input_o\ & (!\SW[11]~input_o\ & (!\SW[10]~input_o\ & \SW[9]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010110000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[11]~input_o\,
	datab => \SW[8]~input_o\,
	datac => \SW[10]~input_o\,
	datad => \SW[9]~input_o\,
	combout => \decode2|display[5]~1_combout\);

-- Location: LCCOMB_X41_Y1_N12
\decode2|display[4]~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode2|display[4]~2_combout\ = (\SW[9]~input_o\ & (!\SW[11]~input_o\ & (\SW[8]~input_o\))) # (!\SW[9]~input_o\ & ((\SW[10]~input_o\ & (!\SW[11]~input_o\)) # (!\SW[10]~input_o\ & ((\SW[8]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010001011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[11]~input_o\,
	datab => \SW[8]~input_o\,
	datac => \SW[10]~input_o\,
	datad => \SW[9]~input_o\,
	combout => \decode2|display[4]~2_combout\);

-- Location: LCCOMB_X41_Y1_N6
\decode2|display[3]~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode2|display[3]~3_combout\ = (\SW[9]~input_o\ & ((\SW[8]~input_o\ & ((\SW[10]~input_o\))) # (!\SW[8]~input_o\ & (\SW[11]~input_o\ & !\SW[10]~input_o\)))) # (!\SW[9]~input_o\ & (!\SW[11]~input_o\ & (\SW[8]~input_o\ $ (\SW[10]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001000010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[11]~input_o\,
	datab => \SW[8]~input_o\,
	datac => \SW[10]~input_o\,
	datad => \SW[9]~input_o\,
	combout => \decode2|display[3]~3_combout\);

-- Location: LCCOMB_X41_Y1_N24
\decode2|display[2]~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode2|display[2]~4_combout\ = (\SW[11]~input_o\ & (((\SW[8]~input_o\ & !\SW[9]~input_o\)) # (!\SW[10]~input_o\))) # (!\SW[11]~input_o\ & ((\SW[8]~input_o\) # ((\SW[10]~input_o\) # (!\SW[9]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111011011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[11]~input_o\,
	datab => \SW[8]~input_o\,
	datac => \SW[10]~input_o\,
	datad => \SW[9]~input_o\,
	combout => \decode2|display[2]~4_combout\);

-- Location: LCCOMB_X41_Y1_N18
\decode2|display[1]~5\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode2|display[1]~5_combout\ = (\SW[11]~input_o\ & ((\SW[8]~input_o\ & ((\SW[9]~input_o\))) # (!\SW[8]~input_o\ & (\SW[10]~input_o\)))) # (!\SW[11]~input_o\ & (\SW[10]~input_o\ & (\SW[8]~input_o\ $ (\SW[9]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100001100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[11]~input_o\,
	datab => \SW[8]~input_o\,
	datac => \SW[10]~input_o\,
	datad => \SW[9]~input_o\,
	combout => \decode2|display[1]~5_combout\);

-- Location: LCCOMB_X41_Y1_N4
\decode2|display[0]~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode2|display[0]~6_combout\ = (\SW[11]~input_o\ & (\SW[8]~input_o\ & (\SW[10]~input_o\ $ (\SW[9]~input_o\)))) # (!\SW[11]~input_o\ & (!\SW[9]~input_o\ & (\SW[8]~input_o\ $ (\SW[10]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100010010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[11]~input_o\,
	datab => \SW[8]~input_o\,
	datac => \SW[10]~input_o\,
	datad => \SW[9]~input_o\,
	combout => \decode2|display[0]~6_combout\);

-- Location: IOIBUF_X25_Y41_N1
\SW[14]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(14),
	o => \SW[14]~input_o\);

-- Location: IOIBUF_X29_Y41_N1
\SW[15]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(15),
	o => \SW[15]~input_o\);

-- Location: IOIBUF_X31_Y41_N8
\SW[12]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(12),
	o => \SW[12]~input_o\);

-- Location: IOIBUF_X25_Y41_N8
\SW[13]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(13),
	o => \SW[13]~input_o\);

-- Location: LCCOMB_X23_Y40_N0
\decode3|display[6]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode3|display[6]~0_combout\ = (\SW[12]~input_o\ & (!\SW[15]~input_o\ & (\SW[14]~input_o\ $ (!\SW[13]~input_o\)))) # (!\SW[12]~input_o\ & (!\SW[13]~input_o\ & (\SW[14]~input_o\ $ (!\SW[15]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000000011001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[14]~input_o\,
	datab => \SW[15]~input_o\,
	datac => \SW[12]~input_o\,
	datad => \SW[13]~input_o\,
	combout => \decode3|display[6]~0_combout\);

-- Location: LCCOMB_X23_Y40_N2
\decode3|display[5]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode3|display[5]~1_combout\ = (\SW[14]~input_o\ & (\SW[12]~input_o\ & (\SW[15]~input_o\ $ (\SW[13]~input_o\)))) # (!\SW[14]~input_o\ & (!\SW[15]~input_o\ & ((\SW[12]~input_o\) # (\SW[13]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000110010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[14]~input_o\,
	datab => \SW[15]~input_o\,
	datac => \SW[12]~input_o\,
	datad => \SW[13]~input_o\,
	combout => \decode3|display[5]~1_combout\);

-- Location: LCCOMB_X23_Y40_N20
\decode3|display[4]~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode3|display[4]~2_combout\ = (\SW[13]~input_o\ & (((!\SW[15]~input_o\ & \SW[12]~input_o\)))) # (!\SW[13]~input_o\ & ((\SW[14]~input_o\ & (!\SW[15]~input_o\)) # (!\SW[14]~input_o\ & ((\SW[12]~input_o\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000001110010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[14]~input_o\,
	datab => \SW[15]~input_o\,
	datac => \SW[12]~input_o\,
	datad => \SW[13]~input_o\,
	combout => \decode3|display[4]~2_combout\);

-- Location: LCCOMB_X23_Y40_N22
\decode3|display[3]~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode3|display[3]~3_combout\ = (\SW[13]~input_o\ & ((\SW[14]~input_o\ & ((\SW[12]~input_o\))) # (!\SW[14]~input_o\ & (\SW[15]~input_o\ & !\SW[12]~input_o\)))) # (!\SW[13]~input_o\ & (!\SW[15]~input_o\ & (\SW[14]~input_o\ $ (\SW[12]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010000010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[14]~input_o\,
	datab => \SW[15]~input_o\,
	datac => \SW[12]~input_o\,
	datad => \SW[13]~input_o\,
	combout => \decode3|display[3]~3_combout\);

-- Location: LCCOMB_X23_Y40_N24
\decode3|display[2]~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode3|display[2]~4_combout\ = (\SW[14]~input_o\ & (((\SW[12]~input_o\ & !\SW[13]~input_o\)) # (!\SW[15]~input_o\))) # (!\SW[14]~input_o\ & ((\SW[15]~input_o\) # ((\SW[12]~input_o\) # (!\SW[13]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011011110111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[14]~input_o\,
	datab => \SW[15]~input_o\,
	datac => \SW[12]~input_o\,
	datad => \SW[13]~input_o\,
	combout => \decode3|display[2]~4_combout\);

-- Location: LCCOMB_X23_Y40_N10
\decode3|display[1]~5\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode3|display[1]~5_combout\ = (\SW[15]~input_o\ & ((\SW[12]~input_o\ & ((\SW[13]~input_o\))) # (!\SW[12]~input_o\ & (\SW[14]~input_o\)))) # (!\SW[15]~input_o\ & (\SW[14]~input_o\ & (\SW[12]~input_o\ $ (\SW[13]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100101000101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[14]~input_o\,
	datab => \SW[15]~input_o\,
	datac => \SW[12]~input_o\,
	datad => \SW[13]~input_o\,
	combout => \decode3|display[1]~5_combout\);

-- Location: LCCOMB_X23_Y40_N12
\decode3|display[0]~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode3|display[0]~6_combout\ = (\SW[14]~input_o\ & (!\SW[13]~input_o\ & (\SW[15]~input_o\ $ (!\SW[12]~input_o\)))) # (!\SW[14]~input_o\ & (\SW[12]~input_o\ & (\SW[15]~input_o\ $ (!\SW[13]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000010010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[14]~input_o\,
	datab => \SW[15]~input_o\,
	datac => \SW[12]~input_o\,
	datad => \SW[13]~input_o\,
	combout => \decode3|display[0]~6_combout\);

-- Location: IOIBUF_X27_Y0_N15
\KEY[0]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(0),
	o => \KEY[0]~input_o\);

-- Location: CLKCTRL_G17
\KEY[0]~inputclkctrl\ : cycloneiv_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \KEY[0]~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \KEY[0]~inputclkctrl_outclk\);

-- Location: LCCOMB_X51_Y28_N12
\latch3|Q\ : cycloneiv_lcell_comb
-- Equation(s):
-- \latch3|Q~combout\ = (GLOBAL(\KEY[0]~inputclkctrl_outclk\) & ((\SW[3]~input_o\))) # (!GLOBAL(\KEY[0]~inputclkctrl_outclk\) & (\latch3|Q~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch3|Q~combout\,
	datac => \SW[3]~input_o\,
	datad => \KEY[0]~inputclkctrl_outclk\,
	combout => \latch3|Q~combout\);

-- Location: LCCOMB_X51_Y28_N16
\latch0|Q\ : cycloneiv_lcell_comb
-- Equation(s):
-- \latch0|Q~combout\ = (GLOBAL(\KEY[0]~inputclkctrl_outclk\) & ((\SW[0]~input_o\))) # (!GLOBAL(\KEY[0]~inputclkctrl_outclk\) & (\latch0|Q~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \latch0|Q~combout\,
	datac => \SW[0]~input_o\,
	datad => \KEY[0]~inputclkctrl_outclk\,
	combout => \latch0|Q~combout\);

-- Location: LCCOMB_X51_Y28_N6
\latch2|Q\ : cycloneiv_lcell_comb
-- Equation(s):
-- \latch2|Q~combout\ = (GLOBAL(\KEY[0]~inputclkctrl_outclk\) & (\SW[2]~input_o\)) # (!GLOBAL(\KEY[0]~inputclkctrl_outclk\) & ((\latch2|Q~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[2]~input_o\,
	datac => \KEY[0]~inputclkctrl_outclk\,
	datad => \latch2|Q~combout\,
	combout => \latch2|Q~combout\);

-- Location: LCCOMB_X51_Y28_N10
\latch1|Q\ : cycloneiv_lcell_comb
-- Equation(s):
-- \latch1|Q~combout\ = (GLOBAL(\KEY[0]~inputclkctrl_outclk\) & (\SW[1]~input_o\)) # (!GLOBAL(\KEY[0]~inputclkctrl_outclk\) & ((\latch1|Q~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SW[1]~input_o\,
	datac => \KEY[0]~inputclkctrl_outclk\,
	datad => \latch1|Q~combout\,
	combout => \latch1|Q~combout\);

-- Location: LCCOMB_X51_Y28_N8
\decode4|display[6]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode4|display[6]~0_combout\ = (\latch0|Q~combout\ & (!\latch3|Q~combout\ & (\latch2|Q~combout\ $ (!\latch1|Q~combout\)))) # (!\latch0|Q~combout\ & (!\latch1|Q~combout\ & (\latch3|Q~combout\ $ (!\latch2|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000100101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch3|Q~combout\,
	datab => \latch0|Q~combout\,
	datac => \latch2|Q~combout\,
	datad => \latch1|Q~combout\,
	combout => \decode4|display[6]~0_combout\);

-- Location: LCCOMB_X51_Y28_N26
\decode4|display[5]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode4|display[5]~1_combout\ = (\latch0|Q~combout\ & (\latch3|Q~combout\ $ (((\latch1|Q~combout\) # (!\latch2|Q~combout\))))) # (!\latch0|Q~combout\ & (!\latch3|Q~combout\ & (!\latch2|Q~combout\ & \latch1|Q~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010110000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch3|Q~combout\,
	datab => \latch0|Q~combout\,
	datac => \latch2|Q~combout\,
	datad => \latch1|Q~combout\,
	combout => \decode4|display[5]~1_combout\);

-- Location: LCCOMB_X51_Y28_N20
\decode4|display[4]~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode4|display[4]~2_combout\ = (\latch1|Q~combout\ & (!\latch3|Q~combout\ & (\latch0|Q~combout\))) # (!\latch1|Q~combout\ & ((\latch2|Q~combout\ & (!\latch3|Q~combout\)) # (!\latch2|Q~combout\ & ((\latch0|Q~combout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100010001011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch3|Q~combout\,
	datab => \latch0|Q~combout\,
	datac => \latch2|Q~combout\,
	datad => \latch1|Q~combout\,
	combout => \decode4|display[4]~2_combout\);

-- Location: LCCOMB_X51_Y28_N22
\decode4|display[3]~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode4|display[3]~3_combout\ = (\latch1|Q~combout\ & ((\latch0|Q~combout\ & ((\latch2|Q~combout\))) # (!\latch0|Q~combout\ & (\latch3|Q~combout\ & !\latch2|Q~combout\)))) # (!\latch1|Q~combout\ & (!\latch3|Q~combout\ & (\latch0|Q~combout\ $ 
-- (\latch2|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001000010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch3|Q~combout\,
	datab => \latch0|Q~combout\,
	datac => \latch2|Q~combout\,
	datad => \latch1|Q~combout\,
	combout => \decode4|display[3]~3_combout\);

-- Location: LCCOMB_X51_Y28_N24
\decode4|display[2]~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode4|display[2]~4_combout\ = (\latch3|Q~combout\ & (((\latch0|Q~combout\ & !\latch1|Q~combout\)) # (!\latch2|Q~combout\))) # (!\latch3|Q~combout\ & ((\latch0|Q~combout\) # ((\latch2|Q~combout\) # (!\latch1|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101111011011111",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch3|Q~combout\,
	datab => \latch0|Q~combout\,
	datac => \latch2|Q~combout\,
	datad => \latch1|Q~combout\,
	combout => \decode4|display[2]~4_combout\);

-- Location: LCCOMB_X51_Y28_N2
\decode4|display[1]~5\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode4|display[1]~5_combout\ = (\latch3|Q~combout\ & ((\latch0|Q~combout\ & ((\latch1|Q~combout\))) # (!\latch0|Q~combout\ & (\latch2|Q~combout\)))) # (!\latch3|Q~combout\ & (\latch2|Q~combout\ & (\latch0|Q~combout\ $ (\latch1|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100001100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch3|Q~combout\,
	datab => \latch0|Q~combout\,
	datac => \latch2|Q~combout\,
	datad => \latch1|Q~combout\,
	combout => \decode4|display[1]~5_combout\);

-- Location: LCCOMB_X51_Y28_N28
\decode4|display[0]~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode4|display[0]~6_combout\ = (\latch3|Q~combout\ & (\latch0|Q~combout\ & (\latch2|Q~combout\ $ (\latch1|Q~combout\)))) # (!\latch3|Q~combout\ & (!\latch1|Q~combout\ & (\latch0|Q~combout\ $ (\latch2|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100010010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch3|Q~combout\,
	datab => \latch0|Q~combout\,
	datac => \latch2|Q~combout\,
	datad => \latch1|Q~combout\,
	combout => \decode4|display[0]~6_combout\);

-- Location: LCCOMB_X51_Y11_N10
\latch5|Q\ : cycloneiv_lcell_comb
-- Equation(s):
-- \latch5|Q~combout\ = (GLOBAL(\KEY[0]~inputclkctrl_outclk\) & ((\SW[5]~input_o\))) # (!GLOBAL(\KEY[0]~inputclkctrl_outclk\) & (\latch5|Q~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch5|Q~combout\,
	datac => \SW[5]~input_o\,
	datad => \KEY[0]~inputclkctrl_outclk\,
	combout => \latch5|Q~combout\);

-- Location: LCCOMB_X51_Y11_N0
\latch4|Q\ : cycloneiv_lcell_comb
-- Equation(s):
-- \latch4|Q~combout\ = (GLOBAL(\KEY[0]~inputclkctrl_outclk\) & (\SW[4]~input_o\)) # (!GLOBAL(\KEY[0]~inputclkctrl_outclk\) & ((\latch4|Q~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SW[4]~input_o\,
	datac => \KEY[0]~inputclkctrl_outclk\,
	datad => \latch4|Q~combout\,
	combout => \latch4|Q~combout\);

-- Location: LCCOMB_X51_Y11_N6
\latch6|Q\ : cycloneiv_lcell_comb
-- Equation(s):
-- \latch6|Q~combout\ = (GLOBAL(\KEY[0]~inputclkctrl_outclk\) & ((\SW[6]~input_o\))) # (!GLOBAL(\KEY[0]~inputclkctrl_outclk\) & (\latch6|Q~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch6|Q~combout\,
	datac => \SW[6]~input_o\,
	datad => \KEY[0]~inputclkctrl_outclk\,
	combout => \latch6|Q~combout\);

-- Location: LCCOMB_X51_Y11_N12
\latch7|Q\ : cycloneiv_lcell_comb
-- Equation(s):
-- \latch7|Q~combout\ = (GLOBAL(\KEY[0]~inputclkctrl_outclk\) & (\SW[7]~input_o\)) # (!GLOBAL(\KEY[0]~inputclkctrl_outclk\) & ((\latch7|Q~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SW[7]~input_o\,
	datac => \KEY[0]~inputclkctrl_outclk\,
	datad => \latch7|Q~combout\,
	combout => \latch7|Q~combout\);

-- Location: LCCOMB_X51_Y11_N8
\decode5|display[6]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode5|display[6]~0_combout\ = (\latch4|Q~combout\ & (!\latch7|Q~combout\ & (\latch5|Q~combout\ $ (!\latch6|Q~combout\)))) # (!\latch4|Q~combout\ & (!\latch5|Q~combout\ & (\latch6|Q~combout\ $ (!\latch7|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000010000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch5|Q~combout\,
	datab => \latch4|Q~combout\,
	datac => \latch6|Q~combout\,
	datad => \latch7|Q~combout\,
	combout => \decode5|display[6]~0_combout\);

-- Location: LCCOMB_X51_Y11_N26
\decode5|display[5]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode5|display[5]~1_combout\ = (\latch5|Q~combout\ & (!\latch7|Q~combout\ & ((\latch4|Q~combout\) # (!\latch6|Q~combout\)))) # (!\latch5|Q~combout\ & (\latch4|Q~combout\ & (\latch6|Q~combout\ $ (!\latch7|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000010001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch5|Q~combout\,
	datab => \latch4|Q~combout\,
	datac => \latch6|Q~combout\,
	datad => \latch7|Q~combout\,
	combout => \decode5|display[5]~1_combout\);

-- Location: LCCOMB_X51_Y11_N20
\decode5|display[4]~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode5|display[4]~2_combout\ = (\latch5|Q~combout\ & (\latch4|Q~combout\ & ((!\latch7|Q~combout\)))) # (!\latch5|Q~combout\ & ((\latch6|Q~combout\ & ((!\latch7|Q~combout\))) # (!\latch6|Q~combout\ & (\latch4|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010011011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch5|Q~combout\,
	datab => \latch4|Q~combout\,
	datac => \latch6|Q~combout\,
	datad => \latch7|Q~combout\,
	combout => \decode5|display[4]~2_combout\);

-- Location: LCCOMB_X51_Y11_N30
\decode5|display[3]~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode5|display[3]~3_combout\ = (\latch5|Q~combout\ & ((\latch4|Q~combout\ & (\latch6|Q~combout\)) # (!\latch4|Q~combout\ & (!\latch6|Q~combout\ & \latch7|Q~combout\)))) # (!\latch5|Q~combout\ & (!\latch7|Q~combout\ & (\latch4|Q~combout\ $ 
-- (\latch6|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001010010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch5|Q~combout\,
	datab => \latch4|Q~combout\,
	datac => \latch6|Q~combout\,
	datad => \latch7|Q~combout\,
	combout => \decode5|display[3]~3_combout\);

-- Location: LCCOMB_X51_Y11_N24
\decode5|display[2]~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode5|display[2]~4_combout\ = (\latch6|Q~combout\ & (((!\latch5|Q~combout\ & \latch4|Q~combout\)) # (!\latch7|Q~combout\))) # (!\latch6|Q~combout\ & (((\latch4|Q~combout\) # (\latch7|Q~combout\)) # (!\latch5|Q~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100111111111101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch5|Q~combout\,
	datab => \latch4|Q~combout\,
	datac => \latch6|Q~combout\,
	datad => \latch7|Q~combout\,
	combout => \decode5|display[2]~4_combout\);

-- Location: LCCOMB_X51_Y11_N18
\decode5|display[1]~5\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode5|display[1]~5_combout\ = (\latch5|Q~combout\ & ((\latch4|Q~combout\ & ((\latch7|Q~combout\))) # (!\latch4|Q~combout\ & (\latch6|Q~combout\)))) # (!\latch5|Q~combout\ & (\latch6|Q~combout\ & (\latch4|Q~combout\ $ (\latch7|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100001100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch5|Q~combout\,
	datab => \latch4|Q~combout\,
	datac => \latch6|Q~combout\,
	datad => \latch7|Q~combout\,
	combout => \decode5|display[1]~5_combout\);

-- Location: LCCOMB_X51_Y11_N4
\decode5|display[0]~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode5|display[0]~6_combout\ = (\latch6|Q~combout\ & (!\latch5|Q~combout\ & (\latch4|Q~combout\ $ (!\latch7|Q~combout\)))) # (!\latch6|Q~combout\ & (\latch4|Q~combout\ & (\latch5|Q~combout\ $ (!\latch7|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100000010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch5|Q~combout\,
	datab => \latch4|Q~combout\,
	datac => \latch6|Q~combout\,
	datad => \latch7|Q~combout\,
	combout => \decode5|display[0]~6_combout\);

-- Location: LCCOMB_X46_Y1_N10
\latch9|Q\ : cycloneiv_lcell_comb
-- Equation(s):
-- \latch9|Q~combout\ = (GLOBAL(\KEY[0]~inputclkctrl_outclk\) & (\SW[9]~input_o\)) # (!GLOBAL(\KEY[0]~inputclkctrl_outclk\) & ((\latch9|Q~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010111110100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[9]~input_o\,
	datac => \KEY[0]~inputclkctrl_outclk\,
	datad => \latch9|Q~combout\,
	combout => \latch9|Q~combout\);

-- Location: LCCOMB_X46_Y1_N24
\latch8|Q\ : cycloneiv_lcell_comb
-- Equation(s):
-- \latch8|Q~combout\ = (GLOBAL(\KEY[0]~inputclkctrl_outclk\) & (\SW[8]~input_o\)) # (!GLOBAL(\KEY[0]~inputclkctrl_outclk\) & ((\latch8|Q~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100111111000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SW[8]~input_o\,
	datac => \KEY[0]~inputclkctrl_outclk\,
	datad => \latch8|Q~combout\,
	combout => \latch8|Q~combout\);

-- Location: LCCOMB_X46_Y1_N6
\latch10|Q\ : cycloneiv_lcell_comb
-- Equation(s):
-- \latch10|Q~combout\ = (GLOBAL(\KEY[0]~inputclkctrl_outclk\) & ((\SW[10]~input_o\))) # (!GLOBAL(\KEY[0]~inputclkctrl_outclk\) & (\latch10|Q~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000010101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch10|Q~combout\,
	datac => \SW[10]~input_o\,
	datad => \KEY[0]~inputclkctrl_outclk\,
	combout => \latch10|Q~combout\);

-- Location: LCCOMB_X46_Y1_N28
\latch11|Q\ : cycloneiv_lcell_comb
-- Equation(s):
-- \latch11|Q~combout\ = (GLOBAL(\KEY[0]~inputclkctrl_outclk\) & ((\SW[11]~input_o\))) # (!GLOBAL(\KEY[0]~inputclkctrl_outclk\) & (\latch11|Q~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \latch11|Q~combout\,
	datac => \SW[11]~input_o\,
	datad => \KEY[0]~inputclkctrl_outclk\,
	combout => \latch11|Q~combout\);

-- Location: LCCOMB_X46_Y1_N8
\decode6|display[6]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode6|display[6]~0_combout\ = (\latch8|Q~combout\ & (!\latch11|Q~combout\ & (\latch9|Q~combout\ $ (!\latch10|Q~combout\)))) # (!\latch8|Q~combout\ & (!\latch9|Q~combout\ & (\latch10|Q~combout\ $ (!\latch11|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000010000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch9|Q~combout\,
	datab => \latch8|Q~combout\,
	datac => \latch10|Q~combout\,
	datad => \latch11|Q~combout\,
	combout => \decode6|display[6]~0_combout\);

-- Location: LCCOMB_X46_Y1_N26
\decode6|display[5]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode6|display[5]~1_combout\ = (\latch9|Q~combout\ & (!\latch11|Q~combout\ & ((\latch8|Q~combout\) # (!\latch10|Q~combout\)))) # (!\latch9|Q~combout\ & (\latch8|Q~combout\ & (\latch10|Q~combout\ $ (!\latch11|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000010001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch9|Q~combout\,
	datab => \latch8|Q~combout\,
	datac => \latch10|Q~combout\,
	datad => \latch11|Q~combout\,
	combout => \decode6|display[5]~1_combout\);

-- Location: LCCOMB_X46_Y1_N4
\decode6|display[4]~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode6|display[4]~2_combout\ = (\latch9|Q~combout\ & (\latch8|Q~combout\ & ((!\latch11|Q~combout\)))) # (!\latch9|Q~combout\ & ((\latch10|Q~combout\ & ((!\latch11|Q~combout\))) # (!\latch10|Q~combout\ & (\latch8|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010011011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch9|Q~combout\,
	datab => \latch8|Q~combout\,
	datac => \latch10|Q~combout\,
	datad => \latch11|Q~combout\,
	combout => \decode6|display[4]~2_combout\);

-- Location: LCCOMB_X46_Y1_N22
\decode6|display[3]~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode6|display[3]~3_combout\ = (\latch9|Q~combout\ & ((\latch8|Q~combout\ & (\latch10|Q~combout\)) # (!\latch8|Q~combout\ & (!\latch10|Q~combout\ & \latch11|Q~combout\)))) # (!\latch9|Q~combout\ & (!\latch11|Q~combout\ & (\latch8|Q~combout\ $ 
-- (\latch10|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001010010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch9|Q~combout\,
	datab => \latch8|Q~combout\,
	datac => \latch10|Q~combout\,
	datad => \latch11|Q~combout\,
	combout => \decode6|display[3]~3_combout\);

-- Location: LCCOMB_X46_Y1_N0
\decode6|display[2]~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode6|display[2]~4_combout\ = (\latch10|Q~combout\ & (((!\latch9|Q~combout\ & \latch8|Q~combout\)) # (!\latch11|Q~combout\))) # (!\latch10|Q~combout\ & (((\latch8|Q~combout\) # (\latch11|Q~combout\)) # (!\latch9|Q~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100111111111101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch9|Q~combout\,
	datab => \latch8|Q~combout\,
	datac => \latch10|Q~combout\,
	datad => \latch11|Q~combout\,
	combout => \decode6|display[2]~4_combout\);

-- Location: LCCOMB_X46_Y1_N18
\decode6|display[1]~5\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode6|display[1]~5_combout\ = (\latch9|Q~combout\ & ((\latch8|Q~combout\ & ((\latch11|Q~combout\))) # (!\latch8|Q~combout\ & (\latch10|Q~combout\)))) # (!\latch9|Q~combout\ & (\latch10|Q~combout\ & (\latch8|Q~combout\ $ (\latch11|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100001100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch9|Q~combout\,
	datab => \latch8|Q~combout\,
	datac => \latch10|Q~combout\,
	datad => \latch11|Q~combout\,
	combout => \decode6|display[1]~5_combout\);

-- Location: LCCOMB_X46_Y1_N20
\decode6|display[0]~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode6|display[0]~6_combout\ = (\latch10|Q~combout\ & (!\latch9|Q~combout\ & (\latch8|Q~combout\ $ (!\latch11|Q~combout\)))) # (!\latch10|Q~combout\ & (\latch8|Q~combout\ & (\latch9|Q~combout\ $ (!\latch11|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100000010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch9|Q~combout\,
	datab => \latch8|Q~combout\,
	datac => \latch10|Q~combout\,
	datad => \latch11|Q~combout\,
	combout => \decode6|display[0]~6_combout\);

-- Location: LCCOMB_X31_Y40_N10
\latch13|Q\ : cycloneiv_lcell_comb
-- Equation(s):
-- \latch13|Q~combout\ = (GLOBAL(\KEY[0]~inputclkctrl_outclk\) & (\SW[13]~input_o\)) # (!GLOBAL(\KEY[0]~inputclkctrl_outclk\) & ((\latch13|Q~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[13]~input_o\,
	datac => \latch13|Q~combout\,
	datad => \KEY[0]~inputclkctrl_outclk\,
	combout => \latch13|Q~combout\);

-- Location: LCCOMB_X31_Y40_N8
\latch12|Q\ : cycloneiv_lcell_comb
-- Equation(s):
-- \latch12|Q~combout\ = (GLOBAL(\KEY[0]~inputclkctrl_outclk\) & (\SW[12]~input_o\)) # (!GLOBAL(\KEY[0]~inputclkctrl_outclk\) & ((\latch12|Q~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SW[12]~input_o\,
	datac => \latch12|Q~combout\,
	datad => \KEY[0]~inputclkctrl_outclk\,
	combout => \latch12|Q~combout\);

-- Location: LCCOMB_X31_Y40_N6
\latch14|Q\ : cycloneiv_lcell_comb
-- Equation(s):
-- \latch14|Q~combout\ = (GLOBAL(\KEY[0]~inputclkctrl_outclk\) & (\SW[14]~input_o\)) # (!GLOBAL(\KEY[0]~inputclkctrl_outclk\) & ((\latch14|Q~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \SW[14]~input_o\,
	datac => \latch14|Q~combout\,
	datad => \KEY[0]~inputclkctrl_outclk\,
	combout => \latch14|Q~combout\);

-- Location: LCCOMB_X31_Y40_N28
\latch15|Q\ : cycloneiv_lcell_comb
-- Equation(s):
-- \latch15|Q~combout\ = (GLOBAL(\KEY[0]~inputclkctrl_outclk\) & ((\SW[15]~input_o\))) # (!GLOBAL(\KEY[0]~inputclkctrl_outclk\) & (\latch15|Q~combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000011001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \latch15|Q~combout\,
	datac => \SW[15]~input_o\,
	datad => \KEY[0]~inputclkctrl_outclk\,
	combout => \latch15|Q~combout\);

-- Location: LCCOMB_X31_Y40_N16
\decode7|display[6]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode7|display[6]~0_combout\ = (\latch12|Q~combout\ & (!\latch15|Q~combout\ & (\latch13|Q~combout\ $ (!\latch14|Q~combout\)))) # (!\latch12|Q~combout\ & (!\latch13|Q~combout\ & (\latch14|Q~combout\ $ (!\latch15|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000010000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch13|Q~combout\,
	datab => \latch12|Q~combout\,
	datac => \latch14|Q~combout\,
	datad => \latch15|Q~combout\,
	combout => \decode7|display[6]~0_combout\);

-- Location: LCCOMB_X31_Y40_N2
\decode7|display[5]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode7|display[5]~1_combout\ = (\latch13|Q~combout\ & (!\latch15|Q~combout\ & ((\latch12|Q~combout\) # (!\latch14|Q~combout\)))) # (!\latch13|Q~combout\ & (\latch12|Q~combout\ & (\latch14|Q~combout\ $ (!\latch15|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000010001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch13|Q~combout\,
	datab => \latch12|Q~combout\,
	datac => \latch14|Q~combout\,
	datad => \latch15|Q~combout\,
	combout => \decode7|display[5]~1_combout\);

-- Location: LCCOMB_X31_Y40_N4
\decode7|display[4]~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode7|display[4]~2_combout\ = (\latch13|Q~combout\ & (\latch12|Q~combout\ & ((!\latch15|Q~combout\)))) # (!\latch13|Q~combout\ & ((\latch14|Q~combout\ & ((!\latch15|Q~combout\))) # (!\latch14|Q~combout\ & (\latch12|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010011011100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch13|Q~combout\,
	datab => \latch12|Q~combout\,
	datac => \latch14|Q~combout\,
	datad => \latch15|Q~combout\,
	combout => \decode7|display[4]~2_combout\);

-- Location: LCCOMB_X31_Y40_N14
\decode7|display[3]~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode7|display[3]~3_combout\ = (\latch13|Q~combout\ & ((\latch12|Q~combout\ & (\latch14|Q~combout\)) # (!\latch12|Q~combout\ & (!\latch14|Q~combout\ & \latch15|Q~combout\)))) # (!\latch13|Q~combout\ & (!\latch15|Q~combout\ & (\latch12|Q~combout\ $ 
-- (\latch14|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000001010010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch13|Q~combout\,
	datab => \latch12|Q~combout\,
	datac => \latch14|Q~combout\,
	datad => \latch15|Q~combout\,
	combout => \decode7|display[3]~3_combout\);

-- Location: LCCOMB_X31_Y40_N24
\decode7|display[2]~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode7|display[2]~4_combout\ = (\latch14|Q~combout\ & (((!\latch13|Q~combout\ & \latch12|Q~combout\)) # (!\latch15|Q~combout\))) # (!\latch14|Q~combout\ & (((\latch12|Q~combout\) # (\latch15|Q~combout\)) # (!\latch13|Q~combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100111111111101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch13|Q~combout\,
	datab => \latch12|Q~combout\,
	datac => \latch14|Q~combout\,
	datad => \latch15|Q~combout\,
	combout => \decode7|display[2]~4_combout\);

-- Location: LCCOMB_X31_Y40_N26
\decode7|display[1]~5\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode7|display[1]~5_combout\ = (\latch13|Q~combout\ & ((\latch12|Q~combout\ & ((\latch15|Q~combout\))) # (!\latch12|Q~combout\ & (\latch14|Q~combout\)))) # (!\latch13|Q~combout\ & (\latch14|Q~combout\ & (\latch12|Q~combout\ $ (\latch15|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011100001100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch13|Q~combout\,
	datab => \latch12|Q~combout\,
	datac => \latch14|Q~combout\,
	datad => \latch15|Q~combout\,
	combout => \decode7|display[1]~5_combout\);

-- Location: LCCOMB_X31_Y40_N12
\decode7|display[0]~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode7|display[0]~6_combout\ = (\latch14|Q~combout\ & (!\latch13|Q~combout\ & (\latch12|Q~combout\ $ (!\latch15|Q~combout\)))) # (!\latch14|Q~combout\ & (\latch12|Q~combout\ & (\latch13|Q~combout\ $ (!\latch15|Q~combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100000010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \latch13|Q~combout\,
	datab => \latch12|Q~combout\,
	datac => \latch14|Q~combout\,
	datad => \latch15|Q~combout\,
	combout => \decode7|display[0]~6_combout\);

-- Location: IOIBUF_X10_Y0_N8
\KEY[1]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(1),
	o => \KEY[1]~input_o\);

ww_HEX0(6) <= \HEX0[6]~output_o\;

ww_HEX0(5) <= \HEX0[5]~output_o\;

ww_HEX0(4) <= \HEX0[4]~output_o\;

ww_HEX0(3) <= \HEX0[3]~output_o\;

ww_HEX0(2) <= \HEX0[2]~output_o\;

ww_HEX0(1) <= \HEX0[1]~output_o\;

ww_HEX0(0) <= \HEX0[0]~output_o\;

ww_HEX1(6) <= \HEX1[6]~output_o\;

ww_HEX1(5) <= \HEX1[5]~output_o\;

ww_HEX1(4) <= \HEX1[4]~output_o\;

ww_HEX1(3) <= \HEX1[3]~output_o\;

ww_HEX1(2) <= \HEX1[2]~output_o\;

ww_HEX1(1) <= \HEX1[1]~output_o\;

ww_HEX1(0) <= \HEX1[0]~output_o\;

ww_HEX2(6) <= \HEX2[6]~output_o\;

ww_HEX2(5) <= \HEX2[5]~output_o\;

ww_HEX2(4) <= \HEX2[4]~output_o\;

ww_HEX2(3) <= \HEX2[3]~output_o\;

ww_HEX2(2) <= \HEX2[2]~output_o\;

ww_HEX2(1) <= \HEX2[1]~output_o\;

ww_HEX2(0) <= \HEX2[0]~output_o\;

ww_HEX3(6) <= \HEX3[6]~output_o\;

ww_HEX3(5) <= \HEX3[5]~output_o\;

ww_HEX3(4) <= \HEX3[4]~output_o\;

ww_HEX3(3) <= \HEX3[3]~output_o\;

ww_HEX3(2) <= \HEX3[2]~output_o\;

ww_HEX3(1) <= \HEX3[1]~output_o\;

ww_HEX3(0) <= \HEX3[0]~output_o\;

ww_HEX4(6) <= \HEX4[6]~output_o\;

ww_HEX4(5) <= \HEX4[5]~output_o\;

ww_HEX4(4) <= \HEX4[4]~output_o\;

ww_HEX4(3) <= \HEX4[3]~output_o\;

ww_HEX4(2) <= \HEX4[2]~output_o\;

ww_HEX4(1) <= \HEX4[1]~output_o\;

ww_HEX4(0) <= \HEX4[0]~output_o\;

ww_HEX5(6) <= \HEX5[6]~output_o\;

ww_HEX5(5) <= \HEX5[5]~output_o\;

ww_HEX5(4) <= \HEX5[4]~output_o\;

ww_HEX5(3) <= \HEX5[3]~output_o\;

ww_HEX5(2) <= \HEX5[2]~output_o\;

ww_HEX5(1) <= \HEX5[1]~output_o\;

ww_HEX5(0) <= \HEX5[0]~output_o\;

ww_HEX6(6) <= \HEX6[6]~output_o\;

ww_HEX6(5) <= \HEX6[5]~output_o\;

ww_HEX6(4) <= \HEX6[4]~output_o\;

ww_HEX6(3) <= \HEX6[3]~output_o\;

ww_HEX6(2) <= \HEX6[2]~output_o\;

ww_HEX6(1) <= \HEX6[1]~output_o\;

ww_HEX6(0) <= \HEX6[0]~output_o\;

ww_HEX7(6) <= \HEX7[6]~output_o\;

ww_HEX7(5) <= \HEX7[5]~output_o\;

ww_HEX7(4) <= \HEX7[4]~output_o\;

ww_HEX7(3) <= \HEX7[3]~output_o\;

ww_HEX7(2) <= \HEX7[2]~output_o\;

ww_HEX7(1) <= \HEX7[1]~output_o\;

ww_HEX7(0) <= \HEX7[0]~output_o\;
END structure;


