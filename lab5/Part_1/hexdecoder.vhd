library ieee;
use ieee.std_logic_1164.all;

entity hexdecoder is
  port (s : in std_logic_vector(3 downto 0);
  display : out std_logic_vector(0 to 6));
end;

architecture behaviour of hexdecoder is
begin
  display <= "0000001" when s = "0000" else
             "1001111" when s = "0001" else
             "0010010" when s = "0010" else
             "0000110" when s = "0011" else
             "1001100" when s = "0100" else
             "0100100" when s = "0101" else
             "0100000" when s = "0110" else
             "0001111" when s = "0111" else
             "0000000" when s = "1000" else
             "0000100" when s = "1001" else
				 "0001000" when s = "1010" else --A
				 "1100000" when s = "1011" else --b
				 "0110001" when s = "1100" else --c
				 "1000010" when s = "1101" else --d
				 "0110000" when s = "1110" else --E
				 "0111000" when s = "1111" else --F
             "1111111";
end;
