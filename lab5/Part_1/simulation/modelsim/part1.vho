-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Web Edition"

-- DATE "04/23/2015 12:11:46"

-- 
-- Device: Altera EP2C35F672C6 Package FBGA672
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEII;
LIBRARY IEEE;
USE CYCLONEII.CYCLONEII_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	part1 IS
    PORT (
	SW : IN std_logic_vector(1 DOWNTO 0);
	KEY : IN std_logic_vector(1 DOWNTO 0);
	LEDR : OUT std_logic_vector(15 DOWNTO 0);
	HEX0 : OUT std_logic_vector(0 TO 6);
	HEX1 : OUT std_logic_vector(0 TO 6);
	HEX2 : OUT std_logic_vector(0 TO 6);
	HEX3 : OUT std_logic_vector(0 TO 6)
	);
END part1;

-- Design Ports Information
-- KEY[1]	=>  Location: PIN_N23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- LEDR[0]	=>  Location: PIN_AE23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[1]	=>  Location: PIN_AF23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[2]	=>  Location: PIN_AB21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[3]	=>  Location: PIN_AC22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[4]	=>  Location: PIN_AD22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[5]	=>  Location: PIN_AD23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[6]	=>  Location: PIN_AD21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[7]	=>  Location: PIN_AC21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[8]	=>  Location: PIN_AA14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[9]	=>  Location: PIN_Y13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[10]	=>  Location: PIN_AA13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[11]	=>  Location: PIN_AC14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[12]	=>  Location: PIN_AD15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[13]	=>  Location: PIN_AE15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[14]	=>  Location: PIN_AF13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- LEDR[15]	=>  Location: PIN_AE13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[6]	=>  Location: PIN_V13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[5]	=>  Location: PIN_V14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[4]	=>  Location: PIN_AE11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[3]	=>  Location: PIN_AD11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[2]	=>  Location: PIN_AC12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[1]	=>  Location: PIN_AB12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX0[0]	=>  Location: PIN_AF10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[6]	=>  Location: PIN_AB24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[5]	=>  Location: PIN_AA23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[4]	=>  Location: PIN_AA24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[3]	=>  Location: PIN_Y22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[2]	=>  Location: PIN_W21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[1]	=>  Location: PIN_V21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX1[0]	=>  Location: PIN_V20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[6]	=>  Location: PIN_Y24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[5]	=>  Location: PIN_AB25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[4]	=>  Location: PIN_AB26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[3]	=>  Location: PIN_AC26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[2]	=>  Location: PIN_AC25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[1]	=>  Location: PIN_V22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX2[0]	=>  Location: PIN_AB23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[6]	=>  Location: PIN_W24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[5]	=>  Location: PIN_U22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[4]	=>  Location: PIN_Y25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[3]	=>  Location: PIN_Y26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[2]	=>  Location: PIN_AA26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[1]	=>  Location: PIN_AA25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- HEX3[0]	=>  Location: PIN_Y23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- SW[1]	=>  Location: PIN_N26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- KEY[0]	=>  Location: PIN_G26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- SW[0]	=>  Location: PIN_N25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default


ARCHITECTURE structure OF part1 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_SW : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_KEY : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_LEDR : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_HEX0 : std_logic_vector(0 TO 6);
SIGNAL ww_HEX1 : std_logic_vector(0 TO 6);
SIGNAL ww_HEX2 : std_logic_vector(0 TO 6);
SIGNAL ww_HEX3 : std_logic_vector(0 TO 6);
SIGNAL \SW[0]~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \counter1|temp~0_combout\ : std_logic;
SIGNAL \SW[0]~clkctrl_outclk\ : std_logic;
SIGNAL \counter1|temp~regout\ : std_logic;
SIGNAL \counter2|temp~0_combout\ : std_logic;
SIGNAL \counter2|temp~regout\ : std_logic;
SIGNAL \counter3|temp~0_combout\ : std_logic;
SIGNAL \counter3|temp~regout\ : std_logic;
SIGNAL \counter3|temp~1_combout\ : std_logic;
SIGNAL \counter4|temp~0_combout\ : std_logic;
SIGNAL \counter4|temp~regout\ : std_logic;
SIGNAL \counter5|temp~0_combout\ : std_logic;
SIGNAL \counter5|temp~regout\ : std_logic;
SIGNAL \counter6|temp~0_combout\ : std_logic;
SIGNAL \counter6|temp~2_combout\ : std_logic;
SIGNAL \counter6|temp~regout\ : std_logic;
SIGNAL \counter7|temp~0_combout\ : std_logic;
SIGNAL \counter7|temp~regout\ : std_logic;
SIGNAL \counter6|temp~1_combout\ : std_logic;
SIGNAL \counter9|temp~0_combout\ : std_logic;
SIGNAL \counter8|temp~0_combout\ : std_logic;
SIGNAL \counter8|temp~regout\ : std_logic;
SIGNAL \counter9|temp~1_combout\ : std_logic;
SIGNAL \counter9|temp~regout\ : std_logic;
SIGNAL \counter10|temp~0_combout\ : std_logic;
SIGNAL \counter10|temp~1_combout\ : std_logic;
SIGNAL \counter10|temp~2_combout\ : std_logic;
SIGNAL \counter10|temp~regout\ : std_logic;
SIGNAL \counter11|temp~0_combout\ : std_logic;
SIGNAL \counter11|temp~regout\ : std_logic;
SIGNAL \counter12|temp~0_combout\ : std_logic;
SIGNAL \counter12|temp~regout\ : std_logic;
SIGNAL \counter13|temp~0_combout\ : std_logic;
SIGNAL \counter13|temp~1_combout\ : std_logic;
SIGNAL \counter13|temp~regout\ : std_logic;
SIGNAL \counter14|temp~0_combout\ : std_logic;
SIGNAL \counter14|temp~regout\ : std_logic;
SIGNAL \counter15|temp~0_combout\ : std_logic;
SIGNAL \counter15|temp~regout\ : std_logic;
SIGNAL \counter16|temp~0_combout\ : std_logic;
SIGNAL \counter16|temp~1_combout\ : std_logic;
SIGNAL \counter16|temp~regout\ : std_logic;
SIGNAL \decode0|display[6]~0_combout\ : std_logic;
SIGNAL \decode0|display[5]~1_combout\ : std_logic;
SIGNAL \decode0|display[4]~2_combout\ : std_logic;
SIGNAL \decode0|display[3]~3_combout\ : std_logic;
SIGNAL \decode0|display[2]~4_combout\ : std_logic;
SIGNAL \decode0|display[1]~5_combout\ : std_logic;
SIGNAL \decode0|display[0]~6_combout\ : std_logic;
SIGNAL \decode1|display[6]~0_combout\ : std_logic;
SIGNAL \decode1|display[5]~1_combout\ : std_logic;
SIGNAL \decode1|display[4]~2_combout\ : std_logic;
SIGNAL \decode1|display[3]~3_combout\ : std_logic;
SIGNAL \decode1|display[2]~4_combout\ : std_logic;
SIGNAL \decode1|display[1]~5_combout\ : std_logic;
SIGNAL \decode1|display[0]~6_combout\ : std_logic;
SIGNAL \decode2|display[6]~0_combout\ : std_logic;
SIGNAL \decode2|display[5]~1_combout\ : std_logic;
SIGNAL \decode2|display[4]~2_combout\ : std_logic;
SIGNAL \decode2|display[3]~3_combout\ : std_logic;
SIGNAL \decode2|display[2]~4_combout\ : std_logic;
SIGNAL \decode2|display[1]~5_combout\ : std_logic;
SIGNAL \decode2|display[0]~6_combout\ : std_logic;
SIGNAL \decode3|display[6]~0_combout\ : std_logic;
SIGNAL \decode3|display[5]~1_combout\ : std_logic;
SIGNAL \decode3|display[4]~2_combout\ : std_logic;
SIGNAL \decode3|display[3]~3_combout\ : std_logic;
SIGNAL \decode3|display[2]~4_combout\ : std_logic;
SIGNAL \decode3|display[1]~5_combout\ : std_logic;
SIGNAL \decode3|display[0]~6_combout\ : std_logic;
SIGNAL \SW~combout\ : std_logic_vector(1 DOWNTO 0);
SIGNAL \KEY~combout\ : std_logic_vector(1 DOWNTO 0);

BEGIN

ww_SW <= SW;
ww_KEY <= KEY;
LEDR <= ww_LEDR;
HEX0 <= ww_HEX0;
HEX1 <= ww_HEX1;
HEX2 <= ww_HEX2;
HEX3 <= ww_HEX3;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\SW[0]~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \SW~combout\(0));

-- Location: PIN_G26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\KEY[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_KEY(0),
	combout => \KEY~combout\(0));

-- Location: PIN_N26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(1),
	combout => \SW~combout\(1));

-- Location: LCCOMB_X63_Y4_N20
\counter1|temp~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter1|temp~0_combout\ = \counter1|temp~regout\ $ (\SW~combout\(1))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \counter1|temp~regout\,
	datad => \SW~combout\(1),
	combout => \counter1|temp~0_combout\);

-- Location: PIN_N25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\SW[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_SW(0),
	combout => \SW~combout\(0));

-- Location: CLKCTRL_G6
\SW[0]~clkctrl\ : cycloneii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \SW[0]~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \SW[0]~clkctrl_outclk\);

-- Location: LCFF_X63_Y4_N21
\counter1|temp\ : cycloneii_lcell_ff
PORT MAP (
	clk => \KEY~combout\(0),
	datain => \counter1|temp~0_combout\,
	aclr => \SW[0]~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \counter1|temp~regout\);

-- Location: LCCOMB_X63_Y4_N6
\counter2|temp~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter2|temp~0_combout\ = \counter2|temp~regout\ $ (((\counter1|temp~regout\ & \SW~combout\(1))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter1|temp~regout\,
	datac => \counter2|temp~regout\,
	datad => \SW~combout\(1),
	combout => \counter2|temp~0_combout\);

-- Location: LCFF_X63_Y4_N7
\counter2|temp\ : cycloneii_lcell_ff
PORT MAP (
	clk => \KEY~combout\(0),
	datain => \counter2|temp~0_combout\,
	aclr => \SW[0]~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \counter2|temp~regout\);

-- Location: LCCOMB_X63_Y4_N12
\counter3|temp~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter3|temp~0_combout\ = \counter3|temp~regout\ $ (((\counter1|temp~regout\ & (\counter2|temp~regout\ & \SW~combout\(1)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter1|temp~regout\,
	datab => \counter2|temp~regout\,
	datac => \counter3|temp~regout\,
	datad => \SW~combout\(1),
	combout => \counter3|temp~0_combout\);

-- Location: LCFF_X63_Y4_N13
\counter3|temp\ : cycloneii_lcell_ff
PORT MAP (
	clk => \KEY~combout\(0),
	datain => \counter3|temp~0_combout\,
	aclr => \SW[0]~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \counter3|temp~regout\);

-- Location: LCCOMB_X63_Y4_N4
\counter3|temp~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter3|temp~1_combout\ = (\counter2|temp~regout\ & (\counter1|temp~regout\ & \SW~combout\(1)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \counter2|temp~regout\,
	datac => \counter1|temp~regout\,
	datad => \SW~combout\(1),
	combout => \counter3|temp~1_combout\);

-- Location: LCCOMB_X63_Y4_N22
\counter4|temp~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter4|temp~0_combout\ = \counter4|temp~regout\ $ (((\counter3|temp~1_combout\ & \counter3|temp~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \counter3|temp~1_combout\,
	datac => \counter4|temp~regout\,
	datad => \counter3|temp~regout\,
	combout => \counter4|temp~0_combout\);

-- Location: LCFF_X63_Y4_N23
\counter4|temp\ : cycloneii_lcell_ff
PORT MAP (
	clk => \KEY~combout\(0),
	datain => \counter4|temp~0_combout\,
	aclr => \SW[0]~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \counter4|temp~regout\);

-- Location: LCCOMB_X63_Y4_N0
\counter5|temp~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter5|temp~0_combout\ = \counter5|temp~regout\ $ (((\counter3|temp~regout\ & (\counter3|temp~1_combout\ & \counter4|temp~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter3|temp~regout\,
	datab => \counter3|temp~1_combout\,
	datac => \counter5|temp~regout\,
	datad => \counter4|temp~regout\,
	combout => \counter5|temp~0_combout\);

-- Location: LCFF_X63_Y4_N1
\counter5|temp\ : cycloneii_lcell_ff
PORT MAP (
	clk => \KEY~combout\(0),
	datain => \counter5|temp~0_combout\,
	aclr => \SW[0]~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \counter5|temp~regout\);

-- Location: LCCOMB_X63_Y4_N18
\counter6|temp~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter6|temp~0_combout\ = (\counter3|temp~regout\ & (\SW~combout\(1) & (\counter1|temp~regout\ & \counter4|temp~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter3|temp~regout\,
	datab => \SW~combout\(1),
	datac => \counter1|temp~regout\,
	datad => \counter4|temp~regout\,
	combout => \counter6|temp~0_combout\);

-- Location: LCCOMB_X63_Y4_N14
\counter6|temp~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter6|temp~2_combout\ = \counter6|temp~regout\ $ (((\counter6|temp~1_combout\ & \counter6|temp~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter6|temp~1_combout\,
	datac => \counter6|temp~regout\,
	datad => \counter6|temp~0_combout\,
	combout => \counter6|temp~2_combout\);

-- Location: LCFF_X63_Y4_N15
\counter6|temp\ : cycloneii_lcell_ff
PORT MAP (
	clk => \KEY~combout\(0),
	datain => \counter6|temp~2_combout\,
	aclr => \SW[0]~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \counter6|temp~regout\);

-- Location: LCCOMB_X63_Y4_N16
\counter7|temp~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter7|temp~0_combout\ = \counter7|temp~regout\ $ (((\counter6|temp~1_combout\ & (\counter6|temp~regout\ & \counter6|temp~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter6|temp~1_combout\,
	datab => \counter6|temp~regout\,
	datac => \counter7|temp~regout\,
	datad => \counter6|temp~0_combout\,
	combout => \counter7|temp~0_combout\);

-- Location: LCFF_X63_Y4_N17
\counter7|temp\ : cycloneii_lcell_ff
PORT MAP (
	clk => \KEY~combout\(0),
	datain => \counter7|temp~0_combout\,
	aclr => \SW[0]~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \counter7|temp~regout\);

-- Location: LCCOMB_X63_Y4_N28
\counter6|temp~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter6|temp~1_combout\ = (\SW~combout\(1) & (\counter5|temp~regout\ & \counter2|temp~regout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW~combout\(1),
	datab => \counter5|temp~regout\,
	datad => \counter2|temp~regout\,
	combout => \counter6|temp~1_combout\);

-- Location: LCCOMB_X63_Y4_N30
\counter9|temp~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter9|temp~0_combout\ = (\counter6|temp~regout\ & (\counter6|temp~1_combout\ & \counter6|temp~0_combout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter6|temp~regout\,
	datac => \counter6|temp~1_combout\,
	datad => \counter6|temp~0_combout\,
	combout => \counter9|temp~0_combout\);

-- Location: LCCOMB_X63_Y4_N10
\counter8|temp~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter8|temp~0_combout\ = \counter8|temp~regout\ $ (((\counter7|temp~regout\ & \counter9|temp~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter7|temp~regout\,
	datac => \counter8|temp~regout\,
	datad => \counter9|temp~0_combout\,
	combout => \counter8|temp~0_combout\);

-- Location: LCFF_X63_Y4_N11
\counter8|temp\ : cycloneii_lcell_ff
PORT MAP (
	clk => \KEY~combout\(0),
	datain => \counter8|temp~0_combout\,
	aclr => \SW[0]~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \counter8|temp~regout\);

-- Location: LCCOMB_X63_Y4_N8
\counter9|temp~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter9|temp~1_combout\ = \counter9|temp~regout\ $ (((\counter7|temp~regout\ & (\counter8|temp~regout\ & \counter9|temp~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter7|temp~regout\,
	datab => \counter8|temp~regout\,
	datac => \counter9|temp~regout\,
	datad => \counter9|temp~0_combout\,
	combout => \counter9|temp~1_combout\);

-- Location: LCFF_X63_Y4_N9
\counter9|temp\ : cycloneii_lcell_ff
PORT MAP (
	clk => \KEY~combout\(0),
	datain => \counter9|temp~1_combout\,
	aclr => \SW[0]~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \counter9|temp~regout\);

-- Location: LCCOMB_X63_Y4_N24
\counter10|temp~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter10|temp~0_combout\ = (\counter6|temp~regout\ & (\counter9|temp~regout\ & \counter8|temp~regout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \counter6|temp~regout\,
	datac => \counter9|temp~regout\,
	datad => \counter8|temp~regout\,
	combout => \counter10|temp~0_combout\);

-- Location: LCCOMB_X63_Y4_N2
\counter10|temp~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter10|temp~1_combout\ = (\counter7|temp~regout\ & (\counter6|temp~0_combout\ & (\counter10|temp~0_combout\ & \counter6|temp~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter7|temp~regout\,
	datab => \counter6|temp~0_combout\,
	datac => \counter10|temp~0_combout\,
	datad => \counter6|temp~1_combout\,
	combout => \counter10|temp~1_combout\);

-- Location: LCCOMB_X64_Y4_N4
\counter10|temp~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter10|temp~2_combout\ = \counter10|temp~regout\ $ (\counter10|temp~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \counter10|temp~regout\,
	datad => \counter10|temp~1_combout\,
	combout => \counter10|temp~2_combout\);

-- Location: LCFF_X64_Y4_N5
\counter10|temp\ : cycloneii_lcell_ff
PORT MAP (
	clk => \KEY~combout\(0),
	datain => \counter10|temp~2_combout\,
	aclr => \SW[0]~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \counter10|temp~regout\);

-- Location: LCCOMB_X64_Y4_N10
\counter11|temp~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter11|temp~0_combout\ = \counter11|temp~regout\ $ (((\counter10|temp~regout\ & \counter10|temp~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \counter10|temp~regout\,
	datac => \counter11|temp~regout\,
	datad => \counter10|temp~1_combout\,
	combout => \counter11|temp~0_combout\);

-- Location: LCFF_X64_Y4_N11
\counter11|temp\ : cycloneii_lcell_ff
PORT MAP (
	clk => \KEY~combout\(0),
	datain => \counter11|temp~0_combout\,
	aclr => \SW[0]~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \counter11|temp~regout\);

-- Location: LCCOMB_X64_Y4_N24
\counter12|temp~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter12|temp~0_combout\ = \counter12|temp~regout\ $ (((\counter11|temp~regout\ & (\counter10|temp~regout\ & \counter10|temp~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter11|temp~regout\,
	datab => \counter10|temp~regout\,
	datac => \counter12|temp~regout\,
	datad => \counter10|temp~1_combout\,
	combout => \counter12|temp~0_combout\);

-- Location: LCFF_X64_Y4_N25
\counter12|temp\ : cycloneii_lcell_ff
PORT MAP (
	clk => \KEY~combout\(0),
	datain => \counter12|temp~0_combout\,
	aclr => \SW[0]~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \counter12|temp~regout\);

-- Location: LCCOMB_X64_Y4_N12
\counter13|temp~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter13|temp~0_combout\ = (\counter11|temp~regout\ & (\counter10|temp~regout\ & (\counter12|temp~regout\ & \counter10|temp~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter11|temp~regout\,
	datab => \counter10|temp~regout\,
	datac => \counter12|temp~regout\,
	datad => \counter10|temp~1_combout\,
	combout => \counter13|temp~0_combout\);

-- Location: LCCOMB_X64_Y4_N30
\counter13|temp~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter13|temp~1_combout\ = \counter13|temp~regout\ $ (\counter13|temp~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \counter13|temp~regout\,
	datad => \counter13|temp~0_combout\,
	combout => \counter13|temp~1_combout\);

-- Location: LCFF_X64_Y4_N31
\counter13|temp\ : cycloneii_lcell_ff
PORT MAP (
	clk => \KEY~combout\(0),
	datain => \counter13|temp~1_combout\,
	aclr => \SW[0]~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \counter13|temp~regout\);

-- Location: LCCOMB_X64_Y4_N16
\counter14|temp~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter14|temp~0_combout\ = \counter14|temp~regout\ $ (((\counter13|temp~regout\ & \counter13|temp~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \counter13|temp~regout\,
	datac => \counter14|temp~regout\,
	datad => \counter13|temp~0_combout\,
	combout => \counter14|temp~0_combout\);

-- Location: LCFF_X64_Y4_N17
\counter14|temp\ : cycloneii_lcell_ff
PORT MAP (
	clk => \KEY~combout\(0),
	datain => \counter14|temp~0_combout\,
	aclr => \SW[0]~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \counter14|temp~regout\);

-- Location: LCCOMB_X64_Y4_N14
\counter15|temp~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter15|temp~0_combout\ = \counter15|temp~regout\ $ (((\counter14|temp~regout\ & (\counter13|temp~regout\ & \counter13|temp~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter14|temp~regout\,
	datab => \counter13|temp~regout\,
	datac => \counter15|temp~regout\,
	datad => \counter13|temp~0_combout\,
	combout => \counter15|temp~0_combout\);

-- Location: LCFF_X64_Y4_N15
\counter15|temp\ : cycloneii_lcell_ff
PORT MAP (
	clk => \KEY~combout\(0),
	datain => \counter15|temp~0_combout\,
	aclr => \SW[0]~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \counter15|temp~regout\);

-- Location: LCCOMB_X64_Y4_N18
\counter16|temp~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter16|temp~0_combout\ = (\counter14|temp~regout\ & (\counter13|temp~regout\ & (\counter15|temp~regout\ & \counter13|temp~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter14|temp~regout\,
	datab => \counter13|temp~regout\,
	datac => \counter15|temp~regout\,
	datad => \counter13|temp~0_combout\,
	combout => \counter16|temp~0_combout\);

-- Location: LCCOMB_X64_Y4_N28
\counter16|temp~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \counter16|temp~1_combout\ = \counter16|temp~regout\ $ (\counter16|temp~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \counter16|temp~regout\,
	datad => \counter16|temp~0_combout\,
	combout => \counter16|temp~1_combout\);

-- Location: LCFF_X64_Y4_N29
\counter16|temp\ : cycloneii_lcell_ff
PORT MAP (
	clk => \KEY~combout\(0),
	datain => \counter16|temp~1_combout\,
	aclr => \SW[0]~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \counter16|temp~regout\);

-- Location: LCCOMB_X28_Y1_N16
\decode0|display[6]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode0|display[6]~0_combout\ = (\counter1|temp~regout\ & (!\counter4|temp~regout\ & (\counter3|temp~regout\ $ (!\counter2|temp~regout\)))) # (!\counter1|temp~regout\ & (!\counter2|temp~regout\ & (\counter4|temp~regout\ $ (!\counter3|temp~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000000011001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter4|temp~regout\,
	datab => \counter3|temp~regout\,
	datac => \counter1|temp~regout\,
	datad => \counter2|temp~regout\,
	combout => \decode0|display[6]~0_combout\);

-- Location: LCCOMB_X28_Y1_N2
\decode0|display[5]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode0|display[5]~1_combout\ = (\counter3|temp~regout\ & (\counter1|temp~regout\ & (\counter4|temp~regout\ $ (\counter2|temp~regout\)))) # (!\counter3|temp~regout\ & (!\counter4|temp~regout\ & ((\counter1|temp~regout\) # (\counter2|temp~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000110010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter4|temp~regout\,
	datab => \counter3|temp~regout\,
	datac => \counter1|temp~regout\,
	datad => \counter2|temp~regout\,
	combout => \decode0|display[5]~1_combout\);

-- Location: LCCOMB_X28_Y1_N20
\decode0|display[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode0|display[4]~2_combout\ = (\counter2|temp~regout\ & (!\counter4|temp~regout\ & ((\counter1|temp~regout\)))) # (!\counter2|temp~regout\ & ((\counter3|temp~regout\ & (!\counter4|temp~regout\)) # (!\counter3|temp~regout\ & 
-- ((\counter1|temp~regout\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000001110100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter4|temp~regout\,
	datab => \counter3|temp~regout\,
	datac => \counter1|temp~regout\,
	datad => \counter2|temp~regout\,
	combout => \decode0|display[4]~2_combout\);

-- Location: LCCOMB_X28_Y1_N26
\decode0|display[3]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode0|display[3]~3_combout\ = (\counter2|temp~regout\ & ((\counter3|temp~regout\ & ((\counter1|temp~regout\))) # (!\counter3|temp~regout\ & (\counter4|temp~regout\ & !\counter1|temp~regout\)))) # (!\counter2|temp~regout\ & (!\counter4|temp~regout\ & 
-- (\counter3|temp~regout\ $ (\counter1|temp~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001000010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter4|temp~regout\,
	datab => \counter3|temp~regout\,
	datac => \counter1|temp~regout\,
	datad => \counter2|temp~regout\,
	combout => \decode0|display[3]~3_combout\);

-- Location: LCCOMB_X28_Y1_N12
\decode0|display[2]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode0|display[2]~4_combout\ = (\counter4|temp~regout\ & (\counter3|temp~regout\ & ((\counter2|temp~regout\) # (!\counter1|temp~regout\)))) # (!\counter4|temp~regout\ & (!\counter3|temp~regout\ & (!\counter1|temp~regout\ & \counter2|temp~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100100001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter4|temp~regout\,
	datab => \counter3|temp~regout\,
	datac => \counter1|temp~regout\,
	datad => \counter2|temp~regout\,
	combout => \decode0|display[2]~4_combout\);

-- Location: LCCOMB_X28_Y1_N18
\decode0|display[1]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode0|display[1]~5_combout\ = (\counter4|temp~regout\ & ((\counter1|temp~regout\ & ((\counter2|temp~regout\))) # (!\counter1|temp~regout\ & (\counter3|temp~regout\)))) # (!\counter4|temp~regout\ & (\counter3|temp~regout\ & (\counter1|temp~regout\ $ 
-- (\counter2|temp~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010110001001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter4|temp~regout\,
	datab => \counter3|temp~regout\,
	datac => \counter1|temp~regout\,
	datad => \counter2|temp~regout\,
	combout => \decode0|display[1]~5_combout\);

-- Location: LCCOMB_X28_Y1_N24
\decode0|display[0]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode0|display[0]~6_combout\ = (\counter4|temp~regout\ & (\counter1|temp~regout\ & (\counter3|temp~regout\ $ (\counter2|temp~regout\)))) # (!\counter4|temp~regout\ & (!\counter2|temp~regout\ & (\counter3|temp~regout\ $ (\counter1|temp~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000010010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter4|temp~regout\,
	datab => \counter3|temp~regout\,
	datac => \counter1|temp~regout\,
	datad => \counter2|temp~regout\,
	combout => \decode0|display[0]~6_combout\);

-- Location: LCCOMB_X64_Y3_N0
\decode1|display[6]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode1|display[6]~0_combout\ = (\counter5|temp~regout\ & (!\counter8|temp~regout\ & (\counter7|temp~regout\ $ (!\counter6|temp~regout\)))) # (!\counter5|temp~regout\ & (!\counter6|temp~regout\ & (\counter7|temp~regout\ $ (!\counter8|temp~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100001000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter5|temp~regout\,
	datab => \counter7|temp~regout\,
	datac => \counter8|temp~regout\,
	datad => \counter6|temp~regout\,
	combout => \decode1|display[6]~0_combout\);

-- Location: LCCOMB_X64_Y3_N2
\decode1|display[5]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode1|display[5]~1_combout\ = (\counter5|temp~regout\ & (\counter8|temp~regout\ $ (((\counter6|temp~regout\) # (!\counter7|temp~regout\))))) # (!\counter5|temp~regout\ & (!\counter7|temp~regout\ & (!\counter8|temp~regout\ & \counter6|temp~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101110000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter5|temp~regout\,
	datab => \counter7|temp~regout\,
	datac => \counter8|temp~regout\,
	datad => \counter6|temp~regout\,
	combout => \decode1|display[5]~1_combout\);

-- Location: LCCOMB_X64_Y3_N28
\decode1|display[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode1|display[4]~2_combout\ = (\counter6|temp~regout\ & (\counter5|temp~regout\ & ((!\counter8|temp~regout\)))) # (!\counter6|temp~regout\ & ((\counter7|temp~regout\ & ((!\counter8|temp~regout\))) # (!\counter7|temp~regout\ & 
-- (\counter5|temp~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101000101110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter5|temp~regout\,
	datab => \counter7|temp~regout\,
	datac => \counter8|temp~regout\,
	datad => \counter6|temp~regout\,
	combout => \decode1|display[4]~2_combout\);

-- Location: LCCOMB_X64_Y3_N22
\decode1|display[3]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode1|display[3]~3_combout\ = (\counter6|temp~regout\ & ((\counter5|temp~regout\ & (\counter7|temp~regout\)) # (!\counter5|temp~regout\ & (!\counter7|temp~regout\ & \counter8|temp~regout\)))) # (!\counter6|temp~regout\ & (!\counter8|temp~regout\ & 
-- (\counter5|temp~regout\ $ (\counter7|temp~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001100000000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter5|temp~regout\,
	datab => \counter7|temp~regout\,
	datac => \counter8|temp~regout\,
	datad => \counter6|temp~regout\,
	combout => \decode1|display[3]~3_combout\);

-- Location: LCCOMB_X63_Y4_N26
\decode1|display[2]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode1|display[2]~4_combout\ = (\counter8|temp~regout\ & (\counter7|temp~regout\ & ((\counter6|temp~regout\) # (!\counter5|temp~regout\)))) # (!\counter8|temp~regout\ & (!\counter5|temp~regout\ & (!\counter7|temp~regout\ & \counter6|temp~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000101000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter5|temp~regout\,
	datab => \counter8|temp~regout\,
	datac => \counter7|temp~regout\,
	datad => \counter6|temp~regout\,
	combout => \decode1|display[2]~4_combout\);

-- Location: LCCOMB_X64_Y3_N12
\decode1|display[1]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode1|display[1]~5_combout\ = (\counter8|temp~regout\ & ((\counter5|temp~regout\ & ((\counter6|temp~regout\))) # (!\counter5|temp~regout\ & (\counter7|temp~regout\)))) # (!\counter8|temp~regout\ & (\counter7|temp~regout\ & (\counter5|temp~regout\ $ 
-- (\counter6|temp~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110010001001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter5|temp~regout\,
	datab => \counter7|temp~regout\,
	datac => \counter8|temp~regout\,
	datad => \counter6|temp~regout\,
	combout => \decode1|display[1]~5_combout\);

-- Location: LCCOMB_X64_Y3_N14
\decode1|display[0]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode1|display[0]~6_combout\ = (\counter7|temp~regout\ & (!\counter6|temp~regout\ & (\counter5|temp~regout\ $ (!\counter8|temp~regout\)))) # (!\counter7|temp~regout\ & (\counter5|temp~regout\ & (\counter8|temp~regout\ $ (!\counter6|temp~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000010000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter5|temp~regout\,
	datab => \counter7|temp~regout\,
	datac => \counter8|temp~regout\,
	datad => \counter6|temp~regout\,
	combout => \decode1|display[0]~6_combout\);

-- Location: LCCOMB_X64_Y4_N26
\decode2|display[6]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode2|display[6]~0_combout\ = (\counter9|temp~regout\ & (!\counter12|temp~regout\ & (\counter10|temp~regout\ $ (!\counter11|temp~regout\)))) # (!\counter9|temp~regout\ & (!\counter10|temp~regout\ & (\counter12|temp~regout\ $ 
-- (!\counter11|temp~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001100000000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter9|temp~regout\,
	datab => \counter10|temp~regout\,
	datac => \counter12|temp~regout\,
	datad => \counter11|temp~regout\,
	combout => \decode2|display[6]~0_combout\);

-- Location: LCCOMB_X64_Y4_N20
\decode2|display[5]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode2|display[5]~1_combout\ = (\counter9|temp~regout\ & (\counter12|temp~regout\ $ (((\counter10|temp~regout\) # (!\counter11|temp~regout\))))) # (!\counter9|temp~regout\ & (\counter10|temp~regout\ & (!\counter12|temp~regout\ & 
-- !\counter11|temp~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100000001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter9|temp~regout\,
	datab => \counter10|temp~regout\,
	datac => \counter12|temp~regout\,
	datad => \counter11|temp~regout\,
	combout => \decode2|display[5]~1_combout\);

-- Location: LCCOMB_X64_Y4_N22
\decode2|display[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode2|display[4]~2_combout\ = (\counter10|temp~regout\ & (\counter9|temp~regout\ & (!\counter12|temp~regout\))) # (!\counter10|temp~regout\ & ((\counter11|temp~regout\ & ((!\counter12|temp~regout\))) # (!\counter11|temp~regout\ & 
-- (\counter9|temp~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000101100101010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter9|temp~regout\,
	datab => \counter10|temp~regout\,
	datac => \counter12|temp~regout\,
	datad => \counter11|temp~regout\,
	combout => \decode2|display[4]~2_combout\);

-- Location: LCCOMB_X64_Y4_N0
\decode2|display[3]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode2|display[3]~3_combout\ = (\counter10|temp~regout\ & ((\counter9|temp~regout\ & ((\counter11|temp~regout\))) # (!\counter9|temp~regout\ & (\counter12|temp~regout\ & !\counter11|temp~regout\)))) # (!\counter10|temp~regout\ & 
-- (!\counter12|temp~regout\ & (\counter9|temp~regout\ $ (\counter11|temp~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100101000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter9|temp~regout\,
	datab => \counter10|temp~regout\,
	datac => \counter12|temp~regout\,
	datad => \counter11|temp~regout\,
	combout => \decode2|display[3]~3_combout\);

-- Location: LCCOMB_X64_Y4_N2
\decode2|display[2]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode2|display[2]~4_combout\ = (\counter12|temp~regout\ & (\counter11|temp~regout\ & ((\counter10|temp~regout\) # (!\counter9|temp~regout\)))) # (!\counter12|temp~regout\ & (!\counter9|temp~regout\ & (\counter10|temp~regout\ & 
-- !\counter11|temp~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter9|temp~regout\,
	datab => \counter10|temp~regout\,
	datac => \counter12|temp~regout\,
	datad => \counter11|temp~regout\,
	combout => \decode2|display[2]~4_combout\);

-- Location: LCCOMB_X64_Y4_N8
\decode2|display[1]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode2|display[1]~5_combout\ = (\counter10|temp~regout\ & ((\counter9|temp~regout\ & (\counter12|temp~regout\)) # (!\counter9|temp~regout\ & ((\counter11|temp~regout\))))) # (!\counter10|temp~regout\ & (\counter11|temp~regout\ & (\counter9|temp~regout\ 
-- $ (\counter12|temp~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101011010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter9|temp~regout\,
	datab => \counter10|temp~regout\,
	datac => \counter12|temp~regout\,
	datad => \counter11|temp~regout\,
	combout => \decode2|display[1]~5_combout\);

-- Location: LCCOMB_X64_Y4_N6
\decode2|display[0]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode2|display[0]~6_combout\ = (\counter12|temp~regout\ & (\counter9|temp~regout\ & (\counter10|temp~regout\ $ (\counter11|temp~regout\)))) # (!\counter12|temp~regout\ & (!\counter10|temp~regout\ & (\counter9|temp~regout\ $ (\counter11|temp~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000110000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter9|temp~regout\,
	datab => \counter10|temp~regout\,
	datac => \counter12|temp~regout\,
	datad => \counter11|temp~regout\,
	combout => \decode2|display[0]~6_combout\);

-- Location: LCCOMB_X64_Y8_N0
\decode3|display[6]~0\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode3|display[6]~0_combout\ = (\counter13|temp~regout\ & (!\counter16|temp~regout\ & (\counter14|temp~regout\ $ (!\counter15|temp~regout\)))) # (!\counter13|temp~regout\ & (!\counter14|temp~regout\ & (\counter15|temp~regout\ $ 
-- (!\counter16|temp~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001000010000011",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter13|temp~regout\,
	datab => \counter14|temp~regout\,
	datac => \counter15|temp~regout\,
	datad => \counter16|temp~regout\,
	combout => \decode3|display[6]~0_combout\);

-- Location: LCCOMB_X64_Y8_N22
\decode3|display[5]~1\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode3|display[5]~1_combout\ = (\counter13|temp~regout\ & (\counter16|temp~regout\ $ (((\counter14|temp~regout\) # (!\counter15|temp~regout\))))) # (!\counter13|temp~regout\ & (\counter14|temp~regout\ & (!\counter15|temp~regout\ & 
-- !\counter16|temp~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000010001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter13|temp~regout\,
	datab => \counter14|temp~regout\,
	datac => \counter15|temp~regout\,
	datad => \counter16|temp~regout\,
	combout => \decode3|display[5]~1_combout\);

-- Location: LCCOMB_X64_Y8_N12
\decode3|display[4]~2\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode3|display[4]~2_combout\ = (\counter14|temp~regout\ & (\counter13|temp~regout\ & ((!\counter16|temp~regout\)))) # (!\counter14|temp~regout\ & ((\counter15|temp~regout\ & ((!\counter16|temp~regout\))) # (!\counter15|temp~regout\ & 
-- (\counter13|temp~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001010111010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter13|temp~regout\,
	datab => \counter14|temp~regout\,
	datac => \counter15|temp~regout\,
	datad => \counter16|temp~regout\,
	combout => \decode3|display[4]~2_combout\);

-- Location: LCCOMB_X64_Y8_N30
\decode3|display[3]~3\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode3|display[3]~3_combout\ = (\counter14|temp~regout\ & ((\counter13|temp~regout\ & (\counter15|temp~regout\)) # (!\counter13|temp~regout\ & (!\counter15|temp~regout\ & \counter16|temp~regout\)))) # (!\counter14|temp~regout\ & 
-- (!\counter16|temp~regout\ & (\counter13|temp~regout\ $ (\counter15|temp~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000010010010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter13|temp~regout\,
	datab => \counter14|temp~regout\,
	datac => \counter15|temp~regout\,
	datad => \counter16|temp~regout\,
	combout => \decode3|display[3]~3_combout\);

-- Location: LCCOMB_X64_Y8_N16
\decode3|display[2]~4\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode3|display[2]~4_combout\ = (\counter15|temp~regout\ & (\counter16|temp~regout\ & ((\counter14|temp~regout\) # (!\counter13|temp~regout\)))) # (!\counter15|temp~regout\ & (!\counter13|temp~regout\ & (\counter14|temp~regout\ & 
-- !\counter16|temp~regout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101000000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter13|temp~regout\,
	datab => \counter14|temp~regout\,
	datac => \counter15|temp~regout\,
	datad => \counter16|temp~regout\,
	combout => \decode3|display[2]~4_combout\);

-- Location: LCCOMB_X64_Y8_N14
\decode3|display[1]~5\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode3|display[1]~5_combout\ = (\counter14|temp~regout\ & ((\counter13|temp~regout\ & ((\counter16|temp~regout\))) # (!\counter13|temp~regout\ & (\counter15|temp~regout\)))) # (!\counter14|temp~regout\ & (\counter15|temp~regout\ & 
-- (\counter13|temp~regout\ $ (\counter16|temp~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1101100001100000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter13|temp~regout\,
	datab => \counter14|temp~regout\,
	datac => \counter15|temp~regout\,
	datad => \counter16|temp~regout\,
	combout => \decode3|display[1]~5_combout\);

-- Location: LCCOMB_X64_Y8_N24
\decode3|display[0]~6\ : cycloneii_lcell_comb
-- Equation(s):
-- \decode3|display[0]~6_combout\ = (\counter15|temp~regout\ & (!\counter14|temp~regout\ & (\counter13|temp~regout\ $ (!\counter16|temp~regout\)))) # (!\counter15|temp~regout\ & (\counter13|temp~regout\ & (\counter14|temp~regout\ $ 
-- (!\counter16|temp~regout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010100000010010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter13|temp~regout\,
	datab => \counter14|temp~regout\,
	datac => \counter15|temp~regout\,
	datad => \counter16|temp~regout\,
	combout => \decode3|display[0]~6_combout\);

-- Location: PIN_N23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\KEY[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_KEY(1));

-- Location: PIN_AE23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \counter1|temp~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(0));

-- Location: PIN_AF23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \counter2|temp~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(1));

-- Location: PIN_AB21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \counter3|temp~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(2));

-- Location: PIN_AC22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \counter4|temp~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(3));

-- Location: PIN_AD22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \counter5|temp~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(4));

-- Location: PIN_AD23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \counter6|temp~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(5));

-- Location: PIN_AD21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \counter7|temp~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(6));

-- Location: PIN_AC21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[7]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \counter8|temp~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(7));

-- Location: PIN_AA14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[8]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \counter9|temp~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(8));

-- Location: PIN_Y13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[9]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \counter10|temp~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(9));

-- Location: PIN_AA13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[10]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \counter11|temp~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(10));

-- Location: PIN_AC14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[11]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \counter12|temp~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(11));

-- Location: PIN_AD15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[12]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \counter13|temp~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(12));

-- Location: PIN_AE15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[13]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \counter14|temp~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(13));

-- Location: PIN_AF13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[14]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \counter15|temp~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(14));

-- Location: PIN_AE13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\LEDR[15]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \counter16|temp~regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_LEDR(15));

-- Location: PIN_V13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode0|display[6]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(6));

-- Location: PIN_V14,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode0|display[5]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(5));

-- Location: PIN_AE11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode0|display[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(4));

-- Location: PIN_AD11,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode0|display[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(3));

-- Location: PIN_AC12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode0|display[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(2));

-- Location: PIN_AB12,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode0|display[1]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(1));

-- Location: PIN_AF10,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX0[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode0|display[0]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX0(0));

-- Location: PIN_AB24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode1|display[6]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(6));

-- Location: PIN_AA23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode1|display[5]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(5));

-- Location: PIN_AA24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode1|display[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(4));

-- Location: PIN_Y22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode1|display[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(3));

-- Location: PIN_W21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode1|display[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(2));

-- Location: PIN_V21,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode1|display[1]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(1));

-- Location: PIN_V20,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX1[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode1|display[0]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX1(0));

-- Location: PIN_Y24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode2|display[6]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(6));

-- Location: PIN_AB25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode2|display[5]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(5));

-- Location: PIN_AB26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode2|display[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(4));

-- Location: PIN_AC26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode2|display[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(3));

-- Location: PIN_AC25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode2|display[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(2));

-- Location: PIN_V22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode2|display[1]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(1));

-- Location: PIN_AB23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX2[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode2|display[0]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX2(0));

-- Location: PIN_W24,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode3|display[6]~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(6));

-- Location: PIN_U22,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode3|display[5]~1_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(5));

-- Location: PIN_Y25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode3|display[4]~2_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(4));

-- Location: PIN_Y26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode3|display[3]~3_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(3));

-- Location: PIN_AA26,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode3|display[2]~4_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(2));

-- Location: PIN_AA25,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode3|display[1]~5_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(1));

-- Location: PIN_Y23,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\HEX3[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \decode3|display[0]~6_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_HEX3(0));
END structure;


