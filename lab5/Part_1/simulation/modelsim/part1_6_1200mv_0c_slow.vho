-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "04/21/2015 23:11:07"

-- 
-- Device: Altera EP4CGX22CF19C6 Package FBGA324
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIV.CYCLONEIV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	part1 IS
    PORT (
	SW : IN std_logic_vector(1 DOWNTO 0);
	KEY : IN std_logic_vector(1 DOWNTO 0);
	LEDR : OUT std_logic_vector(15 DOWNTO 0);
	HEX0 : OUT std_logic_vector(0 TO 6);
	HEX1 : OUT std_logic_vector(0 TO 6);
	HEX2 : OUT std_logic_vector(0 TO 6);
	HEX3 : OUT std_logic_vector(0 TO 6)
	);
END part1;

-- Design Ports Information
-- KEY[1]	=>  Location: PIN_U10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[0]	=>  Location: PIN_C17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[1]	=>  Location: PIN_A16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[2]	=>  Location: PIN_C16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[3]	=>  Location: PIN_F16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[4]	=>  Location: PIN_F17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[5]	=>  Location: PIN_F18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[6]	=>  Location: PIN_G18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[7]	=>  Location: PIN_N17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[8]	=>  Location: PIN_D17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[9]	=>  Location: PIN_E15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[10]	=>  Location: PIN_E16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[11]	=>  Location: PIN_F15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[12]	=>  Location: PIN_B16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[13]	=>  Location: PIN_A14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[14]	=>  Location: PIN_D16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- LEDR[15]	=>  Location: PIN_A15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[6]	=>  Location: PIN_A17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[5]	=>  Location: PIN_A18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[4]	=>  Location: PIN_D11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[3]	=>  Location: PIN_C18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[2]	=>  Location: PIN_B13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[1]	=>  Location: PIN_C15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX0[0]	=>  Location: PIN_B15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[6]	=>  Location: PIN_L18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[5]	=>  Location: PIN_N18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[4]	=>  Location: PIN_M18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[3]	=>  Location: PIN_K15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[2]	=>  Location: PIN_J16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[1]	=>  Location: PIN_E18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX1[0]	=>  Location: PIN_K16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[6]	=>  Location: PIN_J17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[5]	=>  Location: PIN_H16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[4]	=>  Location: PIN_G15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[3]	=>  Location: PIN_G17,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[2]	=>  Location: PIN_M16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[1]	=>  Location: PIN_D18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX2[0]	=>  Location: PIN_G16,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[6]	=>  Location: PIN_C13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[5]	=>  Location: PIN_C14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[4]	=>  Location: PIN_C12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[3]	=>  Location: PIN_D13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[2]	=>  Location: PIN_D14,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[1]	=>  Location: PIN_E12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- HEX3[0]	=>  Location: PIN_D15,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[1]	=>  Location: PIN_B18,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- KEY[0]	=>  Location: PIN_M10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- SW[0]	=>  Location: PIN_M9,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF part1 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_SW : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_KEY : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_LEDR : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_HEX0 : std_logic_vector(0 TO 6);
SIGNAL ww_HEX1 : std_logic_vector(0 TO 6);
SIGNAL ww_HEX2 : std_logic_vector(0 TO 6);
SIGNAL ww_HEX3 : std_logic_vector(0 TO 6);
SIGNAL \SW[0]~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \KEY[0]~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \KEY[1]~input_o\ : std_logic;
SIGNAL \LEDR[0]~output_o\ : std_logic;
SIGNAL \LEDR[1]~output_o\ : std_logic;
SIGNAL \LEDR[2]~output_o\ : std_logic;
SIGNAL \LEDR[3]~output_o\ : std_logic;
SIGNAL \LEDR[4]~output_o\ : std_logic;
SIGNAL \LEDR[5]~output_o\ : std_logic;
SIGNAL \LEDR[6]~output_o\ : std_logic;
SIGNAL \LEDR[7]~output_o\ : std_logic;
SIGNAL \LEDR[8]~output_o\ : std_logic;
SIGNAL \LEDR[9]~output_o\ : std_logic;
SIGNAL \LEDR[10]~output_o\ : std_logic;
SIGNAL \LEDR[11]~output_o\ : std_logic;
SIGNAL \LEDR[12]~output_o\ : std_logic;
SIGNAL \LEDR[13]~output_o\ : std_logic;
SIGNAL \LEDR[14]~output_o\ : std_logic;
SIGNAL \LEDR[15]~output_o\ : std_logic;
SIGNAL \HEX0[6]~output_o\ : std_logic;
SIGNAL \HEX0[5]~output_o\ : std_logic;
SIGNAL \HEX0[4]~output_o\ : std_logic;
SIGNAL \HEX0[3]~output_o\ : std_logic;
SIGNAL \HEX0[2]~output_o\ : std_logic;
SIGNAL \HEX0[1]~output_o\ : std_logic;
SIGNAL \HEX0[0]~output_o\ : std_logic;
SIGNAL \HEX1[6]~output_o\ : std_logic;
SIGNAL \HEX1[5]~output_o\ : std_logic;
SIGNAL \HEX1[4]~output_o\ : std_logic;
SIGNAL \HEX1[3]~output_o\ : std_logic;
SIGNAL \HEX1[2]~output_o\ : std_logic;
SIGNAL \HEX1[1]~output_o\ : std_logic;
SIGNAL \HEX1[0]~output_o\ : std_logic;
SIGNAL \HEX2[6]~output_o\ : std_logic;
SIGNAL \HEX2[5]~output_o\ : std_logic;
SIGNAL \HEX2[4]~output_o\ : std_logic;
SIGNAL \HEX2[3]~output_o\ : std_logic;
SIGNAL \HEX2[2]~output_o\ : std_logic;
SIGNAL \HEX2[1]~output_o\ : std_logic;
SIGNAL \HEX2[0]~output_o\ : std_logic;
SIGNAL \HEX3[6]~output_o\ : std_logic;
SIGNAL \HEX3[5]~output_o\ : std_logic;
SIGNAL \HEX3[4]~output_o\ : std_logic;
SIGNAL \HEX3[3]~output_o\ : std_logic;
SIGNAL \HEX3[2]~output_o\ : std_logic;
SIGNAL \HEX3[1]~output_o\ : std_logic;
SIGNAL \HEX3[0]~output_o\ : std_logic;
SIGNAL \KEY[0]~input_o\ : std_logic;
SIGNAL \KEY[0]~inputclkctrl_outclk\ : std_logic;
SIGNAL \SW[1]~input_o\ : std_logic;
SIGNAL \counter1|temp~0_combout\ : std_logic;
SIGNAL \SW[0]~input_o\ : std_logic;
SIGNAL \SW[0]~inputclkctrl_outclk\ : std_logic;
SIGNAL \counter1|temp~q\ : std_logic;
SIGNAL \counter2|temp~0_combout\ : std_logic;
SIGNAL \counter2|temp~q\ : std_logic;
SIGNAL \counter3|temp~0_combout\ : std_logic;
SIGNAL \counter3|temp~q\ : std_logic;
SIGNAL \counter3|temp~1_combout\ : std_logic;
SIGNAL \counter4|temp~0_combout\ : std_logic;
SIGNAL \counter4|temp~q\ : std_logic;
SIGNAL \counter5|temp~0_combout\ : std_logic;
SIGNAL \counter5|temp~q\ : std_logic;
SIGNAL \counter6|temp~1_combout\ : std_logic;
SIGNAL \counter6|temp~0_combout\ : std_logic;
SIGNAL \counter6|temp~2_combout\ : std_logic;
SIGNAL \counter6|temp~q\ : std_logic;
SIGNAL \counter7|temp~0_combout\ : std_logic;
SIGNAL \counter7|temp~q\ : std_logic;
SIGNAL \counter9|temp~0_combout\ : std_logic;
SIGNAL \counter8|temp~0_combout\ : std_logic;
SIGNAL \counter8|temp~q\ : std_logic;
SIGNAL \counter9|temp~1_combout\ : std_logic;
SIGNAL \counter9|temp~q\ : std_logic;
SIGNAL \counter10|temp~0_combout\ : std_logic;
SIGNAL \counter10|temp~1_combout\ : std_logic;
SIGNAL \counter10|temp~2_combout\ : std_logic;
SIGNAL \counter10|temp~q\ : std_logic;
SIGNAL \counter11|temp~0_combout\ : std_logic;
SIGNAL \counter11|temp~q\ : std_logic;
SIGNAL \counter12|temp~0_combout\ : std_logic;
SIGNAL \counter12|temp~q\ : std_logic;
SIGNAL \counter13|temp~0_combout\ : std_logic;
SIGNAL \counter13|temp~1_combout\ : std_logic;
SIGNAL \counter13|temp~q\ : std_logic;
SIGNAL \counter14|temp~0_combout\ : std_logic;
SIGNAL \counter14|temp~q\ : std_logic;
SIGNAL \counter15|temp~0_combout\ : std_logic;
SIGNAL \counter15|temp~q\ : std_logic;
SIGNAL \counter16|temp~0_combout\ : std_logic;
SIGNAL \counter16|temp~1_combout\ : std_logic;
SIGNAL \counter16|temp~q\ : std_logic;
SIGNAL \decode0|display[6]~0_combout\ : std_logic;
SIGNAL \decode0|display[5]~1_combout\ : std_logic;
SIGNAL \decode0|display[4]~2_combout\ : std_logic;
SIGNAL \decode0|display[3]~3_combout\ : std_logic;
SIGNAL \decode0|display[2]~4_combout\ : std_logic;
SIGNAL \decode0|display[1]~5_combout\ : std_logic;
SIGNAL \decode0|display[0]~6_combout\ : std_logic;
SIGNAL \decode1|display[6]~0_combout\ : std_logic;
SIGNAL \decode1|display[5]~1_combout\ : std_logic;
SIGNAL \decode1|display[4]~2_combout\ : std_logic;
SIGNAL \decode1|display[3]~3_combout\ : std_logic;
SIGNAL \decode1|display[2]~4_combout\ : std_logic;
SIGNAL \decode1|display[1]~5_combout\ : std_logic;
SIGNAL \decode1|display[0]~6_combout\ : std_logic;
SIGNAL \decode2|display[6]~0_combout\ : std_logic;
SIGNAL \decode2|display[5]~1_combout\ : std_logic;
SIGNAL \decode2|display[4]~2_combout\ : std_logic;
SIGNAL \decode2|display[3]~3_combout\ : std_logic;
SIGNAL \decode2|display[2]~4_combout\ : std_logic;
SIGNAL \decode2|display[1]~5_combout\ : std_logic;
SIGNAL \decode2|display[0]~6_combout\ : std_logic;
SIGNAL \decode3|display[6]~0_combout\ : std_logic;
SIGNAL \decode3|display[5]~1_combout\ : std_logic;
SIGNAL \decode3|display[4]~2_combout\ : std_logic;
SIGNAL \decode3|display[3]~3_combout\ : std_logic;
SIGNAL \decode3|display[2]~4_combout\ : std_logic;
SIGNAL \decode3|display[1]~5_combout\ : std_logic;
SIGNAL \decode3|display[0]~6_combout\ : std_logic;
SIGNAL \ALT_INV_SW[0]~inputclkctrl_outclk\ : std_logic;

BEGIN

ww_SW <= SW;
ww_KEY <= KEY;
LEDR <= ww_LEDR;
HEX0 <= ww_HEX0;
HEX1 <= ww_HEX1;
HEX2 <= ww_HEX2;
HEX3 <= ww_HEX3;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\SW[0]~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \SW[0]~input_o\);

\KEY[0]~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \KEY[0]~input_o\);
\ALT_INV_SW[0]~inputclkctrl_outclk\ <= NOT \SW[0]~inputclkctrl_outclk\;

-- Location: IOOBUF_X48_Y41_N9
\LEDR[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \counter1|temp~q\,
	devoe => ww_devoe,
	o => \LEDR[0]~output_o\);

-- Location: IOOBUF_X38_Y41_N2
\LEDR[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \counter2|temp~q\,
	devoe => ww_devoe,
	o => \LEDR[1]~output_o\);

-- Location: IOOBUF_X48_Y41_N2
\LEDR[2]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \counter3|temp~q\,
	devoe => ww_devoe,
	o => \LEDR[2]~output_o\);

-- Location: IOOBUF_X52_Y32_N16
\LEDR[3]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \counter4|temp~q\,
	devoe => ww_devoe,
	o => \LEDR[3]~output_o\);

-- Location: IOOBUF_X52_Y25_N2
\LEDR[4]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \counter5|temp~q\,
	devoe => ww_devoe,
	o => \LEDR[4]~output_o\);

-- Location: IOOBUF_X52_Y30_N2
\LEDR[5]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \counter6|temp~q\,
	devoe => ww_devoe,
	o => \LEDR[5]~output_o\);

-- Location: IOOBUF_X52_Y25_N9
\LEDR[6]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \counter7|temp~q\,
	devoe => ww_devoe,
	o => \LEDR[6]~output_o\);

-- Location: IOOBUF_X52_Y16_N2
\LEDR[7]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \counter8|temp~q\,
	devoe => ww_devoe,
	o => \LEDR[7]~output_o\);

-- Location: IOOBUF_X52_Y31_N2
\LEDR[8]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \counter9|temp~q\,
	devoe => ww_devoe,
	o => \LEDR[8]~output_o\);

-- Location: IOOBUF_X52_Y32_N9
\LEDR[9]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \counter10|temp~q\,
	devoe => ww_devoe,
	o => \LEDR[9]~output_o\);

-- Location: IOOBUF_X52_Y32_N23
\LEDR[10]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \counter11|temp~q\,
	devoe => ww_devoe,
	o => \LEDR[10]~output_o\);

-- Location: IOOBUF_X52_Y32_N2
\LEDR[11]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \counter12|temp~q\,
	devoe => ww_devoe,
	o => \LEDR[11]~output_o\);

-- Location: IOOBUF_X38_Y41_N9
\LEDR[12]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \counter13|temp~q\,
	devoe => ww_devoe,
	o => \LEDR[12]~output_o\);

-- Location: IOOBUF_X34_Y41_N2
\LEDR[13]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \counter14|temp~q\,
	devoe => ww_devoe,
	o => \LEDR[13]~output_o\);

-- Location: IOOBUF_X46_Y41_N9
\LEDR[14]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \counter15|temp~q\,
	devoe => ww_devoe,
	o => \LEDR[14]~output_o\);

-- Location: IOOBUF_X34_Y41_N9
\LEDR[15]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \counter16|temp~q\,
	devoe => ww_devoe,
	o => \LEDR[15]~output_o\);

-- Location: IOOBUF_X46_Y41_N16
\HEX0[6]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode0|display[6]~0_combout\,
	devoe => ww_devoe,
	o => \HEX0[6]~output_o\);

-- Location: IOOBUF_X46_Y41_N23
\HEX0[5]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode0|display[5]~1_combout\,
	devoe => ww_devoe,
	o => \HEX0[5]~output_o\);

-- Location: IOOBUF_X31_Y41_N2
\HEX0[4]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode0|display[4]~2_combout\,
	devoe => ww_devoe,
	o => \HEX0[4]~output_o\);

-- Location: IOOBUF_X50_Y41_N2
\HEX0[3]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode0|display[3]~3_combout\,
	devoe => ww_devoe,
	o => \HEX0[3]~output_o\);

-- Location: IOOBUF_X31_Y41_N23
\HEX0[2]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode0|display[2]~4_combout\,
	devoe => ww_devoe,
	o => \HEX0[2]~output_o\);

-- Location: IOOBUF_X41_Y41_N9
\HEX0[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode0|display[1]~5_combout\,
	devoe => ww_devoe,
	o => \HEX0[1]~output_o\);

-- Location: IOOBUF_X41_Y41_N2
\HEX0[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode0|display[0]~6_combout\,
	devoe => ww_devoe,
	o => \HEX0[0]~output_o\);

-- Location: IOOBUF_X52_Y19_N9
\HEX1[6]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode1|display[6]~0_combout\,
	devoe => ww_devoe,
	o => \HEX1[6]~output_o\);

-- Location: IOOBUF_X52_Y16_N9
\HEX1[5]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode1|display[5]~1_combout\,
	devoe => ww_devoe,
	o => \HEX1[5]~output_o\);

-- Location: IOOBUF_X52_Y19_N2
\HEX1[4]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode1|display[4]~2_combout\,
	devoe => ww_devoe,
	o => \HEX1[4]~output_o\);

-- Location: IOOBUF_X52_Y18_N2
\HEX1[3]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode1|display[3]~3_combout\,
	devoe => ww_devoe,
	o => \HEX1[3]~output_o\);

-- Location: IOOBUF_X52_Y23_N2
\HEX1[2]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode1|display[2]~4_combout\,
	devoe => ww_devoe,
	o => \HEX1[2]~output_o\);

-- Location: IOOBUF_X52_Y30_N9
\HEX1[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode1|display[1]~5_combout\,
	devoe => ww_devoe,
	o => \HEX1[1]~output_o\);

-- Location: IOOBUF_X52_Y18_N9
\HEX1[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode1|display[0]~6_combout\,
	devoe => ww_devoe,
	o => \HEX1[0]~output_o\);

-- Location: IOOBUF_X52_Y23_N9
\HEX2[6]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode2|display[6]~0_combout\,
	devoe => ww_devoe,
	o => \HEX2[6]~output_o\);

-- Location: IOOBUF_X52_Y28_N2
\HEX2[5]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode2|display[5]~1_combout\,
	devoe => ww_devoe,
	o => \HEX2[5]~output_o\);

-- Location: IOOBUF_X52_Y28_N9
\HEX2[4]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode2|display[4]~2_combout\,
	devoe => ww_devoe,
	o => \HEX2[4]~output_o\);

-- Location: IOOBUF_X52_Y27_N9
\HEX2[3]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode2|display[3]~3_combout\,
	devoe => ww_devoe,
	o => \HEX2[3]~output_o\);

-- Location: IOOBUF_X52_Y15_N2
\HEX2[2]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode2|display[2]~4_combout\,
	devoe => ww_devoe,
	o => \HEX2[2]~output_o\);

-- Location: IOOBUF_X52_Y31_N9
\HEX2[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode2|display[1]~5_combout\,
	devoe => ww_devoe,
	o => \HEX2[1]~output_o\);

-- Location: IOOBUF_X52_Y27_N2
\HEX2[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode2|display[0]~6_combout\,
	devoe => ww_devoe,
	o => \HEX2[0]~output_o\);

-- Location: IOOBUF_X36_Y41_N9
\HEX3[6]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode3|display[6]~0_combout\,
	devoe => ww_devoe,
	o => \HEX3[6]~output_o\);

-- Location: IOOBUF_X43_Y41_N2
\HEX3[5]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode3|display[5]~1_combout\,
	devoe => ww_devoe,
	o => \HEX3[5]~output_o\);

-- Location: IOOBUF_X36_Y41_N2
\HEX3[4]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode3|display[4]~2_combout\,
	devoe => ww_devoe,
	o => \HEX3[4]~output_o\);

-- Location: IOOBUF_X41_Y41_N16
\HEX3[3]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode3|display[3]~3_combout\,
	devoe => ww_devoe,
	o => \HEX3[3]~output_o\);

-- Location: IOOBUF_X43_Y41_N9
\HEX3[2]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode3|display[2]~4_combout\,
	devoe => ww_devoe,
	o => \HEX3[2]~output_o\);

-- Location: IOOBUF_X41_Y41_N23
\HEX3[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode3|display[1]~5_combout\,
	devoe => ww_devoe,
	o => \HEX3[1]~output_o\);

-- Location: IOOBUF_X46_Y41_N2
\HEX3[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \decode3|display[0]~6_combout\,
	devoe => ww_devoe,
	o => \HEX3[0]~output_o\);

-- Location: IOIBUF_X27_Y0_N15
\KEY[0]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(0),
	o => \KEY[0]~input_o\);

-- Location: CLKCTRL_G17
\KEY[0]~inputclkctrl\ : cycloneiv_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \KEY[0]~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \KEY[0]~inputclkctrl_outclk\);

-- Location: IOIBUF_X50_Y41_N8
\SW[1]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(1),
	o => \SW[1]~input_o\);

-- Location: LCCOMB_X49_Y37_N8
\counter1|temp~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \counter1|temp~0_combout\ = \SW[1]~input_o\ $ (\counter1|temp~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \SW[1]~input_o\,
	datac => \counter1|temp~q\,
	combout => \counter1|temp~0_combout\);

-- Location: IOIBUF_X27_Y0_N22
\SW[0]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_SW(0),
	o => \SW[0]~input_o\);

-- Location: CLKCTRL_G19
\SW[0]~inputclkctrl\ : cycloneiv_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \SW[0]~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \SW[0]~inputclkctrl_outclk\);

-- Location: FF_X49_Y37_N9
\counter1|temp\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \KEY[0]~inputclkctrl_outclk\,
	d => \counter1|temp~0_combout\,
	clrn => \ALT_INV_SW[0]~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \counter1|temp~q\);

-- Location: LCCOMB_X50_Y37_N10
\counter2|temp~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \counter2|temp~0_combout\ = \counter2|temp~q\ $ (((\counter1|temp~q\ & \SW[1]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111011110001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter1|temp~q\,
	datab => \SW[1]~input_o\,
	datad => \counter2|temp~q\,
	combout => \counter2|temp~0_combout\);

-- Location: FF_X49_Y37_N19
\counter2|temp\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \KEY[0]~inputclkctrl_outclk\,
	asdata => \counter2|temp~0_combout\,
	clrn => \ALT_INV_SW[0]~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \counter2|temp~q\);

-- Location: LCCOMB_X50_Y37_N12
\counter3|temp~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \counter3|temp~0_combout\ = \counter3|temp~q\ $ (((\counter1|temp~q\ & (\counter2|temp~q\ & \SW[1]~input_o\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter1|temp~q\,
	datab => \counter2|temp~q\,
	datac => \counter3|temp~q\,
	datad => \SW[1]~input_o\,
	combout => \counter3|temp~0_combout\);

-- Location: FF_X49_Y37_N5
\counter3|temp\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \KEY[0]~inputclkctrl_outclk\,
	asdata => \counter3|temp~0_combout\,
	clrn => \ALT_INV_SW[0]~inputclkctrl_outclk\,
	sload => VCC,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \counter3|temp~q\);

-- Location: LCCOMB_X50_Y37_N22
\counter3|temp~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \counter3|temp~1_combout\ = (\counter1|temp~q\ & (\SW[1]~input_o\ & \counter2|temp~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter1|temp~q\,
	datac => \SW[1]~input_o\,
	datad => \counter2|temp~q\,
	combout => \counter3|temp~1_combout\);

-- Location: LCCOMB_X49_Y37_N30
\counter4|temp~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \counter4|temp~0_combout\ = \counter4|temp~q\ $ (((\counter3|temp~q\ & \counter3|temp~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \counter3|temp~q\,
	datac => \counter4|temp~q\,
	datad => \counter3|temp~1_combout\,
	combout => \counter4|temp~0_combout\);

-- Location: FF_X49_Y37_N31
\counter4|temp\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \KEY[0]~inputclkctrl_outclk\,
	d => \counter4|temp~0_combout\,
	clrn => \ALT_INV_SW[0]~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \counter4|temp~q\);

-- Location: LCCOMB_X49_Y37_N12
\counter5|temp~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \counter5|temp~0_combout\ = \counter5|temp~q\ $ (((\counter3|temp~q\ & (\counter4|temp~q\ & \counter3|temp~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter3|temp~q\,
	datab => \counter4|temp~q\,
	datac => \counter5|temp~q\,
	datad => \counter3|temp~1_combout\,
	combout => \counter5|temp~0_combout\);

-- Location: FF_X49_Y37_N13
\counter5|temp\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \KEY[0]~inputclkctrl_outclk\,
	d => \counter5|temp~0_combout\,
	clrn => \ALT_INV_SW[0]~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \counter5|temp~q\);

-- Location: LCCOMB_X49_Y37_N4
\counter6|temp~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \counter6|temp~1_combout\ = (\counter5|temp~q\ & (\SW[1]~input_o\ & \counter2|temp~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter5|temp~q\,
	datab => \SW[1]~input_o\,
	datad => \counter2|temp~q\,
	combout => \counter6|temp~1_combout\);

-- Location: LCCOMB_X49_Y37_N22
\counter6|temp~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \counter6|temp~0_combout\ = (\counter4|temp~q\ & (\counter1|temp~q\ & (\counter3|temp~q\ & \SW[1]~input_o\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter4|temp~q\,
	datab => \counter1|temp~q\,
	datac => \counter3|temp~q\,
	datad => \SW[1]~input_o\,
	combout => \counter6|temp~0_combout\);

-- Location: LCCOMB_X50_Y37_N8
\counter6|temp~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \counter6|temp~2_combout\ = \counter6|temp~q\ $ (((\counter6|temp~1_combout\ & \counter6|temp~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \counter6|temp~1_combout\,
	datac => \counter6|temp~q\,
	datad => \counter6|temp~0_combout\,
	combout => \counter6|temp~2_combout\);

-- Location: FF_X50_Y37_N9
\counter6|temp\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \KEY[0]~inputclkctrl_outclk\,
	d => \counter6|temp~2_combout\,
	clrn => \ALT_INV_SW[0]~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \counter6|temp~q\);

-- Location: LCCOMB_X49_Y37_N26
\counter7|temp~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \counter7|temp~0_combout\ = \counter7|temp~q\ $ (((\counter6|temp~0_combout\ & (\counter6|temp~q\ & \counter6|temp~1_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter6|temp~0_combout\,
	datab => \counter6|temp~q\,
	datac => \counter7|temp~q\,
	datad => \counter6|temp~1_combout\,
	combout => \counter7|temp~0_combout\);

-- Location: FF_X49_Y37_N27
\counter7|temp\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \KEY[0]~inputclkctrl_outclk\,
	d => \counter7|temp~0_combout\,
	clrn => \ALT_INV_SW[0]~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \counter7|temp~q\);

-- Location: LCCOMB_X49_Y37_N18
\counter9|temp~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \counter9|temp~0_combout\ = (\counter6|temp~0_combout\ & (\counter6|temp~1_combout\ & \counter6|temp~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter6|temp~0_combout\,
	datab => \counter6|temp~1_combout\,
	datad => \counter6|temp~q\,
	combout => \counter9|temp~0_combout\);

-- Location: LCCOMB_X49_Y37_N20
\counter8|temp~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \counter8|temp~0_combout\ = \counter8|temp~q\ $ (((\counter7|temp~q\ & \counter9|temp~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \counter7|temp~q\,
	datac => \counter8|temp~q\,
	datad => \counter9|temp~0_combout\,
	combout => \counter8|temp~0_combout\);

-- Location: FF_X49_Y37_N21
\counter8|temp\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \KEY[0]~inputclkctrl_outclk\,
	d => \counter8|temp~0_combout\,
	clrn => \ALT_INV_SW[0]~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \counter8|temp~q\);

-- Location: LCCOMB_X49_Y37_N10
\counter9|temp~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \counter9|temp~1_combout\ = \counter9|temp~q\ $ (((\counter7|temp~q\ & (\counter8|temp~q\ & \counter9|temp~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter7|temp~q\,
	datab => \counter8|temp~q\,
	datac => \counter9|temp~q\,
	datad => \counter9|temp~0_combout\,
	combout => \counter9|temp~1_combout\);

-- Location: FF_X49_Y37_N11
\counter9|temp\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \KEY[0]~inputclkctrl_outclk\,
	d => \counter9|temp~1_combout\,
	clrn => \ALT_INV_SW[0]~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \counter9|temp~q\);

-- Location: LCCOMB_X49_Y37_N28
\counter10|temp~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \counter10|temp~0_combout\ = (\counter8|temp~q\ & (\counter7|temp~q\ & \counter9|temp~q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \counter8|temp~q\,
	datac => \counter7|temp~q\,
	datad => \counter9|temp~q\,
	combout => \counter10|temp~0_combout\);

-- Location: LCCOMB_X49_Y37_N2
\counter10|temp~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \counter10|temp~1_combout\ = (\counter6|temp~0_combout\ & (\counter6|temp~q\ & (\counter6|temp~1_combout\ & \counter10|temp~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter6|temp~0_combout\,
	datab => \counter6|temp~q\,
	datac => \counter6|temp~1_combout\,
	datad => \counter10|temp~0_combout\,
	combout => \counter10|temp~1_combout\);

-- Location: LCCOMB_X49_Y37_N16
\counter10|temp~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \counter10|temp~2_combout\ = \counter10|temp~q\ $ (\counter10|temp~1_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \counter10|temp~q\,
	datad => \counter10|temp~1_combout\,
	combout => \counter10|temp~2_combout\);

-- Location: FF_X49_Y37_N17
\counter10|temp\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \KEY[0]~inputclkctrl_outclk\,
	d => \counter10|temp~2_combout\,
	clrn => \ALT_INV_SW[0]~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \counter10|temp~q\);

-- Location: LCCOMB_X50_Y37_N6
\counter11|temp~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \counter11|temp~0_combout\ = \counter11|temp~q\ $ (((\counter10|temp~q\ & \counter10|temp~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter10|temp~q\,
	datac => \counter11|temp~q\,
	datad => \counter10|temp~1_combout\,
	combout => \counter11|temp~0_combout\);

-- Location: FF_X50_Y37_N7
\counter11|temp\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \KEY[0]~inputclkctrl_outclk\,
	d => \counter11|temp~0_combout\,
	clrn => \ALT_INV_SW[0]~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \counter11|temp~q\);

-- Location: LCCOMB_X49_Y37_N14
\counter12|temp~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \counter12|temp~0_combout\ = \counter12|temp~q\ $ (((\counter10|temp~q\ & (\counter10|temp~1_combout\ & \counter11|temp~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter10|temp~q\,
	datab => \counter10|temp~1_combout\,
	datac => \counter12|temp~q\,
	datad => \counter11|temp~q\,
	combout => \counter12|temp~0_combout\);

-- Location: FF_X49_Y37_N15
\counter12|temp\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \KEY[0]~inputclkctrl_outclk\,
	d => \counter12|temp~0_combout\,
	clrn => \ALT_INV_SW[0]~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \counter12|temp~q\);

-- Location: LCCOMB_X49_Y37_N24
\counter13|temp~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \counter13|temp~0_combout\ = (\counter11|temp~q\ & (\counter10|temp~q\ & (\counter12|temp~q\ & \counter10|temp~1_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter11|temp~q\,
	datab => \counter10|temp~q\,
	datac => \counter12|temp~q\,
	datad => \counter10|temp~1_combout\,
	combout => \counter13|temp~0_combout\);

-- Location: LCCOMB_X48_Y37_N8
\counter13|temp~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \counter13|temp~1_combout\ = \counter13|temp~q\ $ (\counter13|temp~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \counter13|temp~q\,
	datad => \counter13|temp~0_combout\,
	combout => \counter13|temp~1_combout\);

-- Location: FF_X48_Y37_N9
\counter13|temp\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \KEY[0]~inputclkctrl_outclk\,
	d => \counter13|temp~1_combout\,
	clrn => \ALT_INV_SW[0]~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \counter13|temp~q\);

-- Location: LCCOMB_X48_Y37_N26
\counter14|temp~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \counter14|temp~0_combout\ = \counter14|temp~q\ $ (((\counter13|temp~q\ & \counter13|temp~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \counter13|temp~q\,
	datac => \counter14|temp~q\,
	datad => \counter13|temp~0_combout\,
	combout => \counter14|temp~0_combout\);

-- Location: FF_X48_Y37_N27
\counter14|temp\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \KEY[0]~inputclkctrl_outclk\,
	d => \counter14|temp~0_combout\,
	clrn => \ALT_INV_SW[0]~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \counter14|temp~q\);

-- Location: LCCOMB_X48_Y37_N24
\counter15|temp~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \counter15|temp~0_combout\ = \counter15|temp~q\ $ (((\counter14|temp~q\ & (\counter13|temp~q\ & \counter13|temp~0_combout\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0111100011110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter14|temp~q\,
	datab => \counter13|temp~q\,
	datac => \counter15|temp~q\,
	datad => \counter13|temp~0_combout\,
	combout => \counter15|temp~0_combout\);

-- Location: FF_X48_Y37_N25
\counter15|temp\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \KEY[0]~inputclkctrl_outclk\,
	d => \counter15|temp~0_combout\,
	clrn => \ALT_INV_SW[0]~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \counter15|temp~q\);

-- Location: LCCOMB_X49_Y37_N6
\counter16|temp~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \counter16|temp~0_combout\ = (\counter13|temp~q\ & (\counter15|temp~q\ & (\counter14|temp~q\ & \counter13|temp~0_combout\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000000000000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter13|temp~q\,
	datab => \counter15|temp~q\,
	datac => \counter14|temp~q\,
	datad => \counter13|temp~0_combout\,
	combout => \counter16|temp~0_combout\);

-- Location: LCCOMB_X49_Y37_N0
\counter16|temp~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \counter16|temp~1_combout\ = \counter16|temp~q\ $ (\counter16|temp~0_combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \counter16|temp~q\,
	datad => \counter16|temp~0_combout\,
	combout => \counter16|temp~1_combout\);

-- Location: FF_X49_Y37_N1
\counter16|temp\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \KEY[0]~inputclkctrl_outclk\,
	d => \counter16|temp~1_combout\,
	clrn => \ALT_INV_SW[0]~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \counter16|temp~q\);

-- Location: LCCOMB_X50_Y37_N28
\decode0|display[6]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode0|display[6]~0_combout\ = (\counter1|temp~q\ & (!\counter4|temp~q\ & (\counter2|temp~q\ $ (!\counter3|temp~q\)))) # (!\counter1|temp~q\ & (!\counter2|temp~q\ & (\counter4|temp~q\ $ (!\counter3|temp~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100001000010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter4|temp~q\,
	datab => \counter2|temp~q\,
	datac => \counter1|temp~q\,
	datad => \counter3|temp~q\,
	combout => \decode0|display[6]~0_combout\);

-- Location: LCCOMB_X50_Y37_N30
\decode0|display[5]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode0|display[5]~1_combout\ = (\counter2|temp~q\ & (!\counter4|temp~q\ & ((\counter1|temp~q\) # (!\counter3|temp~q\)))) # (!\counter2|temp~q\ & (\counter1|temp~q\ & (\counter4|temp~q\ $ (!\counter3|temp~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000001010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter4|temp~q\,
	datab => \counter2|temp~q\,
	datac => \counter1|temp~q\,
	datad => \counter3|temp~q\,
	combout => \decode0|display[5]~1_combout\);

-- Location: LCCOMB_X50_Y37_N16
\decode0|display[4]~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode0|display[4]~2_combout\ = (\counter2|temp~q\ & (!\counter4|temp~q\ & (\counter1|temp~q\))) # (!\counter2|temp~q\ & ((\counter3|temp~q\ & (!\counter4|temp~q\)) # (!\counter3|temp~q\ & ((\counter1|temp~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000101110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter4|temp~q\,
	datab => \counter2|temp~q\,
	datac => \counter1|temp~q\,
	datad => \counter3|temp~q\,
	combout => \decode0|display[4]~2_combout\);

-- Location: LCCOMB_X50_Y37_N14
\decode0|display[3]~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode0|display[3]~3_combout\ = (\counter2|temp~q\ & ((\counter1|temp~q\ & ((\counter3|temp~q\))) # (!\counter1|temp~q\ & (\counter4|temp~q\ & !\counter3|temp~q\)))) # (!\counter2|temp~q\ & (!\counter4|temp~q\ & (\counter1|temp~q\ $ 
-- (\counter3|temp~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000100011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter4|temp~q\,
	datab => \counter2|temp~q\,
	datac => \counter1|temp~q\,
	datad => \counter3|temp~q\,
	combout => \decode0|display[3]~3_combout\);

-- Location: LCCOMB_X50_Y37_N0
\decode0|display[2]~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode0|display[2]~4_combout\ = (\counter4|temp~q\ & (\counter3|temp~q\ & ((\counter2|temp~q\) # (!\counter1|temp~q\)))) # (!\counter4|temp~q\ & (\counter2|temp~q\ & (!\counter1|temp~q\ & !\counter3|temp~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter4|temp~q\,
	datab => \counter2|temp~q\,
	datac => \counter1|temp~q\,
	datad => \counter3|temp~q\,
	combout => \decode0|display[2]~4_combout\);

-- Location: LCCOMB_X50_Y37_N26
\decode0|display[1]~5\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode0|display[1]~5_combout\ = (\counter4|temp~q\ & ((\counter1|temp~q\ & (\counter2|temp~q\)) # (!\counter1|temp~q\ & ((\counter3|temp~q\))))) # (!\counter4|temp~q\ & (\counter3|temp~q\ & (\counter2|temp~q\ $ (\counter1|temp~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001111010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter4|temp~q\,
	datab => \counter2|temp~q\,
	datac => \counter1|temp~q\,
	datad => \counter3|temp~q\,
	combout => \decode0|display[1]~5_combout\);

-- Location: LCCOMB_X50_Y37_N24
\decode0|display[0]~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode0|display[0]~6_combout\ = (\counter4|temp~q\ & (\counter1|temp~q\ & (\counter2|temp~q\ $ (\counter3|temp~q\)))) # (!\counter4|temp~q\ & (!\counter2|temp~q\ & (\counter1|temp~q\ $ (\counter3|temp~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000110010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter4|temp~q\,
	datab => \counter2|temp~q\,
	datac => \counter1|temp~q\,
	datad => \counter3|temp~q\,
	combout => \decode0|display[0]~6_combout\);

-- Location: LCCOMB_X51_Y30_N8
\decode1|display[6]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode1|display[6]~0_combout\ = (\counter5|temp~q\ & (!\counter8|temp~q\ & (\counter7|temp~q\ $ (!\counter6|temp~q\)))) # (!\counter5|temp~q\ & (!\counter6|temp~q\ & (\counter7|temp~q\ $ (!\counter8|temp~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000001010000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter7|temp~q\,
	datab => \counter5|temp~q\,
	datac => \counter6|temp~q\,
	datad => \counter8|temp~q\,
	combout => \decode1|display[6]~0_combout\);

-- Location: LCCOMB_X51_Y30_N18
\decode1|display[5]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode1|display[5]~1_combout\ = (\counter7|temp~q\ & (\counter5|temp~q\ & (\counter6|temp~q\ $ (\counter8|temp~q\)))) # (!\counter7|temp~q\ & (!\counter8|temp~q\ & ((\counter5|temp~q\) # (\counter6|temp~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000100011010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter7|temp~q\,
	datab => \counter5|temp~q\,
	datac => \counter6|temp~q\,
	datad => \counter8|temp~q\,
	combout => \decode1|display[5]~1_combout\);

-- Location: LCCOMB_X51_Y30_N12
\decode1|display[4]~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode1|display[4]~2_combout\ = (\counter6|temp~q\ & (((\counter5|temp~q\ & !\counter8|temp~q\)))) # (!\counter6|temp~q\ & ((\counter7|temp~q\ & ((!\counter8|temp~q\))) # (!\counter7|temp~q\ & (\counter5|temp~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000010011001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter7|temp~q\,
	datab => \counter5|temp~q\,
	datac => \counter6|temp~q\,
	datad => \counter8|temp~q\,
	combout => \decode1|display[4]~2_combout\);

-- Location: LCCOMB_X51_Y30_N10
\decode1|display[3]~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode1|display[3]~3_combout\ = (\counter6|temp~q\ & ((\counter7|temp~q\ & (\counter5|temp~q\)) # (!\counter7|temp~q\ & (!\counter5|temp~q\ & \counter8|temp~q\)))) # (!\counter6|temp~q\ & (!\counter8|temp~q\ & (\counter7|temp~q\ $ (\counter5|temp~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001000010000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter7|temp~q\,
	datab => \counter5|temp~q\,
	datac => \counter6|temp~q\,
	datad => \counter8|temp~q\,
	combout => \decode1|display[3]~3_combout\);

-- Location: LCCOMB_X51_Y30_N20
\decode1|display[2]~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode1|display[2]~4_combout\ = (\counter7|temp~q\ & (\counter8|temp~q\ & ((\counter6|temp~q\) # (!\counter5|temp~q\)))) # (!\counter7|temp~q\ & (!\counter5|temp~q\ & (\counter6|temp~q\ & !\counter8|temp~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010001000010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter7|temp~q\,
	datab => \counter5|temp~q\,
	datac => \counter6|temp~q\,
	datad => \counter8|temp~q\,
	combout => \decode1|display[2]~4_combout\);

-- Location: LCCOMB_X51_Y30_N6
\decode1|display[1]~5\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode1|display[1]~5_combout\ = (\counter6|temp~q\ & ((\counter5|temp~q\ & ((\counter8|temp~q\))) # (!\counter5|temp~q\ & (\counter7|temp~q\)))) # (!\counter6|temp~q\ & (\counter7|temp~q\ & (\counter5|temp~q\ $ (\counter8|temp~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1110001000101000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter7|temp~q\,
	datab => \counter5|temp~q\,
	datac => \counter6|temp~q\,
	datad => \counter8|temp~q\,
	combout => \decode1|display[1]~5_combout\);

-- Location: LCCOMB_X51_Y30_N28
\decode1|display[0]~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode1|display[0]~6_combout\ = (\counter7|temp~q\ & (!\counter6|temp~q\ & (\counter5|temp~q\ $ (!\counter8|temp~q\)))) # (!\counter7|temp~q\ & (\counter5|temp~q\ & (\counter6|temp~q\ $ (!\counter8|temp~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100000000110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter7|temp~q\,
	datab => \counter5|temp~q\,
	datac => \counter6|temp~q\,
	datad => \counter8|temp~q\,
	combout => \decode1|display[0]~6_combout\);

-- Location: LCCOMB_X51_Y31_N20
\decode2|display[6]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode2|display[6]~0_combout\ = (\counter9|temp~q\ & (!\counter12|temp~q\ & (\counter10|temp~q\ $ (!\counter11|temp~q\)))) # (!\counter9|temp~q\ & (!\counter10|temp~q\ & (\counter12|temp~q\ $ (!\counter11|temp~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001100000000101",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter10|temp~q\,
	datab => \counter9|temp~q\,
	datac => \counter12|temp~q\,
	datad => \counter11|temp~q\,
	combout => \decode2|display[6]~0_combout\);

-- Location: LCCOMB_X51_Y31_N26
\decode2|display[5]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode2|display[5]~1_combout\ = (\counter10|temp~q\ & (!\counter12|temp~q\ & ((\counter9|temp~q\) # (!\counter11|temp~q\)))) # (!\counter10|temp~q\ & (\counter9|temp~q\ & (\counter12|temp~q\ $ (!\counter11|temp~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100100000001110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter10|temp~q\,
	datab => \counter9|temp~q\,
	datac => \counter12|temp~q\,
	datad => \counter11|temp~q\,
	combout => \decode2|display[5]~1_combout\);

-- Location: LCCOMB_X51_Y31_N12
\decode2|display[4]~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode2|display[4]~2_combout\ = (\counter10|temp~q\ & (\counter9|temp~q\ & (!\counter12|temp~q\))) # (!\counter10|temp~q\ & ((\counter11|temp~q\ & ((!\counter12|temp~q\))) # (!\counter11|temp~q\ & (\counter9|temp~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000110101001100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter10|temp~q\,
	datab => \counter9|temp~q\,
	datac => \counter12|temp~q\,
	datad => \counter11|temp~q\,
	combout => \decode2|display[4]~2_combout\);

-- Location: LCCOMB_X51_Y31_N10
\decode2|display[3]~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode2|display[3]~3_combout\ = (\counter10|temp~q\ & ((\counter9|temp~q\ & ((\counter11|temp~q\))) # (!\counter9|temp~q\ & (\counter12|temp~q\ & !\counter11|temp~q\)))) # (!\counter10|temp~q\ & (!\counter12|temp~q\ & (\counter9|temp~q\ $ 
-- (\counter11|temp~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000100100100100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter10|temp~q\,
	datab => \counter9|temp~q\,
	datac => \counter12|temp~q\,
	datad => \counter11|temp~q\,
	combout => \decode2|display[3]~3_combout\);

-- Location: LCCOMB_X51_Y31_N4
\decode2|display[2]~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode2|display[2]~4_combout\ = (\counter12|temp~q\ & (\counter11|temp~q\ & ((\counter10|temp~q\) # (!\counter9|temp~q\)))) # (!\counter12|temp~q\ & (\counter10|temp~q\ & (!\counter9|temp~q\ & !\counter11|temp~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011000000000010",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter10|temp~q\,
	datab => \counter9|temp~q\,
	datac => \counter12|temp~q\,
	datad => \counter11|temp~q\,
	combout => \decode2|display[2]~4_combout\);

-- Location: LCCOMB_X51_Y31_N6
\decode2|display[1]~5\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode2|display[1]~5_combout\ = (\counter10|temp~q\ & ((\counter9|temp~q\ & (\counter12|temp~q\)) # (!\counter9|temp~q\ & ((\counter11|temp~q\))))) # (!\counter10|temp~q\ & (\counter11|temp~q\ & (\counter9|temp~q\ $ (\counter12|temp~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1011011010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter10|temp~q\,
	datab => \counter9|temp~q\,
	datac => \counter12|temp~q\,
	datad => \counter11|temp~q\,
	combout => \decode2|display[1]~5_combout\);

-- Location: LCCOMB_X51_Y31_N24
\decode2|display[0]~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode2|display[0]~6_combout\ = (\counter12|temp~q\ & (\counter9|temp~q\ & (\counter10|temp~q\ $ (\counter11|temp~q\)))) # (!\counter12|temp~q\ & (!\counter10|temp~q\ & (\counter9|temp~q\ $ (\counter11|temp~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100000110000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter10|temp~q\,
	datab => \counter9|temp~q\,
	datac => \counter12|temp~q\,
	datad => \counter11|temp~q\,
	combout => \decode2|display[0]~6_combout\);

-- Location: LCCOMB_X48_Y37_N10
\decode3|display[6]~0\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode3|display[6]~0_combout\ = (\counter13|temp~q\ & (!\counter16|temp~q\ & (\counter14|temp~q\ $ (!\counter15|temp~q\)))) # (!\counter13|temp~q\ & (!\counter14|temp~q\ & (\counter16|temp~q\ $ (!\counter15|temp~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100001000010001",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter16|temp~q\,
	datab => \counter14|temp~q\,
	datac => \counter13|temp~q\,
	datad => \counter15|temp~q\,
	combout => \decode3|display[6]~0_combout\);

-- Location: LCCOMB_X48_Y37_N4
\decode3|display[5]~1\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode3|display[5]~1_combout\ = (\counter14|temp~q\ & (!\counter16|temp~q\ & ((\counter13|temp~q\) # (!\counter15|temp~q\)))) # (!\counter14|temp~q\ & (\counter13|temp~q\ & (\counter16|temp~q\ $ (!\counter15|temp~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110000001010100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter16|temp~q\,
	datab => \counter14|temp~q\,
	datac => \counter13|temp~q\,
	datad => \counter15|temp~q\,
	combout => \decode3|display[5]~1_combout\);

-- Location: LCCOMB_X48_Y37_N14
\decode3|display[4]~2\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode3|display[4]~2_combout\ = (\counter14|temp~q\ & (!\counter16|temp~q\ & (\counter13|temp~q\))) # (!\counter14|temp~q\ & ((\counter15|temp~q\ & (!\counter16|temp~q\)) # (!\counter15|temp~q\ & ((\counter13|temp~q\)))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101000101110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter16|temp~q\,
	datab => \counter14|temp~q\,
	datac => \counter13|temp~q\,
	datad => \counter15|temp~q\,
	combout => \decode3|display[4]~2_combout\);

-- Location: LCCOMB_X48_Y37_N20
\decode3|display[3]~3\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode3|display[3]~3_combout\ = (\counter14|temp~q\ & ((\counter13|temp~q\ & ((\counter15|temp~q\))) # (!\counter13|temp~q\ & (\counter16|temp~q\ & !\counter15|temp~q\)))) # (!\counter14|temp~q\ & (!\counter16|temp~q\ & (\counter13|temp~q\ $ 
-- (\counter15|temp~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100000100011000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter16|temp~q\,
	datab => \counter14|temp~q\,
	datac => \counter13|temp~q\,
	datad => \counter15|temp~q\,
	combout => \decode3|display[3]~3_combout\);

-- Location: LCCOMB_X48_Y37_N22
\decode3|display[2]~4\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode3|display[2]~4_combout\ = (\counter16|temp~q\ & (\counter15|temp~q\ & ((\counter14|temp~q\) # (!\counter13|temp~q\)))) # (!\counter16|temp~q\ & (\counter14|temp~q\ & (!\counter13|temp~q\ & !\counter15|temp~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000101000000100",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter16|temp~q\,
	datab => \counter14|temp~q\,
	datac => \counter13|temp~q\,
	datad => \counter15|temp~q\,
	combout => \decode3|display[2]~4_combout\);

-- Location: LCCOMB_X48_Y37_N12
\decode3|display[1]~5\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode3|display[1]~5_combout\ = (\counter16|temp~q\ & ((\counter13|temp~q\ & (\counter14|temp~q\)) # (!\counter13|temp~q\ & ((\counter15|temp~q\))))) # (!\counter16|temp~q\ & (\counter15|temp~q\ & (\counter14|temp~q\ $ (\counter13|temp~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1001111010000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter16|temp~q\,
	datab => \counter14|temp~q\,
	datac => \counter13|temp~q\,
	datad => \counter15|temp~q\,
	combout => \decode3|display[1]~5_combout\);

-- Location: LCCOMB_X48_Y37_N2
\decode3|display[0]~6\ : cycloneiv_lcell_comb
-- Equation(s):
-- \decode3|display[0]~6_combout\ = (\counter16|temp~q\ & (\counter13|temp~q\ & (\counter14|temp~q\ $ (\counter15|temp~q\)))) # (!\counter16|temp~q\ & (!\counter14|temp~q\ & (\counter13|temp~q\ $ (\counter15|temp~q\))))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010000110010000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \counter16|temp~q\,
	datab => \counter14|temp~q\,
	datac => \counter13|temp~q\,
	datad => \counter15|temp~q\,
	combout => \decode3|display[0]~6_combout\);

-- Location: IOIBUF_X23_Y0_N1
\KEY[1]~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_KEY(1),
	o => \KEY[1]~input_o\);

ww_LEDR(0) <= \LEDR[0]~output_o\;

ww_LEDR(1) <= \LEDR[1]~output_o\;

ww_LEDR(2) <= \LEDR[2]~output_o\;

ww_LEDR(3) <= \LEDR[3]~output_o\;

ww_LEDR(4) <= \LEDR[4]~output_o\;

ww_LEDR(5) <= \LEDR[5]~output_o\;

ww_LEDR(6) <= \LEDR[6]~output_o\;

ww_LEDR(7) <= \LEDR[7]~output_o\;

ww_LEDR(8) <= \LEDR[8]~output_o\;

ww_LEDR(9) <= \LEDR[9]~output_o\;

ww_LEDR(10) <= \LEDR[10]~output_o\;

ww_LEDR(11) <= \LEDR[11]~output_o\;

ww_LEDR(12) <= \LEDR[12]~output_o\;

ww_LEDR(13) <= \LEDR[13]~output_o\;

ww_LEDR(14) <= \LEDR[14]~output_o\;

ww_LEDR(15) <= \LEDR[15]~output_o\;

ww_HEX0(6) <= \HEX0[6]~output_o\;

ww_HEX0(5) <= \HEX0[5]~output_o\;

ww_HEX0(4) <= \HEX0[4]~output_o\;

ww_HEX0(3) <= \HEX0[3]~output_o\;

ww_HEX0(2) <= \HEX0[2]~output_o\;

ww_HEX0(1) <= \HEX0[1]~output_o\;

ww_HEX0(0) <= \HEX0[0]~output_o\;

ww_HEX1(6) <= \HEX1[6]~output_o\;

ww_HEX1(5) <= \HEX1[5]~output_o\;

ww_HEX1(4) <= \HEX1[4]~output_o\;

ww_HEX1(3) <= \HEX1[3]~output_o\;

ww_HEX1(2) <= \HEX1[2]~output_o\;

ww_HEX1(1) <= \HEX1[1]~output_o\;

ww_HEX1(0) <= \HEX1[0]~output_o\;

ww_HEX2(6) <= \HEX2[6]~output_o\;

ww_HEX2(5) <= \HEX2[5]~output_o\;

ww_HEX2(4) <= \HEX2[4]~output_o\;

ww_HEX2(3) <= \HEX2[3]~output_o\;

ww_HEX2(2) <= \HEX2[2]~output_o\;

ww_HEX2(1) <= \HEX2[1]~output_o\;

ww_HEX2(0) <= \HEX2[0]~output_o\;

ww_HEX3(6) <= \HEX3[6]~output_o\;

ww_HEX3(5) <= \HEX3[5]~output_o\;

ww_HEX3(4) <= \HEX3[4]~output_o\;

ww_HEX3(3) <= \HEX3[3]~output_o\;

ww_HEX3(2) <= \HEX3[2]~output_o\;

ww_HEX3(1) <= \HEX3[1]~output_o\;

ww_HEX3(0) <= \HEX3[0]~output_o\;
END structure;


