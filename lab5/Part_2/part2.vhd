library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_signed.all;


entity part2 is
		port(clk : in STD_LOGIC;
			  R : in STD_LOGIC;
			  enable : in STD_LOGIC;
			  Q : BUFFER STD_LOGIC_VECTOR(15 downto 0));
end part2;

architecture rtl of part2 is
		signal count : std_logic_vector(15 downto 0);
begin
	PROCESS(clk, R)
	BEGIN
			IF R = '1' then
					Q <= (others => '0');
			ELSIF (clk'event AND clk='1' AND enable='1') then
					Q <= Q+1;
			END IF;
			count <= Q;
	END PROCESS; 
end rtl;

