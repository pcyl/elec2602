-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 13.0.1 Build 232 06/12/2013 Service Pack 1 SJ Web Edition"

-- DATE "04/23/2015 12:23:30"

-- 
-- Device: Altera EP2C35F672C6 Package FBGA672
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY CYCLONEII;
LIBRARY IEEE;
USE CYCLONEII.CYCLONEII_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	part2 IS
    PORT (
	clk : IN std_logic;
	R : IN std_logic;
	enable : IN std_logic;
	Q : BUFFER std_logic_vector(15 DOWNTO 0)
	);
END part2;

-- Design Ports Information
-- Q[0]	=>  Location: PIN_G16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- Q[1]	=>  Location: PIN_H15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- Q[2]	=>  Location: PIN_D15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- Q[3]	=>  Location: PIN_G15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- Q[4]	=>  Location: PIN_J17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- Q[5]	=>  Location: PIN_A18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- Q[6]	=>  Location: PIN_D16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- Q[7]	=>  Location: PIN_F16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- Q[8]	=>  Location: PIN_B18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- Q[9]	=>  Location: PIN_C17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- Q[10]	=>  Location: PIN_E15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- Q[11]	=>  Location: PIN_A17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- Q[12]	=>  Location: PIN_D17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- Q[13]	=>  Location: PIN_B17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- Q[14]	=>  Location: PIN_H16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- Q[15]	=>  Location: PIN_F15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
-- enable	=>  Location: PIN_C13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- clk	=>  Location: PIN_P2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
-- R	=>  Location: PIN_P1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default


ARCHITECTURE structure OF part2 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_R : std_logic;
SIGNAL ww_enable : std_logic;
SIGNAL ww_Q : std_logic_vector(15 DOWNTO 0);
SIGNAL \clk~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \R~clkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \clk~combout\ : std_logic;
SIGNAL \clk~clkctrl_outclk\ : std_logic;
SIGNAL \enable~combout\ : std_logic;
SIGNAL \Q[0]~15_combout\ : std_logic;
SIGNAL \R~combout\ : std_logic;
SIGNAL \R~clkctrl_outclk\ : std_logic;
SIGNAL \Q[0]~reg0_regout\ : std_logic;
SIGNAL \Q[1]~16_combout\ : std_logic;
SIGNAL \Q[1]~reg0_regout\ : std_logic;
SIGNAL \Q[1]~17\ : std_logic;
SIGNAL \Q[2]~18_combout\ : std_logic;
SIGNAL \Q[2]~reg0_regout\ : std_logic;
SIGNAL \Q[2]~19\ : std_logic;
SIGNAL \Q[3]~20_combout\ : std_logic;
SIGNAL \Q[3]~reg0_regout\ : std_logic;
SIGNAL \Q[3]~21\ : std_logic;
SIGNAL \Q[4]~22_combout\ : std_logic;
SIGNAL \Q[4]~reg0_regout\ : std_logic;
SIGNAL \Q[4]~23\ : std_logic;
SIGNAL \Q[5]~24_combout\ : std_logic;
SIGNAL \Q[5]~reg0_regout\ : std_logic;
SIGNAL \Q[5]~25\ : std_logic;
SIGNAL \Q[6]~26_combout\ : std_logic;
SIGNAL \Q[6]~reg0_regout\ : std_logic;
SIGNAL \Q[6]~27\ : std_logic;
SIGNAL \Q[7]~28_combout\ : std_logic;
SIGNAL \Q[7]~reg0_regout\ : std_logic;
SIGNAL \Q[7]~29\ : std_logic;
SIGNAL \Q[8]~30_combout\ : std_logic;
SIGNAL \Q[8]~reg0_regout\ : std_logic;
SIGNAL \Q[8]~31\ : std_logic;
SIGNAL \Q[9]~32_combout\ : std_logic;
SIGNAL \Q[9]~reg0_regout\ : std_logic;
SIGNAL \Q[9]~33\ : std_logic;
SIGNAL \Q[10]~34_combout\ : std_logic;
SIGNAL \Q[10]~reg0_regout\ : std_logic;
SIGNAL \Q[10]~35\ : std_logic;
SIGNAL \Q[11]~36_combout\ : std_logic;
SIGNAL \Q[11]~reg0_regout\ : std_logic;
SIGNAL \Q[11]~37\ : std_logic;
SIGNAL \Q[12]~38_combout\ : std_logic;
SIGNAL \Q[12]~reg0_regout\ : std_logic;
SIGNAL \Q[12]~39\ : std_logic;
SIGNAL \Q[13]~40_combout\ : std_logic;
SIGNAL \Q[13]~reg0_regout\ : std_logic;
SIGNAL \Q[13]~41\ : std_logic;
SIGNAL \Q[14]~42_combout\ : std_logic;
SIGNAL \Q[14]~reg0_regout\ : std_logic;
SIGNAL \Q[14]~43\ : std_logic;
SIGNAL \Q[15]~44_combout\ : std_logic;
SIGNAL \Q[15]~reg0_regout\ : std_logic;

BEGIN

ww_clk <= clk;
ww_R <= R;
ww_enable <= enable;
Q <= ww_Q;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\clk~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \clk~combout\);

\R~clkctrl_INCLK_bus\ <= (gnd & gnd & gnd & \R~combout\);

-- Location: PIN_P2,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\clk~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_clk,
	combout => \clk~combout\);

-- Location: CLKCTRL_G3
\clk~clkctrl\ : cycloneii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \clk~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clk~clkctrl_outclk\);

-- Location: PIN_C13,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\enable~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_enable,
	combout => \enable~combout\);

-- Location: LCCOMB_X43_Y35_N30
\Q[0]~15\ : cycloneii_lcell_comb
-- Equation(s):
-- \Q[0]~15_combout\ = \Q[0]~reg0_regout\ $ (\enable~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Q[0]~reg0_regout\,
	datad => \enable~combout\,
	combout => \Q[0]~15_combout\);

-- Location: PIN_P1,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: Default
\R~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "input",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	padio => ww_R,
	combout => \R~combout\);

-- Location: CLKCTRL_G1
\R~clkctrl\ : cycloneii_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \R~clkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \R~clkctrl_outclk\);

-- Location: LCFF_X43_Y35_N31
\Q[0]~reg0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \Q[0]~15_combout\,
	aclr => \R~clkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Q[0]~reg0_regout\);

-- Location: LCCOMB_X43_Y35_N0
\Q[1]~16\ : cycloneii_lcell_comb
-- Equation(s):
-- \Q[1]~16_combout\ = (\Q[0]~reg0_regout\ & (\Q[1]~reg0_regout\ $ (VCC))) # (!\Q[0]~reg0_regout\ & (\Q[1]~reg0_regout\ & VCC))
-- \Q[1]~17\ = CARRY((\Q[0]~reg0_regout\ & \Q[1]~reg0_regout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Q[0]~reg0_regout\,
	datab => \Q[1]~reg0_regout\,
	datad => VCC,
	combout => \Q[1]~16_combout\,
	cout => \Q[1]~17\);

-- Location: LCFF_X43_Y35_N1
\Q[1]~reg0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \Q[1]~16_combout\,
	aclr => \R~clkctrl_outclk\,
	ena => \enable~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Q[1]~reg0_regout\);

-- Location: LCCOMB_X43_Y35_N2
\Q[2]~18\ : cycloneii_lcell_comb
-- Equation(s):
-- \Q[2]~18_combout\ = (\Q[2]~reg0_regout\ & (!\Q[1]~17\)) # (!\Q[2]~reg0_regout\ & ((\Q[1]~17\) # (GND)))
-- \Q[2]~19\ = CARRY((!\Q[1]~17\) # (!\Q[2]~reg0_regout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Q[2]~reg0_regout\,
	datad => VCC,
	cin => \Q[1]~17\,
	combout => \Q[2]~18_combout\,
	cout => \Q[2]~19\);

-- Location: LCFF_X43_Y35_N3
\Q[2]~reg0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \Q[2]~18_combout\,
	aclr => \R~clkctrl_outclk\,
	ena => \enable~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Q[2]~reg0_regout\);

-- Location: LCCOMB_X43_Y35_N4
\Q[3]~20\ : cycloneii_lcell_comb
-- Equation(s):
-- \Q[3]~20_combout\ = (\Q[3]~reg0_regout\ & (\Q[2]~19\ $ (GND))) # (!\Q[3]~reg0_regout\ & (!\Q[2]~19\ & VCC))
-- \Q[3]~21\ = CARRY((\Q[3]~reg0_regout\ & !\Q[2]~19\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Q[3]~reg0_regout\,
	datad => VCC,
	cin => \Q[2]~19\,
	combout => \Q[3]~20_combout\,
	cout => \Q[3]~21\);

-- Location: LCFF_X43_Y35_N5
\Q[3]~reg0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \Q[3]~20_combout\,
	aclr => \R~clkctrl_outclk\,
	ena => \enable~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Q[3]~reg0_regout\);

-- Location: LCCOMB_X43_Y35_N6
\Q[4]~22\ : cycloneii_lcell_comb
-- Equation(s):
-- \Q[4]~22_combout\ = (\Q[4]~reg0_regout\ & (!\Q[3]~21\)) # (!\Q[4]~reg0_regout\ & ((\Q[3]~21\) # (GND)))
-- \Q[4]~23\ = CARRY((!\Q[3]~21\) # (!\Q[4]~reg0_regout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Q[4]~reg0_regout\,
	datad => VCC,
	cin => \Q[3]~21\,
	combout => \Q[4]~22_combout\,
	cout => \Q[4]~23\);

-- Location: LCFF_X43_Y35_N7
\Q[4]~reg0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \Q[4]~22_combout\,
	aclr => \R~clkctrl_outclk\,
	ena => \enable~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Q[4]~reg0_regout\);

-- Location: LCCOMB_X43_Y35_N8
\Q[5]~24\ : cycloneii_lcell_comb
-- Equation(s):
-- \Q[5]~24_combout\ = (\Q[5]~reg0_regout\ & (\Q[4]~23\ $ (GND))) # (!\Q[5]~reg0_regout\ & (!\Q[4]~23\ & VCC))
-- \Q[5]~25\ = CARRY((\Q[5]~reg0_regout\ & !\Q[4]~23\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Q[5]~reg0_regout\,
	datad => VCC,
	cin => \Q[4]~23\,
	combout => \Q[5]~24_combout\,
	cout => \Q[5]~25\);

-- Location: LCFF_X43_Y35_N9
\Q[5]~reg0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \Q[5]~24_combout\,
	aclr => \R~clkctrl_outclk\,
	ena => \enable~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Q[5]~reg0_regout\);

-- Location: LCCOMB_X43_Y35_N10
\Q[6]~26\ : cycloneii_lcell_comb
-- Equation(s):
-- \Q[6]~26_combout\ = (\Q[6]~reg0_regout\ & (!\Q[5]~25\)) # (!\Q[6]~reg0_regout\ & ((\Q[5]~25\) # (GND)))
-- \Q[6]~27\ = CARRY((!\Q[5]~25\) # (!\Q[6]~reg0_regout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Q[6]~reg0_regout\,
	datad => VCC,
	cin => \Q[5]~25\,
	combout => \Q[6]~26_combout\,
	cout => \Q[6]~27\);

-- Location: LCFF_X43_Y35_N11
\Q[6]~reg0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \Q[6]~26_combout\,
	aclr => \R~clkctrl_outclk\,
	ena => \enable~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Q[6]~reg0_regout\);

-- Location: LCCOMB_X43_Y35_N12
\Q[7]~28\ : cycloneii_lcell_comb
-- Equation(s):
-- \Q[7]~28_combout\ = (\Q[7]~reg0_regout\ & (\Q[6]~27\ $ (GND))) # (!\Q[7]~reg0_regout\ & (!\Q[6]~27\ & VCC))
-- \Q[7]~29\ = CARRY((\Q[7]~reg0_regout\ & !\Q[6]~27\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Q[7]~reg0_regout\,
	datad => VCC,
	cin => \Q[6]~27\,
	combout => \Q[7]~28_combout\,
	cout => \Q[7]~29\);

-- Location: LCFF_X43_Y35_N13
\Q[7]~reg0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \Q[7]~28_combout\,
	aclr => \R~clkctrl_outclk\,
	ena => \enable~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Q[7]~reg0_regout\);

-- Location: LCCOMB_X43_Y35_N14
\Q[8]~30\ : cycloneii_lcell_comb
-- Equation(s):
-- \Q[8]~30_combout\ = (\Q[8]~reg0_regout\ & (!\Q[7]~29\)) # (!\Q[8]~reg0_regout\ & ((\Q[7]~29\) # (GND)))
-- \Q[8]~31\ = CARRY((!\Q[7]~29\) # (!\Q[8]~reg0_regout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Q[8]~reg0_regout\,
	datad => VCC,
	cin => \Q[7]~29\,
	combout => \Q[8]~30_combout\,
	cout => \Q[8]~31\);

-- Location: LCFF_X43_Y35_N15
\Q[8]~reg0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \Q[8]~30_combout\,
	aclr => \R~clkctrl_outclk\,
	ena => \enable~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Q[8]~reg0_regout\);

-- Location: LCCOMB_X43_Y35_N16
\Q[9]~32\ : cycloneii_lcell_comb
-- Equation(s):
-- \Q[9]~32_combout\ = (\Q[9]~reg0_regout\ & (\Q[8]~31\ $ (GND))) # (!\Q[9]~reg0_regout\ & (!\Q[8]~31\ & VCC))
-- \Q[9]~33\ = CARRY((\Q[9]~reg0_regout\ & !\Q[8]~31\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Q[9]~reg0_regout\,
	datad => VCC,
	cin => \Q[8]~31\,
	combout => \Q[9]~32_combout\,
	cout => \Q[9]~33\);

-- Location: LCFF_X43_Y35_N17
\Q[9]~reg0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \Q[9]~32_combout\,
	aclr => \R~clkctrl_outclk\,
	ena => \enable~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Q[9]~reg0_regout\);

-- Location: LCCOMB_X43_Y35_N18
\Q[10]~34\ : cycloneii_lcell_comb
-- Equation(s):
-- \Q[10]~34_combout\ = (\Q[10]~reg0_regout\ & (!\Q[9]~33\)) # (!\Q[10]~reg0_regout\ & ((\Q[9]~33\) # (GND)))
-- \Q[10]~35\ = CARRY((!\Q[9]~33\) # (!\Q[10]~reg0_regout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Q[10]~reg0_regout\,
	datad => VCC,
	cin => \Q[9]~33\,
	combout => \Q[10]~34_combout\,
	cout => \Q[10]~35\);

-- Location: LCFF_X43_Y35_N19
\Q[10]~reg0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \Q[10]~34_combout\,
	aclr => \R~clkctrl_outclk\,
	ena => \enable~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Q[10]~reg0_regout\);

-- Location: LCCOMB_X43_Y35_N20
\Q[11]~36\ : cycloneii_lcell_comb
-- Equation(s):
-- \Q[11]~36_combout\ = (\Q[11]~reg0_regout\ & (\Q[10]~35\ $ (GND))) # (!\Q[11]~reg0_regout\ & (!\Q[10]~35\ & VCC))
-- \Q[11]~37\ = CARRY((\Q[11]~reg0_regout\ & !\Q[10]~35\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Q[11]~reg0_regout\,
	datad => VCC,
	cin => \Q[10]~35\,
	combout => \Q[11]~36_combout\,
	cout => \Q[11]~37\);

-- Location: LCFF_X43_Y35_N21
\Q[11]~reg0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \Q[11]~36_combout\,
	aclr => \R~clkctrl_outclk\,
	ena => \enable~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Q[11]~reg0_regout\);

-- Location: LCCOMB_X43_Y35_N22
\Q[12]~38\ : cycloneii_lcell_comb
-- Equation(s):
-- \Q[12]~38_combout\ = (\Q[12]~reg0_regout\ & (!\Q[11]~37\)) # (!\Q[12]~reg0_regout\ & ((\Q[11]~37\) # (GND)))
-- \Q[12]~39\ = CARRY((!\Q[11]~37\) # (!\Q[12]~reg0_regout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Q[12]~reg0_regout\,
	datad => VCC,
	cin => \Q[11]~37\,
	combout => \Q[12]~38_combout\,
	cout => \Q[12]~39\);

-- Location: LCFF_X43_Y35_N23
\Q[12]~reg0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \Q[12]~38_combout\,
	aclr => \R~clkctrl_outclk\,
	ena => \enable~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Q[12]~reg0_regout\);

-- Location: LCCOMB_X43_Y35_N24
\Q[13]~40\ : cycloneii_lcell_comb
-- Equation(s):
-- \Q[13]~40_combout\ = (\Q[13]~reg0_regout\ & (\Q[12]~39\ $ (GND))) # (!\Q[13]~reg0_regout\ & (!\Q[12]~39\ & VCC))
-- \Q[13]~41\ = CARRY((\Q[13]~reg0_regout\ & !\Q[12]~39\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Q[13]~reg0_regout\,
	datad => VCC,
	cin => \Q[12]~39\,
	combout => \Q[13]~40_combout\,
	cout => \Q[13]~41\);

-- Location: LCFF_X43_Y35_N25
\Q[13]~reg0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \Q[13]~40_combout\,
	aclr => \R~clkctrl_outclk\,
	ena => \enable~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Q[13]~reg0_regout\);

-- Location: LCCOMB_X43_Y35_N26
\Q[14]~42\ : cycloneii_lcell_comb
-- Equation(s):
-- \Q[14]~42_combout\ = (\Q[14]~reg0_regout\ & (!\Q[13]~41\)) # (!\Q[14]~reg0_regout\ & ((\Q[13]~41\) # (GND)))
-- \Q[14]~43\ = CARRY((!\Q[13]~41\) # (!\Q[14]~reg0_regout\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Q[14]~reg0_regout\,
	datad => VCC,
	cin => \Q[13]~41\,
	combout => \Q[14]~42_combout\,
	cout => \Q[14]~43\);

-- Location: LCFF_X43_Y35_N27
\Q[14]~reg0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \Q[14]~42_combout\,
	aclr => \R~clkctrl_outclk\,
	ena => \enable~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Q[14]~reg0_regout\);

-- Location: LCCOMB_X43_Y35_N28
\Q[15]~44\ : cycloneii_lcell_comb
-- Equation(s):
-- \Q[15]~44_combout\ = \Q[14]~43\ $ (!\Q[15]~reg0_regout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111000000001111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datad => \Q[15]~reg0_regout\,
	cin => \Q[14]~43\,
	combout => \Q[15]~44_combout\);

-- Location: LCFF_X43_Y35_N29
\Q[15]~reg0\ : cycloneii_lcell_ff
PORT MAP (
	clk => \clk~clkctrl_outclk\,
	datain => \Q[15]~44_combout\,
	aclr => \R~clkctrl_outclk\,
	ena => \enable~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Q[15]~reg0_regout\);

-- Location: PIN_G16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\Q[0]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \Q[0]~reg0_regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_Q(0));

-- Location: PIN_H15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\Q[1]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \Q[1]~reg0_regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_Q(1));

-- Location: PIN_D15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\Q[2]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \Q[2]~reg0_regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_Q(2));

-- Location: PIN_G15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\Q[3]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \Q[3]~reg0_regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_Q(3));

-- Location: PIN_J17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\Q[4]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \Q[4]~reg0_regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_Q(4));

-- Location: PIN_A18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\Q[5]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \Q[5]~reg0_regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_Q(5));

-- Location: PIN_D16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\Q[6]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \Q[6]~reg0_regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_Q(6));

-- Location: PIN_F16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\Q[7]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \Q[7]~reg0_regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_Q(7));

-- Location: PIN_B18,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\Q[8]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \Q[8]~reg0_regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_Q(8));

-- Location: PIN_C17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\Q[9]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \Q[9]~reg0_regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_Q(9));

-- Location: PIN_E15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\Q[10]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \Q[10]~reg0_regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_Q(10));

-- Location: PIN_A17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\Q[11]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \Q[11]~reg0_regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_Q(11));

-- Location: PIN_D17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\Q[12]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \Q[12]~reg0_regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_Q(12));

-- Location: PIN_B17,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\Q[13]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \Q[13]~reg0_regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_Q(13));

-- Location: PIN_H16,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\Q[14]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \Q[14]~reg0_regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_Q(14));

-- Location: PIN_F15,	 I/O Standard: 3.3-V LVTTL,	 Current Strength: 24mA
\Q[15]~I\ : cycloneii_io
-- pragma translate_off
GENERIC MAP (
	input_async_reset => "none",
	input_power_up => "low",
	input_register_mode => "none",
	input_sync_reset => "none",
	oe_async_reset => "none",
	oe_power_up => "low",
	oe_register_mode => "none",
	oe_sync_reset => "none",
	operation_mode => "output",
	output_async_reset => "none",
	output_power_up => "low",
	output_register_mode => "none",
	output_sync_reset => "none")
-- pragma translate_on
PORT MAP (
	datain => \Q[15]~reg0_regout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	padio => ww_Q(15));
END structure;


