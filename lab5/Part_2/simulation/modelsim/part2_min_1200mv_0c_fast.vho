-- Copyright (C) 1991-2014 Altera Corporation. All rights reserved.
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, the Altera Quartus II License Agreement,
-- the Altera MegaCore Function License Agreement, or other 
-- applicable license agreement, including, without limitation, 
-- that your use is for the sole purpose of programming logic 
-- devices manufactured by Altera and sold by Altera or its 
-- authorized distributors.  Please refer to the applicable 
-- agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 14.1.0 Build 186 12/03/2014 SJ Web Edition"

-- DATE "04/21/2015 23:56:43"

-- 
-- Device: Altera EP4CGX15BF14C6 Package FBGA169
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIV;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIV.CYCLONEIV_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	part2 IS
    PORT (
	clk : IN std_logic;
	R : IN std_logic;
	enable : IN std_logic;
	Q : BUFFER std_logic_vector(15 DOWNTO 0)
	);
END part2;

-- Design Ports Information
-- Q[0]	=>  Location: PIN_K8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[1]	=>  Location: PIN_N9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[2]	=>  Location: PIN_M9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[3]	=>  Location: PIN_B10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[4]	=>  Location: PIN_K9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[5]	=>  Location: PIN_N10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[6]	=>  Location: PIN_N12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[7]	=>  Location: PIN_L9,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[8]	=>  Location: PIN_M11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[9]	=>  Location: PIN_N8,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[10]	=>  Location: PIN_K12,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[11]	=>  Location: PIN_L7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[12]	=>  Location: PIN_M13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[13]	=>  Location: PIN_L11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[14]	=>  Location: PIN_N11,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- Q[15]	=>  Location: PIN_N13,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- enable	=>  Location: PIN_K10,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- clk	=>  Location: PIN_J7,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- R	=>  Location: PIN_J6,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF part2 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_R : std_logic;
SIGNAL ww_enable : std_logic;
SIGNAL ww_Q : std_logic_vector(15 DOWNTO 0);
SIGNAL \R~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \clk~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \Q[0]~output_o\ : std_logic;
SIGNAL \Q[1]~output_o\ : std_logic;
SIGNAL \Q[2]~output_o\ : std_logic;
SIGNAL \Q[3]~output_o\ : std_logic;
SIGNAL \Q[4]~output_o\ : std_logic;
SIGNAL \Q[5]~output_o\ : std_logic;
SIGNAL \Q[6]~output_o\ : std_logic;
SIGNAL \Q[7]~output_o\ : std_logic;
SIGNAL \Q[8]~output_o\ : std_logic;
SIGNAL \Q[9]~output_o\ : std_logic;
SIGNAL \Q[10]~output_o\ : std_logic;
SIGNAL \Q[11]~output_o\ : std_logic;
SIGNAL \Q[12]~output_o\ : std_logic;
SIGNAL \Q[13]~output_o\ : std_logic;
SIGNAL \Q[14]~output_o\ : std_logic;
SIGNAL \Q[15]~output_o\ : std_logic;
SIGNAL \clk~input_o\ : std_logic;
SIGNAL \clk~inputclkctrl_outclk\ : std_logic;
SIGNAL \enable~input_o\ : std_logic;
SIGNAL \Q[0]~15_combout\ : std_logic;
SIGNAL \R~input_o\ : std_logic;
SIGNAL \R~inputclkctrl_outclk\ : std_logic;
SIGNAL \Q[0]~reg0_q\ : std_logic;
SIGNAL \Q[1]~16_combout\ : std_logic;
SIGNAL \Q[1]~reg0_q\ : std_logic;
SIGNAL \Q[1]~17\ : std_logic;
SIGNAL \Q[2]~18_combout\ : std_logic;
SIGNAL \Q[2]~reg0_q\ : std_logic;
SIGNAL \Q[2]~19\ : std_logic;
SIGNAL \Q[3]~20_combout\ : std_logic;
SIGNAL \Q[3]~reg0_q\ : std_logic;
SIGNAL \Q[3]~21\ : std_logic;
SIGNAL \Q[4]~22_combout\ : std_logic;
SIGNAL \Q[4]~reg0_q\ : std_logic;
SIGNAL \Q[4]~23\ : std_logic;
SIGNAL \Q[5]~24_combout\ : std_logic;
SIGNAL \Q[5]~reg0_q\ : std_logic;
SIGNAL \Q[5]~25\ : std_logic;
SIGNAL \Q[6]~26_combout\ : std_logic;
SIGNAL \Q[6]~reg0_q\ : std_logic;
SIGNAL \Q[6]~27\ : std_logic;
SIGNAL \Q[7]~28_combout\ : std_logic;
SIGNAL \Q[7]~reg0_q\ : std_logic;
SIGNAL \Q[7]~29\ : std_logic;
SIGNAL \Q[8]~30_combout\ : std_logic;
SIGNAL \Q[8]~reg0_q\ : std_logic;
SIGNAL \Q[8]~31\ : std_logic;
SIGNAL \Q[9]~32_combout\ : std_logic;
SIGNAL \Q[9]~reg0_q\ : std_logic;
SIGNAL \Q[9]~33\ : std_logic;
SIGNAL \Q[10]~34_combout\ : std_logic;
SIGNAL \Q[10]~reg0_q\ : std_logic;
SIGNAL \Q[10]~35\ : std_logic;
SIGNAL \Q[11]~36_combout\ : std_logic;
SIGNAL \Q[11]~reg0_q\ : std_logic;
SIGNAL \Q[11]~37\ : std_logic;
SIGNAL \Q[12]~38_combout\ : std_logic;
SIGNAL \Q[12]~reg0_q\ : std_logic;
SIGNAL \Q[12]~39\ : std_logic;
SIGNAL \Q[13]~40_combout\ : std_logic;
SIGNAL \Q[13]~reg0_q\ : std_logic;
SIGNAL \Q[13]~41\ : std_logic;
SIGNAL \Q[14]~42_combout\ : std_logic;
SIGNAL \Q[14]~reg0_q\ : std_logic;
SIGNAL \Q[14]~43\ : std_logic;
SIGNAL \Q[15]~44_combout\ : std_logic;
SIGNAL \Q[15]~reg0_q\ : std_logic;
SIGNAL \ALT_INV_R~inputclkctrl_outclk\ : std_logic;

BEGIN

ww_clk <= clk;
ww_R <= R;
ww_enable <= enable;
Q <= ww_Q;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\R~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \R~input_o\);

\clk~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \clk~input_o\);
\ALT_INV_R~inputclkctrl_outclk\ <= NOT \R~inputclkctrl_outclk\;

-- Location: IOOBUF_X22_Y0_N9
\Q[0]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Q[0]~reg0_q\,
	devoe => ww_devoe,
	o => \Q[0]~output_o\);

-- Location: IOOBUF_X20_Y0_N2
\Q[1]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Q[1]~reg0_q\,
	devoe => ww_devoe,
	o => \Q[1]~output_o\);

-- Location: IOOBUF_X24_Y0_N2
\Q[2]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Q[2]~reg0_q\,
	devoe => ww_devoe,
	o => \Q[2]~output_o\);

-- Location: IOOBUF_X24_Y31_N9
\Q[3]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Q[3]~reg0_q\,
	devoe => ww_devoe,
	o => \Q[3]~output_o\);

-- Location: IOOBUF_X22_Y0_N2
\Q[4]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Q[4]~reg0_q\,
	devoe => ww_devoe,
	o => \Q[4]~output_o\);

-- Location: IOOBUF_X26_Y0_N9
\Q[5]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Q[5]~reg0_q\,
	devoe => ww_devoe,
	o => \Q[5]~output_o\);

-- Location: IOOBUF_X29_Y0_N2
\Q[6]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Q[6]~reg0_q\,
	devoe => ww_devoe,
	o => \Q[6]~output_o\);

-- Location: IOOBUF_X24_Y0_N9
\Q[7]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Q[7]~reg0_q\,
	devoe => ww_devoe,
	o => \Q[7]~output_o\);

-- Location: IOOBUF_X29_Y0_N9
\Q[8]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Q[8]~reg0_q\,
	devoe => ww_devoe,
	o => \Q[8]~output_o\);

-- Location: IOOBUF_X20_Y0_N9
\Q[9]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Q[9]~reg0_q\,
	devoe => ww_devoe,
	o => \Q[9]~output_o\);

-- Location: IOOBUF_X33_Y11_N9
\Q[10]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Q[10]~reg0_q\,
	devoe => ww_devoe,
	o => \Q[10]~output_o\);

-- Location: IOOBUF_X14_Y0_N2
\Q[11]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Q[11]~reg0_q\,
	devoe => ww_devoe,
	o => \Q[11]~output_o\);

-- Location: IOOBUF_X33_Y10_N2
\Q[12]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Q[12]~reg0_q\,
	devoe => ww_devoe,
	o => \Q[12]~output_o\);

-- Location: IOOBUF_X31_Y0_N2
\Q[13]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Q[13]~reg0_q\,
	devoe => ww_devoe,
	o => \Q[13]~output_o\);

-- Location: IOOBUF_X26_Y0_N2
\Q[14]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Q[14]~reg0_q\,
	devoe => ww_devoe,
	o => \Q[14]~output_o\);

-- Location: IOOBUF_X33_Y10_N9
\Q[15]~output\ : cycloneiv_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \Q[15]~reg0_q\,
	devoe => ww_devoe,
	o => \Q[15]~output_o\);

-- Location: IOIBUF_X16_Y0_N15
\clk~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clk,
	o => \clk~input_o\);

-- Location: CLKCTRL_G17
\clk~inputclkctrl\ : cycloneiv_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \clk~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clk~inputclkctrl_outclk\);

-- Location: IOIBUF_X31_Y0_N8
\enable~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_enable,
	o => \enable~input_o\);

-- Location: LCCOMB_X26_Y1_N0
\Q[0]~15\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Q[0]~15_combout\ = \Q[0]~reg0_q\ $ (\enable~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111111110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \Q[0]~reg0_q\,
	datad => \enable~input_o\,
	combout => \Q[0]~15_combout\);

-- Location: IOIBUF_X16_Y0_N22
\R~input\ : cycloneiv_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_R,
	o => \R~input_o\);

-- Location: CLKCTRL_G19
\R~inputclkctrl\ : cycloneiv_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \R~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \R~inputclkctrl_outclk\);

-- Location: FF_X26_Y1_N1
\Q[0]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \Q[0]~15_combout\,
	clrn => \ALT_INV_R~inputclkctrl_outclk\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Q[0]~reg0_q\);

-- Location: LCCOMB_X26_Y1_N2
\Q[1]~16\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Q[1]~16_combout\ = (\Q[0]~reg0_q\ & (\Q[1]~reg0_q\ $ (VCC))) # (!\Q[0]~reg0_q\ & (\Q[1]~reg0_q\ & VCC))
-- \Q[1]~17\ = CARRY((\Q[0]~reg0_q\ & \Q[1]~reg0_q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0110011010001000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \Q[0]~reg0_q\,
	datab => \Q[1]~reg0_q\,
	datad => VCC,
	combout => \Q[1]~16_combout\,
	cout => \Q[1]~17\);

-- Location: FF_X26_Y1_N3
\Q[1]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \Q[1]~16_combout\,
	clrn => \ALT_INV_R~inputclkctrl_outclk\,
	ena => \enable~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Q[1]~reg0_q\);

-- Location: LCCOMB_X26_Y1_N4
\Q[2]~18\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Q[2]~18_combout\ = (\Q[2]~reg0_q\ & (!\Q[1]~17\)) # (!\Q[2]~reg0_q\ & ((\Q[1]~17\) # (GND)))
-- \Q[2]~19\ = CARRY((!\Q[1]~17\) # (!\Q[2]~reg0_q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Q[2]~reg0_q\,
	datad => VCC,
	cin => \Q[1]~17\,
	combout => \Q[2]~18_combout\,
	cout => \Q[2]~19\);

-- Location: FF_X26_Y1_N5
\Q[2]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \Q[2]~18_combout\,
	clrn => \ALT_INV_R~inputclkctrl_outclk\,
	ena => \enable~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Q[2]~reg0_q\);

-- Location: LCCOMB_X26_Y1_N6
\Q[3]~20\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Q[3]~20_combout\ = (\Q[3]~reg0_q\ & (\Q[2]~19\ $ (GND))) # (!\Q[3]~reg0_q\ & (!\Q[2]~19\ & VCC))
-- \Q[3]~21\ = CARRY((\Q[3]~reg0_q\ & !\Q[2]~19\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Q[3]~reg0_q\,
	datad => VCC,
	cin => \Q[2]~19\,
	combout => \Q[3]~20_combout\,
	cout => \Q[3]~21\);

-- Location: FF_X26_Y1_N7
\Q[3]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \Q[3]~20_combout\,
	clrn => \ALT_INV_R~inputclkctrl_outclk\,
	ena => \enable~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Q[3]~reg0_q\);

-- Location: LCCOMB_X26_Y1_N8
\Q[4]~22\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Q[4]~22_combout\ = (\Q[4]~reg0_q\ & (!\Q[3]~21\)) # (!\Q[4]~reg0_q\ & ((\Q[3]~21\) # (GND)))
-- \Q[4]~23\ = CARRY((!\Q[3]~21\) # (!\Q[4]~reg0_q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Q[4]~reg0_q\,
	datad => VCC,
	cin => \Q[3]~21\,
	combout => \Q[4]~22_combout\,
	cout => \Q[4]~23\);

-- Location: FF_X26_Y1_N9
\Q[4]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \Q[4]~22_combout\,
	clrn => \ALT_INV_R~inputclkctrl_outclk\,
	ena => \enable~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Q[4]~reg0_q\);

-- Location: LCCOMB_X26_Y1_N10
\Q[5]~24\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Q[5]~24_combout\ = (\Q[5]~reg0_q\ & (\Q[4]~23\ $ (GND))) # (!\Q[5]~reg0_q\ & (!\Q[4]~23\ & VCC))
-- \Q[5]~25\ = CARRY((\Q[5]~reg0_q\ & !\Q[4]~23\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Q[5]~reg0_q\,
	datad => VCC,
	cin => \Q[4]~23\,
	combout => \Q[5]~24_combout\,
	cout => \Q[5]~25\);

-- Location: FF_X26_Y1_N11
\Q[5]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \Q[5]~24_combout\,
	clrn => \ALT_INV_R~inputclkctrl_outclk\,
	ena => \enable~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Q[5]~reg0_q\);

-- Location: LCCOMB_X26_Y1_N12
\Q[6]~26\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Q[6]~26_combout\ = (\Q[6]~reg0_q\ & (!\Q[5]~25\)) # (!\Q[6]~reg0_q\ & ((\Q[5]~25\) # (GND)))
-- \Q[6]~27\ = CARRY((!\Q[5]~25\) # (!\Q[6]~reg0_q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101101001011111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Q[6]~reg0_q\,
	datad => VCC,
	cin => \Q[5]~25\,
	combout => \Q[6]~26_combout\,
	cout => \Q[6]~27\);

-- Location: FF_X26_Y1_N13
\Q[6]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \Q[6]~26_combout\,
	clrn => \ALT_INV_R~inputclkctrl_outclk\,
	ena => \enable~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Q[6]~reg0_q\);

-- Location: LCCOMB_X26_Y1_N14
\Q[7]~28\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Q[7]~28_combout\ = (\Q[7]~reg0_q\ & (\Q[6]~27\ $ (GND))) # (!\Q[7]~reg0_q\ & (!\Q[6]~27\ & VCC))
-- \Q[7]~29\ = CARRY((\Q[7]~reg0_q\ & !\Q[6]~27\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Q[7]~reg0_q\,
	datad => VCC,
	cin => \Q[6]~27\,
	combout => \Q[7]~28_combout\,
	cout => \Q[7]~29\);

-- Location: FF_X26_Y1_N15
\Q[7]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \Q[7]~28_combout\,
	clrn => \ALT_INV_R~inputclkctrl_outclk\,
	ena => \enable~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Q[7]~reg0_q\);

-- Location: LCCOMB_X26_Y1_N16
\Q[8]~30\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Q[8]~30_combout\ = (\Q[8]~reg0_q\ & (!\Q[7]~29\)) # (!\Q[8]~reg0_q\ & ((\Q[7]~29\) # (GND)))
-- \Q[8]~31\ = CARRY((!\Q[7]~29\) # (!\Q[8]~reg0_q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Q[8]~reg0_q\,
	datad => VCC,
	cin => \Q[7]~29\,
	combout => \Q[8]~30_combout\,
	cout => \Q[8]~31\);

-- Location: FF_X26_Y1_N17
\Q[8]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \Q[8]~30_combout\,
	clrn => \ALT_INV_R~inputclkctrl_outclk\,
	ena => \enable~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Q[8]~reg0_q\);

-- Location: LCCOMB_X26_Y1_N18
\Q[9]~32\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Q[9]~32_combout\ = (\Q[9]~reg0_q\ & (\Q[8]~31\ $ (GND))) # (!\Q[9]~reg0_q\ & (!\Q[8]~31\ & VCC))
-- \Q[9]~33\ = CARRY((\Q[9]~reg0_q\ & !\Q[8]~31\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1100001100001100",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Q[9]~reg0_q\,
	datad => VCC,
	cin => \Q[8]~31\,
	combout => \Q[9]~32_combout\,
	cout => \Q[9]~33\);

-- Location: FF_X26_Y1_N19
\Q[9]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \Q[9]~32_combout\,
	clrn => \ALT_INV_R~inputclkctrl_outclk\,
	ena => \enable~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Q[9]~reg0_q\);

-- Location: LCCOMB_X26_Y1_N20
\Q[10]~34\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Q[10]~34_combout\ = (\Q[10]~reg0_q\ & (!\Q[9]~33\)) # (!\Q[10]~reg0_q\ & ((\Q[9]~33\) # (GND)))
-- \Q[10]~35\ = CARRY((!\Q[9]~33\) # (!\Q[10]~reg0_q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Q[10]~reg0_q\,
	datad => VCC,
	cin => \Q[9]~33\,
	combout => \Q[10]~34_combout\,
	cout => \Q[10]~35\);

-- Location: FF_X26_Y1_N21
\Q[10]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \Q[10]~34_combout\,
	clrn => \ALT_INV_R~inputclkctrl_outclk\,
	ena => \enable~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Q[10]~reg0_q\);

-- Location: LCCOMB_X26_Y1_N22
\Q[11]~36\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Q[11]~36_combout\ = (\Q[11]~reg0_q\ & (\Q[10]~35\ $ (GND))) # (!\Q[11]~reg0_q\ & (!\Q[10]~35\ & VCC))
-- \Q[11]~37\ = CARRY((\Q[11]~reg0_q\ & !\Q[10]~35\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Q[11]~reg0_q\,
	datad => VCC,
	cin => \Q[10]~35\,
	combout => \Q[11]~36_combout\,
	cout => \Q[11]~37\);

-- Location: FF_X26_Y1_N23
\Q[11]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \Q[11]~36_combout\,
	clrn => \ALT_INV_R~inputclkctrl_outclk\,
	ena => \enable~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Q[11]~reg0_q\);

-- Location: LCCOMB_X26_Y1_N24
\Q[12]~38\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Q[12]~38_combout\ = (\Q[12]~reg0_q\ & (!\Q[11]~37\)) # (!\Q[12]~reg0_q\ & ((\Q[11]~37\) # (GND)))
-- \Q[12]~39\ = CARRY((!\Q[11]~37\) # (!\Q[12]~reg0_q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Q[12]~reg0_q\,
	datad => VCC,
	cin => \Q[11]~37\,
	combout => \Q[12]~38_combout\,
	cout => \Q[12]~39\);

-- Location: FF_X26_Y1_N25
\Q[12]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \Q[12]~38_combout\,
	clrn => \ALT_INV_R~inputclkctrl_outclk\,
	ena => \enable~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Q[12]~reg0_q\);

-- Location: LCCOMB_X26_Y1_N26
\Q[13]~40\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Q[13]~40_combout\ = (\Q[13]~reg0_q\ & (\Q[12]~39\ $ (GND))) # (!\Q[13]~reg0_q\ & (!\Q[12]~39\ & VCC))
-- \Q[13]~41\ = CARRY((\Q[13]~reg0_q\ & !\Q[12]~39\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010100001010",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Q[13]~reg0_q\,
	datad => VCC,
	cin => \Q[12]~39\,
	combout => \Q[13]~40_combout\,
	cout => \Q[13]~41\);

-- Location: FF_X26_Y1_N27
\Q[13]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \Q[13]~40_combout\,
	clrn => \ALT_INV_R~inputclkctrl_outclk\,
	ena => \enable~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Q[13]~reg0_q\);

-- Location: LCCOMB_X26_Y1_N28
\Q[14]~42\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Q[14]~42_combout\ = (\Q[14]~reg0_q\ & (!\Q[13]~41\)) # (!\Q[14]~reg0_q\ & ((\Q[13]~41\) # (GND)))
-- \Q[14]~43\ = CARRY((!\Q[13]~41\) # (!\Q[14]~reg0_q\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011110000111111",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	datab => \Q[14]~reg0_q\,
	datad => VCC,
	cin => \Q[13]~41\,
	combout => \Q[14]~42_combout\,
	cout => \Q[14]~43\);

-- Location: FF_X26_Y1_N29
\Q[14]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \Q[14]~42_combout\,
	clrn => \ALT_INV_R~inputclkctrl_outclk\,
	ena => \enable~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Q[14]~reg0_q\);

-- Location: LCCOMB_X26_Y1_N30
\Q[15]~44\ : cycloneiv_lcell_comb
-- Equation(s):
-- \Q[15]~44_combout\ = \Q[15]~reg0_q\ $ (!\Q[14]~43\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1010010110100101",
	sum_lutc_input => "cin")
-- pragma translate_on
PORT MAP (
	dataa => \Q[15]~reg0_q\,
	cin => \Q[14]~43\,
	combout => \Q[15]~44_combout\);

-- Location: FF_X26_Y1_N31
\Q[15]~reg0\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \Q[15]~44_combout\,
	clrn => \ALT_INV_R~inputclkctrl_outclk\,
	ena => \enable~input_o\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \Q[15]~reg0_q\);

ww_Q(0) <= \Q[0]~output_o\;

ww_Q(1) <= \Q[1]~output_o\;

ww_Q(2) <= \Q[2]~output_o\;

ww_Q(3) <= \Q[3]~output_o\;

ww_Q(4) <= \Q[4]~output_o\;

ww_Q(5) <= \Q[5]~output_o\;

ww_Q(6) <= \Q[6]~output_o\;

ww_Q(7) <= \Q[7]~output_o\;

ww_Q(8) <= \Q[8]~output_o\;

ww_Q(9) <= \Q[9]~output_o\;

ww_Q(10) <= \Q[10]~output_o\;

ww_Q(11) <= \Q[11]~output_o\;

ww_Q(12) <= \Q[12]~output_o\;

ww_Q(13) <= \Q[13]~output_o\;

ww_Q(14) <= \Q[14]~output_o\;

ww_Q(15) <= \Q[15]~output_o\;
END structure;


