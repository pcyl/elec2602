LIBRARY ieee;
USE ieee.std_logic_1164.all;
ENTITY part1 IS

PORT (SW: IN STD_LOGIC_VECTOR(17 downto 16); --SW(16):reset, SW(17):the K input 
		KEY: IN STD_LOGIC_VECTOR(0 downto 0); --clk
		LEDR: OUT STD_LOGIC_VECTOR(17 downto 0); --flip flop output
		LEDG: OUT STD_LOGIC_VECTOR(7 downto 0)); --G is true output
end part1;

ARCHITECTURE Behavior OF part1 IS
SIGNAL y_p, y_n : STD_LOGIC_VECTOR(8 downto 0) ;
BEGIN

PROCESS (KEY(0), SW(16))
BEGIN
	if SW(16) = '1' then y_p <= "000000000" ; -- resets to state A
	elsif (KEY(0)'EVENT AND KEY(0) = '1') then y_p <= y_n ; --rising edge clock
	end if;
END PROCESS ;

	y_n(0) <= '1';
	y_n(1) <= ((not y_p(0)) and (not y_p(4)) and (not y_p(8)) and SW(17)) or (y_p(0) and y_p(4) and (not y_p(8)) and SW(17)) or (y_p(0) and (not y_p(4)) and y_p(8) and SW(17));
	y_n(2) <= y_p(0) and y_p(1) and SW(17);
	y_n(3) <= y_p(0) and y_p(2) and SW(17);
	y_n(4) <= y_p(0) and y_p(3) and SW(17);
	y_n(5) <= ((not y_p(0)) and (not y_p(4)) and (not y_p(8)) and (not SW(17))) or (y_p(0) and y_p(4) and (not y_p(8)) and (not SW(17))) or (y_p(0) and (not y_p(4)) and y_p(8) and (not SW(17)));
	y_n(6) <= (y_p(0) and y_p(1) and (not y_p(5)) and (not SW(17))) or (y_p(0) and (not y_p(1)) and y_p(5));
	y_n(7) <= (y_p(0) and y_p(2) and (not y_p(6)) and (not sw(17))) or (y_p(0) and (not y_p(2)) and y_p(6));
	y_n(8) <= (y_p(0) and y_p(3) and (not y_p(7)) and (not SW(17))) or (y_p(0) and (not y_p(3)) and y_p(7));
	LEDR(8 downto 0) <= y_p(8 downto 0);
	LEDG(7 downto 0) <= "11111111" when y_p = ("000010001") else "00000000";
	LEDR(17 downto 10) <= "11111111"when y_p = ("100000001") else "00000000";
END Behavior;